package com.example.zenia.inbox.view.fragment

import android.app.ProgressDialog
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.zenia.R
import com.example.zenia.inbox.presenter.NotifikasiPresenter
import com.example.zenia.inbox.view.recycleView.NotifikasiAdapter
import com.example.zenia.pojo.GetAllNotifikasiResponse
import kotlinx.android.synthetic.main.fragment_notifikasi.*

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 * Use the [NotifikasiFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class NotifikasiFragment(val token: String) : Fragment(), NotifikasiPresenter.Listener {
    // TODO: Rename and change types of parameters
    private var param1: String? = null
    private var param2: String? = null

    lateinit var presenter: NotifikasiPresenter

    @Suppress("DEPRECATION")
    lateinit var progressDialog: ProgressDialog

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            param1 = it.getString(ARG_PARAM1)
            param2 = it.getString(ARG_PARAM2)
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_notifikasi, container, false)
    }

    override fun onResume() {
        super.onResume()

        presenter.getNotif(token)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

//        val notifList = arrayListOf(
//            Notifikasi(
//                "Transaksi Berhasil",
//                "Transaksi transfer uang telah berhasil dilakukan",
//                "12/08/2020 17:00:00",
//                R.drawable.ic_notification_rekening
//            ),
//            Notifikasi(
//                "Transaksi Berhasil",
//                "Transaksi transfer uang telah berhasil dilakukan",
//                "12/08/2020 17:00:00",
//                R.drawable.ic_notification_kpr
//            ),
//            Notifikasi(
//                "Transaksi Berhasil",
//                "Transaksi transfer uang telah berhasil dilakukan",
//                "12/08/2020 17:00:00",
//                R.drawable.ic_notification_rekening
//            )
//        )
//
//        notifRVConatiner.layoutManager = LinearLayoutManager(activity, LinearLayoutManager.VERTICAL,false)
//        notifRVConatiner.adapter = AdapterNotifikasi(notifList)

        presenter = NotifikasiPresenter(this)

        @Suppress("DEPRECATION")
        progressDialog = ProgressDialog(activity)

        @Suppress("DEPRECATION")
        progressDialog.setMessage("Please Wait...")


    }

    companion object {
        private var DATA_TOKEN = "TOKEN"

        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         *
         * @param param1 Parameter 1.
         * @param param2 Parameter 2.
         * @return A new instance of fragment TransaksiFragment.
         */
        // TODO: Rename and change types and number of parameters
        @JvmStatic
//        fun newInstance(param1: String, param2: String) =
//            TransaksiFragment().apply {
//                arguments = Bundle().apply {
//                    putString(ARG_PARAM1, param1)
//                    putString(ARG_PARAM2, param2)
//                }
//            }
        fun newInstance(token: String) =
            InboxFragment().apply {
                arguments = Bundle().apply {
                    putString(DATA_TOKEN, token)
                }
            }
    }

    override fun showDialog() {
        progressDialog.show()
    }

    override fun dismissDialog() {
        progressDialog.dismiss()
    }

    override fun showData(list: List<GetAllNotifikasiResponse.Data>) {
        activity?.runOnUiThread {
            notifRVConatiner.layoutManager =
                LinearLayoutManager(activity, LinearLayoutManager.VERTICAL, false)
            notifRVConatiner.adapter = NotifikasiAdapter(list, token, presenter)
        }
    }
}