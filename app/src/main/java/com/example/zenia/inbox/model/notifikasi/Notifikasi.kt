package com.example.zenia.inbox.model.notifikasi

data class Notifikasi (val judul: String, val detail: String, val waktu: String, val logo: Int)