package com.example.zenia.inbox.presenter

import android.util.Log
import com.example.zenia.network.ApiClient
import com.example.zenia.pojo.GetAllNotifikasiResponse
import com.example.zenia.pojo.PutUpdateNotif
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class TransaksiPresenter(val listener: Listener) {
    interface Listener{
        fun showDialog()
        fun dismissDialog()
        fun showData(list: List<GetAllNotifikasiResponse.Data>)
    }

    fun getNotif(token:String){
        Log.d("token di presenter", token)
        listener.showDialog()
        ApiClient.apiService.getNotification("transaksi", "Bearer $token" ).enqueue(object :
            Callback<GetAllNotifikasiResponse> {
            override fun onResponse(
                call: Call<GetAllNotifikasiResponse>,
                response: Response<GetAllNotifikasiResponse>
            ) {
                Log.d("Response", "${response.body()}")
                response.body()?.data?.let { listener.showData(it) }
//                response.body()?.result?.let {
//                    listener.showListPerson(it)
//                }
                listener.dismissDialog()
            }

            override fun onFailure(call: Call<GetAllNotifikasiResponse>, t: Throwable) {
                //listener.showToastError(t)
                listener.dismissDialog()
            }


        })

        //listener.showDialog()

    }

    fun updateOnClick(id:String, token:String){
        listener.showDialog()
        ApiClient.apiService.putNotification(id, "Bearer $token" ).enqueue(object :
            Callback<PutUpdateNotif> {
            override fun onResponse(
                call: Call<PutUpdateNotif>,
                response: Response<PutUpdateNotif>
            ) {
                listener.dismissDialog()
                getNotif(token)
            }

            override fun onFailure(call: Call<PutUpdateNotif>, t: Throwable) {
                listener.dismissDialog()
            }

        })
    }
}