package com.example.zenia.inbox.view.recycleView

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.example.zenia.R
import com.example.zenia.inbox.model.notifikasi.Notifikasi
import kotlinx.android.synthetic.main.notification_item.view.*

class AdapterNotifikasi(private val listNotifikasi: ArrayList<Notifikasi>): RecyclerView.Adapter<AdapterNotifikasi.ViewHolder>() {
    class ViewHolder(itemView: View): RecyclerView.ViewHolder(itemView)

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ):ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.notification_item, parent, false)

        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        Glide.with(holder.itemView.context)
            .load(listNotifikasi[position].logo)
            .into(holder.itemView.notifImage)

        holder.itemView.notifTitle.text = listNotifikasi[position].judul
        holder.itemView.notifDetail.text = listNotifikasi[position].detail
        holder.itemView.notifTime.text = listNotifikasi[position].waktu
    }

    override fun getItemCount(): Int {
        return listNotifikasi.size
    }

}