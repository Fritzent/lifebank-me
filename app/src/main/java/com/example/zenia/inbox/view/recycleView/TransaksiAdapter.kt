package com.example.zenia.inbox.view.recycleView

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.example.zenia.R
import com.example.zenia.inbox.presenter.TransaksiPresenter
import com.example.zenia.pojo.GetAllNotifikasiResponse
import kotlinx.android.synthetic.main.notification_item.view.*

class TransaksiAdapter(private val listNotif: List<GetAllNotifikasiResponse.Data>, val token:String, val presenter: TransaksiPresenter) : RecyclerView.Adapter<TransaksiAdapter.ViewHolder>() {
    class ViewHolder(itemView: View): RecyclerView.ViewHolder(itemView)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return  ViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.notification_item, parent, false))
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {

        val resource = when (listNotif[position].type) {
            "tabungan" -> {
                R.drawable.ic_notification_rekening
            }
            "kpr" -> {
                R.drawable.ic_notification_kpr
            }
            "nasabah" -> {
                R.drawable.ic_notification_data_profil
            }
            else -> R.drawable.ic_notification_data_profil
        }

        Glide.with(holder.itemView.context)
            .load(resource)
            .into(holder.itemView.notifImage)

        holder.itemView.notifTitle.text = listNotif[position].mobileTitle
        holder.itemView.notifDetail.text = listNotif[position].mobileDescription
        holder.itemView.notifTime.text = listNotif[position].date

        holder.itemView.setOnClickListener {
            presenter.updateOnClick(listNotif[position].id, token)
        }

        val state = listNotif[position].onReadStatus
        if (state){
            holder.itemView.isClickable = false
            holder.itemView.setBackgroundResource(R.color.white)
        } else {
            holder.itemView.isClickable = true
            holder.itemView.setBackgroundResource(R.color.cSecondaryButton)
        }


    }

    override fun getItemCount(): Int {
        return listNotif.size
    }
}