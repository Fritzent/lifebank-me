package com.example.zenia.inbox.view.pager

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter
import com.example.zenia.inbox.view.fragment.NotifikasiFragment
import com.example.zenia.inbox.view.fragment.TransaksiFragment

@Suppress("DEPRECATION")
class InboxPagerAdapter(fm: FragmentManager, token:String): FragmentPagerAdapter(fm) {
    private val pages = listOf(TransaksiFragment(token),NotifikasiFragment(token))

    override fun getCount(): Int {
        return pages.size
    }

    override fun getItem(position: Int): Fragment {
        return pages[position]
    }

    override fun getPageTitle(position: Int): CharSequence? {
        var title = ""

        if (position == 0){
            title = "Transaksi"
        } else if (position == 1){
            title = "Notifikasi"
        }

        return title
    }
}