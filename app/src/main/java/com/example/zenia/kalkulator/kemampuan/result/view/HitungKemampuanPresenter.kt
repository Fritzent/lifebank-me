package com.example.zenia.kalkulator.kemampuan.result.view

import android.os.Bundle
import java.text.DecimalFormat
import java.text.NumberFormat
import java.util.*
import kotlin.properties.Delegates

class HitungKemampuanPresenter (val listener: Listener) {
    interface Listener{
        fun showHasil(
            kemampuanCicil: String,
            hargaRumah: String,
            dp: String,
            jangkaWaktu:String
        )

        fun showCatatanSatudanDua(text1: String, text2:String)
        fun showCatatanTiga(text:String)
        fun showCatatanEmpat(text: String)
        fun dismissCatatanTiga()
        fun dismissCatatanEmpat()
        fun dismissButtonSingle()
        fun dismissButtonDouble()
    }

    private var kemampuanCicil by Delegates.notNull<Long>()
    var hargaRumah by Delegates.notNull<Long>()
    var dp by Delegates.notNull<Long>()
    private var bulan by Delegates.notNull<Int>()
    var tahun by Delegates.notNull<Int>()
    private var umur by Delegates.notNull<Int>()
    private var cicilanLainnya by Delegates.notNull<Long>()

    fun setData(bundle: Bundle){
        kemampuanCicil = bundle.getLong("Kemampuan Cicil")
        hargaRumah = bundle.getLong("Harga Rumah")
        dp = bundle.getLong("DP")
        bulan = bundle.getInt("Bulan")
        tahun = bundle.getInt("Tahun")
        umur = bundle.getInt("Umur")
        cicilanLainnya = bundle.getLong("Cicilan Lainnya")
    }

    fun showHasil(){
        listener.showHasil(
            formatUang(kemampuanCicil),
            formatUang(hargaRumah),
            formatUang(dp),
            "$tahun tahun $bulan bulan"
        )
    }

    fun showCatatan(){
        val kemampuanCicilString = formatUang(kemampuanCicil)
        val dpString = formatUang(dp)
        val hargaRumahString = formatUang(hargaRumah)

        val text1 =
            "Kamu bisa mengajukan KPR dengan kemampuan cicilanmu yang lebih dari Rp 1.000.000 dan jika kamu sudah memiliki uang muka/DP minimal $dpString untuk rumah seharga $hargaRumahString"
        val text2 =
            "Jika kamu belum memiliki uang muka/DP, kamu bisa mulai menabung setiap bulannya sebesar $kemampuanCicilString selama $tahun tahun $bulan bulan untuk memenuhi syarat minimal uang muka/DP pengajuan KPR rumah seharga $hargaRumahString"
        val text3 = "Kamu belum bisa mengajukan KPR karena usiamu kurang dari 21 tahun"
        val text4 = "Kamu belum bisa mengajukan KPR dengan kemampuan cicilanmu yang kurang dari Rp 1.000.000."
        val text5 = "Kamu bisa mempersiapkan uang muka/DP dengan menabung setiap bulannya sebesar $kemampuanCicilString"
        val text6 = "Lunasi cicilan lain yang menjadi tanggunganmu saat ini, sebelum mengajukan KPR."

        if (kemampuanCicil > 1000000){
            listener.showCatatanSatudanDua(text1,text2)
            if (umur < 21){
                listener.showCatatanTiga(text3)
            } else {
                listener.dismissCatatanTiga()
            }
            listener.dismissCatatanEmpat()
            listener.dismissButtonSingle()
        } else {
            listener.showCatatanSatudanDua(text4, text5)
            if (umur < 21 && cicilanLainnya > 0){
                listener.showCatatanTiga(text6)
                listener.showCatatanEmpat(text3)
            } else if (umur < 21){
                listener.showCatatanTiga(text3)
                listener.dismissCatatanEmpat()
            } else if (cicilanLainnya > 0){
                listener.showCatatanTiga(text6)
                listener.dismissCatatanEmpat()
            } else {
                listener.dismissCatatanTiga()
                listener.dismissCatatanEmpat()
            }
            listener.dismissButtonDouble()
        }
    }

    private fun formatUang(input: Long): String {

        val formatter =
            NumberFormat.getCurrencyInstance(Locale("in", "ID")) as DecimalFormat

        var formattedString = formatter.format(input).toString()

        if (formattedString.contains("Rp")) {
            formattedString = formattedString.replace("Rp".toRegex(), "Rp ")
        }

        if (formattedString.contains(",00")) {
            formattedString = formattedString.replace(",00".toRegex(), "")
        }

        return formattedString
    }




}