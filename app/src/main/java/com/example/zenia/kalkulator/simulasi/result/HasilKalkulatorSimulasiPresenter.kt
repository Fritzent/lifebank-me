package com.example.zenia.kalkulator.simulasi.result

import android.os.Bundle
import java.text.DecimalFormat
import java.text.NumberFormat
import java.util.*

class HasilKalkulatorSimulasiPresenter(val listener: Listener) {
    interface Listener {
        fun showHasil(
            cicilan: String,
            pokokKPR: String,
            pembayaranAwal: String,
            tenor: String
        )

        fun showCatatan(
            text1:String,
            text2:String,
            text3:String
        )

        fun showDialog(
            dp: String,
            provisi: String,
            bphtb: String,
            pnbp: String,
            bbn: String,
            pembayaranAwal: String
        )
    }

    lateinit var cicilan: String
    private lateinit var pokokKPR: String
    private lateinit var pembayaranAwal: String
    private lateinit var tenor: String
    lateinit var hargaRumah: String
    private lateinit var provisi: String
    lateinit var dp: String
    private lateinit var bphtb: String
    private lateinit var pnbp: String
    private lateinit var bbn: String

    fun setData(bundle: Bundle) {
        cicilan = formatUang(bundle.getLong("cicilan"))
        pokokKPR = formatUang(bundle.getLong("pokok"))
        pembayaranAwal = formatUang(bundle.getLong("pembayaran awal"))
        tenor = bundle.getInt("tenor").let { "$it Tahun" }
        hargaRumah = formatUang(bundle.getLong("harga rumah"))
        provisi = formatUang(bundle.getLong("provisi"))
        dp = formatUang(bundle.getLong("dp"))
        bphtb = formatUang(bundle.getLong("bphtb"))
        pnbp = formatUang(bundle.getLong("pnbp"))
        bbn = formatUang(bundle.getLong("bbn"))
    }

    fun showHasil() {
        listener.showHasil(cicilan, pokokKPR, pembayaranAwal, tenor)
    }

    fun showCatatan(){
        val text1 = "Untuk mengajukan KPR rumah seharga $hargaRumah kamu harus melakukan pembayaran awal minimal $pembayaranAwal"
        val text2 = "Pokok KPR yang harus kamu cicil sebesar $pokokKPR"
        val text3 = "Cicilan KPR yang harus kamu bayarkan setiap bulannya adalah $cicilan selama $tenor"

        listener.showCatatan(text1, text2, text3)
    }

    fun showDialog(){
        listener.showDialog(dp, provisi, bphtb, pnbp, bbn, pembayaranAwal)
    }

    private fun formatUang(input: Long): String {

        val formatter =
            NumberFormat.getCurrencyInstance(Locale("in", "ID")) as DecimalFormat

        var formattedString = formatter.format(input).toString()

        if (formattedString.contains("Rp")) {
            formattedString = formattedString.replace("Rp".toRegex(), "Rp ")
        }

        if (formattedString.contains(",00")) {
            formattedString = formattedString.replace(",00".toRegex(), "")
        }

        return formattedString
    }
}