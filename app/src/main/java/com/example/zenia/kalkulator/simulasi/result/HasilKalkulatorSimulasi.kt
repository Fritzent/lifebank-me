package com.example.zenia.kalkulator.simulasi.result

import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import com.example.zenia.databinding.ActivityHasilKalkulatorSimulasiBinding
import com.example.zenia.kalkulator.simulasi.result.dialog.InformasiHasilDialogFragment
import kotlinx.android.synthetic.main.activity_hasil_kalkulator_simulasi.*
import kotlinx.android.synthetic.main.toolbar.view.*

class HasilKalkulatorSimulasi : AppCompatActivity(), HasilKalkulatorSimulasiPresenter.Listener {
    private lateinit var binding: ActivityHasilKalkulatorSimulasiBinding
    private lateinit var presenter: HasilKalkulatorSimulasiPresenter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityHasilKalkulatorSimulasiBinding.inflate(layoutInflater)
        presenter = HasilKalkulatorSimulasiPresenter(this)
        setContentView(binding.root)

        val appBar = binding.menuToolbar.appBarLayout.toolbar
        setSupportActionBar(appBar)
        supportActionBar?.title = "Simulasi KPR"
        supportActionBar?.setDisplayHomeAsUpEnabled(true)


        binding.includeButton.doubleButton.visibility = View.GONE

        val bundle= intent.extras
        bundle?.let { presenter.setData(it) }

        presenter.showHasil()
        presenter.showCatatan()



        btnInfo.setOnClickListener {
            presenter.showDialog()
        }
    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }

    override fun showHasil(
        cicilan: String,
        pokokKPR: String,
        pembayaranAwal: String,
        tenor: String
    ) {
        tvHasilCicilan.text = cicilan
        tvHasilPokokKPR.text = pokokKPR
        tvHasilPembayaranAwal.text = pembayaranAwal
        tvHasilLamaCicilan.text = tenor
    }

    override fun showCatatan(text1: String, text2: String, text3: String) {
        tvCatatanSatu.text = text1
        tvCatatanDua.text = text2
        tvCatatanTiga.text = text3
    }

    override fun showDialog(
        dp: String,
        provisi: String,
        bphtb: String,
        pnbp: String,
        bbn: String,
        pembayaranAwal: String
    ) {
        val fm = supportFragmentManager
        InformasiHasilDialogFragment.dataDialog(
            dp,
            provisi,
            bphtb,
            pnbp,
            bbn,
            pembayaranAwal
        ).show(fm,"Modal Bottom Sheet")
    }


}