package com.example.zenia.kalkulator.kemampuan.result.view

import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import com.example.zenia.databinding.ActivityHitungKemampuanBinding
import kotlinx.android.synthetic.main.activity_hitung_kemampuan.*
import kotlinx.android.synthetic.main.toolbar.view.*

class HitungKemampuanActivity : AppCompatActivity(), HitungKemampuanPresenter.Listener {
    private lateinit var binding: ActivityHitungKemampuanBinding
    private lateinit var presenter: HitungKemampuanPresenter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = ActivityHitungKemampuanBinding.inflate(layoutInflater)
        val view = binding.root

        presenter = HitungKemampuanPresenter(this)

        val appBar = binding.menuToolbar.appBarLayout.toolbar
        setSupportActionBar(appBar)
        supportActionBar?.title = "Kemampuan Menabung/Cicilan"
        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        setContentView(view)

        //binding.includeButton.btnTabungan.visibility = View.GONE

        val bundle = intent.extras
        if (bundle != null) {
            presenter.setData(bundle)
        }

        presenter.showHasil()

        presenter.showCatatan()


    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }

    override fun showHasil(
        kemampuanCicil: String,
        hargaRumah: String,
        dp: String,
        jangkaWaktu: String
    ) {
        tvHasilKemampuan.text = kemampuanCicil
        tvHasilHargaMaksimum.text = hargaRumah
        tvHasilDPDibutuhkan.text = dp
        tvHasilWaktuDP.text = jangkaWaktu
    }

    override fun showCatatanSatudanDua(text1: String, text2: String) {
        binding.includeCatatan.tvCatatanSatu.text = text1
        binding.includeCatatan.tvCatatanDua.text = text2
    }

    override fun showCatatanTiga(text: String) {
        binding.includeCatatan.tvCatatanTiga.text = text
    }

    override fun showCatatanEmpat(text: String) {
        binding.includeCatatan.tvCatatanEmpat.text = text
    }

    override fun dismissCatatanTiga() {
        binding.includeCatatan.catatanTiga.visibility = View.GONE
    }

    override fun dismissCatatanEmpat() {
        binding.includeCatatan.catatanEmpat.visibility = View.GONE
    }

    override fun dismissButtonSingle() {
        binding.includeButton.singleButton.visibility = View.GONE
    }

    override fun dismissButtonDouble() {
        binding.includeButton.doubleButton.visibility = View.GONE
    }
}