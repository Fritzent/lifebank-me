package com.example.zenia.kalkulator.tabungan.form.view

import android.content.Intent
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import com.example.zenia.R
import com.example.zenia.databinding.ActivityFormKalkulatorTabunganBinding
import com.example.zenia.kalkulator.tabungan.result.view.HasilKalkulatorTabungan
import kotlinx.android.synthetic.main.activity_form_kalkulator_tabungan.*
import kotlinx.android.synthetic.main.toolbar.view.*

class FormKalkulatorTabungan : AppCompatActivity(), FormKalkulatorPresenter.Listener {
    lateinit var presenter: FormKalkulatorPresenter
    lateinit var binding: ActivityFormKalkulatorTabunganBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityFormKalkulatorTabunganBinding.inflate(layoutInflater)

        setContentView(binding.root)

        val appBar = binding.menuToolbar.appBarLayout.toolbar
        setSupportActionBar(appBar)
        supportActionBar?.title = "Setoran Bulanan Tabungan DP"
        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        presenter = FormKalkulatorPresenter(this)

        presenter.buttonState()

        binding.etUmurSekarang.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {

            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {

            }

            override fun afterTextChanged(s: Editable?) {
                binding.etUmurSekarang.removeTextChangedListener(this)

                presenter.setUmurSekarang(s.toString())

                binding.etUmurSekarang.setSelection(etUmurSekarang.text.length)
                binding.etUmurSekarang.addTextChangedListener(this)
            }
        })

        binding.etUmurDP.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {

            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {

            }

            override fun afterTextChanged(s: Editable?) {
                binding.etUmurDP.removeTextChangedListener(this)

                presenter.setUmurDp(s.toString())

                binding.etUmurDP.setSelection(etUmurDP.text.length)
                binding.etUmurDP.addTextChangedListener(this)
            }
        })

        binding.etHargaRumah.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {

            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {

            }

            override fun afterTextChanged(s: Editable?) {
                binding.etHargaRumah.removeTextChangedListener(this)

                binding.etHargaRumah.setText(presenter.formatHargaRumah(s.toString()))

                binding.etHargaRumah.setSelection(etHargaRumah.text.length)
                binding.etHargaRumah.addTextChangedListener(this)
            }
        })

    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }

    override fun goToHasil(setoran: Long, dp:Long, tahun: Int, bulan:Int, hargaRumah: Long) {

        val intent = Intent(this, HasilKalkulatorTabungan::class.java)

        val bundle = Bundle()
        bundle.putLong("setoran",setoran)
        bundle.putLong("dp", dp)
        bundle.putInt("tahun", tahun)
        bundle.putInt("bulan", bulan)
        bundle.putLong("harga rumah", hargaRumah)

        intent.putExtras(bundle)
        startActivity(intent)

        Log.d(
            "Hasil",
            "Setoran perbulan = $setoran, Jumlah DP = $dp, DP akan tercapai dalam $tahun tahun $bulan bulan"
        )
    }

    override fun showWarningUmur(msg: String) {
        binding.tvUmurSekarangNote.text = msg
    }

    override fun showWarningUmurDp(msg: String) {
        binding.tvUmurDPNote.text = msg
    }

    override fun showWarningHargaRumah(msg: String) {
        binding.tvHargaRumahNote.text = msg
    }

    override fun disableButton() {
        binding.btnSubmit.isClickable = false
        binding.btnSubmit.setBackgroundResource(R.drawable.disabled_button)
    }

    override fun enableButton() {
        binding.btnSubmit.isClickable = true
        binding.btnSubmit.setBackgroundResource(R.drawable.primary_button)
        binding.btnSubmit.setOnClickListener {
            presenter.goToHasilActvity()
        }
    }
}