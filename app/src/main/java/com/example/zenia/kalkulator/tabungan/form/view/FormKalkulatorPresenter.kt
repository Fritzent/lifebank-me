package com.example.zenia.kalkulator.tabungan.form.view

import java.text.DecimalFormat
import java.text.NumberFormat
import java.util.*

class FormKalkulatorPresenter(private val listener: Listener) {
    interface Listener {
        fun goToHasil(setoran: Long, dp: Long, tahun: Int, bulan: Int, hargaRumah: Long)
        fun showWarningUmur(msg: String)
        fun showWarningUmurDp(msg: String)
        fun showWarningHargaRumah(msg: String)
        fun disableButton()
        fun enableButton()
    }

    private var umurSekarang: Int = 0
    private var umurDP: Int = 0
    private var hargaRumah: Long = 0

    private var stateUmurSekarang = false
    private var stateUmurDp = false
    private var stateHargaRumah = false


    fun buttonState(){
        if (stateHargaRumah && stateUmurDp && stateUmurSekarang){
            listener.enableButton()
        } else {
            listener.disableButton()
        }
    }

    fun goToHasilActvity() {
        val dp = hitungDP()
        val setoran = hitungSetoranTabungan()
        val tahun = getTahun()
        val bulan = getBulan()

        listener.goToHasil(setoran, dp, tahun, bulan, hargaRumah)
    }

    fun setUmurSekarang(textInput: String) {
        umurSekarang = if (textInput == "") {
            0
        } else {
            textInput.toInt()
        }

        stateUmurSekarang = when {
            umurSekarang < 18 -> {
                listener.showWarningUmur("Umur minimal 18 tahun")
                false
            }
            umurSekarang > 50 -> {
                listener.showWarningUmur("Umur maksimal 50 tahun")
                false
            }
            else -> {
                listener.showWarningUmur("")
                true
            }
        }

        buttonState()

    }

    fun setUmurDp(textInput: String) {
        umurDP = if (textInput == "") {
            0
        } else {
            textInput.toInt()
        }
        when {
            umurDP < 21 -> {
                listener.showWarningUmurDp("Umur minimal 21 tahun")
                stateUmurDp = false
            }
            umurDP <= umurSekarang -> {
                listener.showWarningUmurDp("Jarak umur DP dengan umur sekarang minimal 1 tahun")
                stateUmurDp = false
            }
            umurDP > 50 -> {
                listener.showWarningUmurDp("Umur maksimal 50 tahun")
                stateUmurDp = false
            }
            else -> {
                listener.showWarningUmurDp("")
                stateUmurDp = true
            }
        }

        buttonState()
    }

    fun formatHargaRumah(input: String): String {
        var textInput = input

        if (textInput.contains(".")) {
            textInput = textInput.replace("[.]".toRegex(), "")
        }

        if (textInput.contains("Rp ")) {
            textInput = textInput.replace("Rp ".toRegex(), "")
        }

        hargaRumah = if (textInput == "") {
            0
        } else {
            textInput.toLong()
        }

        stateHargaRumah = when {
            hargaRumah <= 0 -> {
                listener.showWarningHargaRumah("Harga rumah harus lebih dari 0")
                false
            }
            hargaRumah > 1000000000000 -> {
                listener.showWarningHargaRumah("Harga maksimal 1 triliun")
                false
            }
            else -> {
                listener.showWarningHargaRumah("")
                true
            }
        }

        val formatter =
            NumberFormat.getCurrencyInstance(Locale("in", "ID")) as DecimalFormat

        var formattedString = formatter.format(hargaRumah).toString()

        if (formattedString.contains("Rp")) {
            formattedString = formattedString.replace("Rp".toRegex(), "Rp ")
        }

        if (formattedString.contains(",00")) {
            formattedString = formattedString.replace(",00".toRegex(), "")
        }

        buttonState()

        return formattedString
    }

    private fun hitungJangkaDP(): Int {
        return (umurDP - umurSekarang) * 12
    }

    private fun hitungDP(): Long {
        val dp = hargaRumah * 0.15

        return dp.toLong()
    }

    private fun hitungSetoranTabungan(): Long {
        return hitungDP() / hitungJangkaDP().toLong()
    }

    private fun getBulan(): Int {

        return hitungJangkaDP() % 12
    }

    private fun getTahun(): Int {

        return (hitungJangkaDP() - getBulan()) / 12
    }
}