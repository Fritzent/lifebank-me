package com.example.zenia.kalkulator.menu.view

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.zenia.R
import com.example.zenia.kalkulator.menu.model.MenuKalkulator
import kotlinx.android.synthetic.main.menu_kalkulator_item.view.*

class MenuKalkulatorAdapter(private val listMenu: ArrayList<MenuKalkulator>, private val presenter: MenuKalkulatorPresenter): RecyclerView.Adapter<MenuKalkulatorAdapter.ViewHolder>() {
    class ViewHolder(itemview: View) : RecyclerView.ViewHolder(itemview)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.menu_kalkulator_item,parent, false)

        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val judul = holder.itemView.tvJudul
        val definisi = holder.itemView.tvDefinisi

        judul.text = listMenu[position].judul
        definisi.text = listMenu[position].definisi

        holder.itemView.setOnClickListener {
            presenter.goToKalkulator(position)
        }
    }

    override fun getItemCount(): Int {
        return listMenu.size
    }
}