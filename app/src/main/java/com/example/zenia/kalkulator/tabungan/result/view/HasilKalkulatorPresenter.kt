package com.example.zenia.kalkulator.tabungan.result.view

import android.os.Bundle
import java.text.DecimalFormat
import java.text.NumberFormat
import java.util.*
import kotlin.properties.Delegates

class HasilKalkulatorPresenter(val listener: Listener) {
    interface Listener {
        fun showHasil(
            setoran: String,
            dp: String,
            waktu: String,
            catatan: String
        )
    }

    private var setoran by Delegates.notNull<Long>()
    var dp by Delegates.notNull<Long>()
    var hargaRumah by Delegates.notNull<Long>()
    private var bulan by Delegates.notNull<Int>()
    var tahun by Delegates.notNull<Int>()

    fun setData(bundle: Bundle) {
        setoran = bundle.getLong("setoran", 0)
        dp = bundle.getLong("dp", 0)
        tahun = bundle.getInt("tahun", 0)
        bulan = bundle.getInt("bulan", 0)
        hargaRumah = bundle.getLong("harga rumah", 0)
    }

    fun showHasil() {
        val setoran = formatUang(setoran)
        val dp = formatUang(dp)
        val waktu = "$tahun tahun $bulan bulan"
        val catatan =
            "Kamu bisa mulai menabung setiap bulannya sebesar $setoran selama $tahun tahun $bulan bulan untuk memenuhi syarat minimal uang muka/DP pengajuan KPR rumah seharga ${
                formatUang(hargaRumah)
            }."

        listener.showHasil(setoran, dp, waktu, catatan)
    }


    private fun formatUang(input: Long): String {

        val formatter =
            NumberFormat.getCurrencyInstance(Locale("in", "ID")) as DecimalFormat

        var formattedString = formatter.format(input).toString()

        if (formattedString.contains("Rp")) {
            formattedString = formattedString.replace("Rp".toRegex(), "Rp ")
        }

        if (formattedString.contains(",00")) {
            formattedString = formattedString.replace(",00".toRegex(), "")
        }

        return formattedString
    }
}