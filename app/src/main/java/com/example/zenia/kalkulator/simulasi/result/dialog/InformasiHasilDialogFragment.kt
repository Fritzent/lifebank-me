package com.example.zenia.kalkulator.simulasi.result.dialog

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.example.zenia.R
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import kotlinx.android.synthetic.main.fragment_informasi_hasil_dialog_list_dialog.*

/**
 *
 * A fragment that shows a list of items as a modal bottom sheet.
 *
 * You can show this modal bottom sheet from your activity like this:
 * <pre>
 *    InformasiHasilDialogFragment.newInstance(30).show(supportFragmentManager, "dialog")
 * </pre>
 */
class InformasiHasilDialogFragment : BottomSheetDialogFragment() {

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(
            R.layout.fragment_informasi_hasil_dialog_list_dialog,
            container,
            false
        )
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        Log.d("Argument", "$arguments")

        tvHasilDP.text = arguments?.getString("dp")
        tvHasilProvisi.text = arguments?.getString("provisi")
        tvHasilBPHTB.text = arguments?.getString("bphtb")
        tvHasilPNBP.text = arguments?.getString("pnbp")
        tvHasilBBN.text = arguments?.getString("bbn")
        tvHasilPembayaranAwal.text = arguments?.getString("pembayaran awal")

        closeBTN.setOnClickListener {
            dismiss()
        }

        closeButton.setOnClickListener {
            dismiss()
        }
    }

    companion object {

        fun dataDialog(
            dp: String,
            provisi: String,
            bphtb: String,
            pnbp: String,
            bbn: String,
            pembayaranAwal: String
        ): InformasiHasilDialogFragment {
            val fragment = InformasiHasilDialogFragment()


            val bundle = Bundle().apply {
                putString("dp", dp)
                putString("provisi", provisi)
                putString("bphtb", bphtb)
                putString("pnbp", pnbp)
                putString("bbn", bbn)
                putString("pembayaran awal", pembayaranAwal)
            }

            Log.d("Bundle", "$bundle")

            fragment.arguments = bundle

            return fragment
        }
    }
}