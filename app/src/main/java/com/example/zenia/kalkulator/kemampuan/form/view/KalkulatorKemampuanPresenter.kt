package com.example.zenia.kalkulator.kemampuan.form.view

import java.text.DecimalFormat
import java.text.NumberFormat
import java.util.*

class KalkulatorKemampuanPresenter(val listener: Listener) {
    private var umur: Int = 0
    private var penghasilan: Long = 0
    private var cicilan: Long = 0

    private var stateUmur : Boolean = false
    private var statePenghasilan : Boolean = false

    private var stateInfo = false

    interface Listener {
        fun goToHasil(
            kemampuanCicil : Long,
            hargaRumah: Long,
            dp: Long,
            bulan: Int,
            tahun: Int,
            umur: Int,
            cicilanLainnya: Long
        )
        fun showWarningUmur(msg:String)
        fun showWarningPenghasilan(msg:String)
        fun disableButton()
        fun enableButton()

        fun showInfo()
        fun dismissInfo()
    }

    fun goToHasilActivity() {
        listener.goToHasil(
            hitungKemampuanCicil(),
            hitungHargaRumah(),
            hitungDP(),
            getBulan(),
            getTahun(),
            umur,
            cicilan
        )
    }

    fun stateInfoChange(){
        stateInfo = if (stateInfo){
            listener.dismissInfo()
            false
        } else {
            listener.showInfo()
            true
        }
    }

    fun setUmur(textInput: String) {
        umur = if (textInput == "") {
            0
        } else {
            textInput.toInt()
        }

        stateUmur = when {
            umur < 18 -> {
                listener.showWarningUmur("Umur minimal 18 tahun")
                false
            }
            umur > 50 -> {
                listener.showWarningUmur("Umur maksimal 50 tahun")
                false
            }
            else -> {
                listener.showWarningUmur("")
                true
            }
        }

        buttonState()
    }

    fun formatUangPenghasilan(input: String): String {
        var textInput = input

        if (textInput.contains(".")) {
            textInput = textInput.replace("[.]".toRegex(), "")
        }

        if (textInput.contains("Rp ")) {
            textInput = textInput.replace("Rp ".toRegex(), "")
        }

        penghasilan = if (textInput == "") {
            0
        } else {
            textInput.toLong()
        }

        statePenghasilan = when {
            penghasilan <= 0 -> {
                listener.showWarningPenghasilan("Pengahasilan harus lebih dari 0")
                false
            }
            penghasilan > 5000000000 -> {
                listener.showWarningPenghasilan("Pengahasilan maksimal adalah 5 miliar")
                false
            }
            else -> {
                listener.showWarningPenghasilan("")
                true
            }
        }

        val formatter =
            NumberFormat.getCurrencyInstance(Locale("in", "ID")) as DecimalFormat

        var formattedString = formatter.format(penghasilan).toString()

        if (formattedString.contains("Rp")) {
            formattedString = formattedString.replace("Rp".toRegex(), "Rp ")
        }

        if (formattedString.contains(",00")) {
            formattedString = formattedString.replace(",00".toRegex(), "")
        }

        buttonState()

        return formattedString
    }

    fun formatUangCicilan(input: String): String {
        var textInput = input

        if (textInput.contains(".")) {
            textInput = textInput.replace("[.]".toRegex(), "")
        }

        if (textInput.contains("Rp ")) {
            textInput = textInput.replace("Rp ".toRegex(), "")
        }

        cicilan = if (textInput == "") {
            0
        } else {
            textInput.toLong()
        }

        val formatter =
            NumberFormat.getCurrencyInstance(Locale("in", "ID")) as DecimalFormat

        var formattedString = formatter.format(cicilan).toString()

        if (formattedString.contains("Rp")) {
            formattedString = formattedString.replace("Rp".toRegex(), "Rp ")
        }

        if (formattedString.contains(",00")) {
            formattedString = formattedString.replace(",00".toRegex(), "")
        }

        return formattedString
    }

    private fun hitungKemampuanCicil(): Long {
        var kemampuanCicil = (penghasilan * 30 /100) - cicilan

        if (kemampuanCicil < 0){
            kemampuanCicil = 0
        }

        return kemampuanCicil
    }

    private fun hitungHargaRumah(): Long {
        val jangkaCicil = (50 - umur) * 12

        return hitungKemampuanCicil() * jangkaCicil
    }

    private fun hitungDP(): Long {

        return hitungHargaRumah() * 15 / 100
    }

    private fun hitungWaktuDp(): Long {

        return if (hitungDP() <= 0){
            0
        } else {
            hitungDP() / hitungKemampuanCicil()
        }
    }

    private fun getBulan(): Int {
        val bulan = hitungWaktuDp() % 12
        return bulan.toInt()
    }

    private fun getTahun(): Int {
        val tahun = (hitungWaktuDp() - getBulan()) / 12
        return tahun.toInt()
    }


    fun buttonState(){
        if (stateUmur && statePenghasilan){
            listener.enableButton()
        } else {
            listener.disableButton()
        }
    }
}