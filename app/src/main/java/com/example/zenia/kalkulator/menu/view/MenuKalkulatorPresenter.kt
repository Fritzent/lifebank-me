package com.example.zenia.kalkulator.menu.view

import android.content.Intent
import com.example.zenia.kalkulator.kemampuan.form.view.KalkulatorKemampuanActivity
import com.example.zenia.kalkulator.menu.model.MenuKalkulator
import com.example.zenia.kalkulator.simulasi.form.view.FormKalkulatorSimulasiActivity
import com.example.zenia.kalkulator.tabungan.form.view.FormKalkulatorTabungan

class MenuKalkulatorPresenter (val listener: Listener) {
    interface Listener{
        fun showMenuList(listMenu: ArrayList<MenuKalkulator>)
        fun goToKalkulatorKemampuan()
        fun goToKalkulatorTabungan()
        fun goToKalkulatorSimulai()
    }

    fun goToKalkulator(position: Int){
        when (position) {
            0 -> {
                listener.goToKalkulatorKemampuan()
            }
            1 -> {
                listener.goToKalkulatorTabungan()
            }
            2 -> {
                listener.goToKalkulatorSimulai()
            }
            else -> {

            }
        }
    }

    fun showMenu(){
        val menuList = arrayListOf(
            MenuKalkulator("Kemampuan Menabung/Cicilan", "Menghitung kemampuan menabung/cicilanmu setiap bulan"),
            MenuKalkulator("Setoran Bulanan Tabungan DP", "Menghitur besar setoran tabungan untuk DP-mu setiap bulan"),
            MenuKalkulator("Cicilan Bulanan KPR", "Menghitung besar cicilan untuk KPR-mu setiap bulan")
        )

        listener.showMenuList(menuList)
    }


}