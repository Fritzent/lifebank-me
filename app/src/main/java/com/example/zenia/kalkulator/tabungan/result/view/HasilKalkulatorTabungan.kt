package com.example.zenia.kalkulator.tabungan.result.view

import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import com.example.zenia.databinding.ActivityHasilKalkulatorTabunganBinding
import kotlinx.android.synthetic.main.activity_hasil_kalkulator_tabungan.*
import kotlinx.android.synthetic.main.toolbar.view.*

class HasilKalkulatorTabungan : AppCompatActivity(), HasilKalkulatorPresenter.Listener {
    private lateinit var binding: ActivityHasilKalkulatorTabunganBinding
    private lateinit var presenter: HasilKalkulatorPresenter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityHasilKalkulatorTabunganBinding.inflate(layoutInflater)

        presenter = HasilKalkulatorPresenter(this)
        setContentView(binding.root)

        val appBar = binding.menuToolbar.appBarLayout.toolbar
        setSupportActionBar(appBar)
        supportActionBar?.title = "Setoran Bulanan Tabungan DP"
        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        binding.includeButton.doubleButton.visibility = View.GONE

        val bundle= intent.extras
        if (bundle != null) {
            presenter.setData(bundle)
        }

        presenter.showHasil()

    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }

    override fun showHasil(setoran: String, dp: String, waktu: String, catatan: String) {
        tvHasilSetoran.text = setoran
        tvHasilUangMuka.text = dp
        tvHasilWaktuDP.text = waktu
        tvCatatanTabungan.text = catatan
    }


}