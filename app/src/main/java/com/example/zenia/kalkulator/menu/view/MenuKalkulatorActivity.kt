package com.example.zenia.kalkulator.menu.view

import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.zenia.databinding.ActivityMenuKalkulatorBinding
import com.example.zenia.kalkulator.kemampuan.form.view.KalkulatorKemampuanActivity
import com.example.zenia.kalkulator.menu.model.MenuKalkulator
import com.example.zenia.kalkulator.simulasi.form.view.FormKalkulatorSimulasiActivity
import com.example.zenia.kalkulator.tabungan.form.view.FormKalkulatorTabungan
import kotlinx.android.synthetic.main.activity_menu_kalkulator.*
import kotlinx.android.synthetic.main.toolbar.view.*

class MenuKalkulatorActivity : AppCompatActivity(), MenuKalkulatorPresenter.Listener {
    private lateinit var presenter: MenuKalkulatorPresenter
    private lateinit var binding: ActivityMenuKalkulatorBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMenuKalkulatorBinding.inflate(layoutInflater)


        setContentView(binding.root)

        val appBar = binding.menuToolbar.appBarLayout.toolbar
        setSupportActionBar(appBar)
        supportActionBar?.title = "Kalkulator"
        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        presenter = MenuKalkulatorPresenter(this)

        presenter.showMenu()
    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }

    override fun showMenuList(listMenu: ArrayList<MenuKalkulator>) {
        rv_menu_kalkulator.layoutManager =
            LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false)
        rv_menu_kalkulator.adapter = MenuKalkulatorAdapter(listMenu,presenter)
    }

    override fun goToKalkulatorKemampuan() {
        val intent = Intent(this, KalkulatorKemampuanActivity::class.java)
        startActivity(intent)
    }

    override fun goToKalkulatorTabungan() {
        val intent = Intent(this, FormKalkulatorTabungan::class.java)
        startActivity(intent)
    }

    override fun goToKalkulatorSimulai() {
        val intent = Intent(this, FormKalkulatorSimulasiActivity::class.java)
        startActivity(intent)
    }

}