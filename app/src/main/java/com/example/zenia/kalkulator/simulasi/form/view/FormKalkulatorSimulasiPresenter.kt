package com.example.zenia.kalkulator.simulasi.form.view

import android.util.Log
import java.text.DecimalFormat
import java.text.NumberFormat
import java.util.*

class FormKalkulatorSimulasiPresenter(val listener: Listener) {
    interface Listener {
        fun goToHasil(
            pokok:Long,
            tenor: Int,
            cicilan: Long,
            pembayaranAwal: Long,
            dp: Long,
            provisi: Long,
            bphtb: Long,
            pnbp: Long,
            bbn: Long,
            hargaRumah: Long
        )

        fun showWarningUmur(msg:String)
        fun showWarningHargaRumah(msg:String)
        fun disableButton()
        fun enableButton()
    }

    private var umurDP: Int = 0
    private var hargaRumah: Long =0

    private var stateUmurDP = false
    private var stateHargaRumah = false

    fun buttonState(){
        if (stateUmurDP && stateHargaRumah){
            listener.enableButton()
        } else {
            listener.disableButton()
        }
    }

    fun setUmurDp(textInput: String) {
        umurDP = if (textInput == "") {
            0
        } else {
            textInput.toInt()
        }

        stateUmurDP = when {
            umurDP < 21 -> {
                listener.showWarningUmur("Umur minimal 21 tahun")
                false
            }
            umurDP > 50 -> {
                listener.showWarningUmur("Umur maksimal 50 tahun")
                false
            }
            else -> {
                listener.showWarningUmur("")
                true
            }
        }

        buttonState()

        Log.d("State Umur DP", stateUmurDP.toString())
    }

    fun formatHargaRumah(input: String): String {
        var textInput = input

        if (textInput.contains(".")) {
            textInput = textInput.replace("[.]".toRegex(), "")
        }

        if (textInput.contains("Rp ")) {
            textInput = textInput.replace("Rp ".toRegex(), "")
        }

        hargaRumah = if (textInput == "") {
            0
        } else {
            textInput.toLong()
        }

        stateHargaRumah = when {
            hargaRumah <= 0 -> {
                listener.showWarningHargaRumah("Harga rumah harus lebih dari 0")
                false
            }
            hargaRumah > 1000000000000 -> {
                listener.showWarningHargaRumah("Harga maksimal 1 triliun")
                false
            }
            else -> {
                listener.showWarningHargaRumah("")
                true
            }
        }

        Log.d("State Rumah", stateHargaRumah.toString())

        val formatter =
            NumberFormat.getCurrencyInstance(Locale("in", "ID")) as DecimalFormat

        var formattedString = formatter.format(hargaRumah).toString()

        if (formattedString.contains("Rp")) {
            formattedString = formattedString.replace("Rp".toRegex(), "Rp ")
        }

        if (formattedString.contains(",00")) {
            formattedString = formattedString.replace(",00".toRegex(), "")
        }

        buttonState()

        return formattedString
    }

    private fun hitungDP(): Long {
        val dp = hargaRumah * 0.15

        return dp.toLong()
    }

    private fun hitungTenor(): Int {
        return 50 - umurDP
    }

    private fun hitungPokok():  Long {
        return hargaRumah - hitungDP()
    }

    private fun hitungCicilan(): Long{
        val pokok = hitungPokok()
        val tenor = hitungTenor()

        val hasil = ((pokok*(0.08*tenor))+pokok)/(tenor*12)

        return hasil.toLong()
    }

    private fun hitungProvisi(): Long{
        val hasil = 0.01*hitungPokok()

        return hasil.toLong()
    }

    private fun hitungBPHTB(): Long{
        val hasil = 0.05*(hargaRumah - 60000000)

        return hasil.toLong()
    }

    private fun hitungBBN(): Long{
        val hasil = (0.01*hargaRumah)+500000

        return hasil.toLong()
    }

    private fun hitungPNBP(): Long {
        return (hargaRumah*1/1000)+50000
    }

    private fun hitungPembayaranAwal(): Long{
        return hitungDP()+hitungProvisi()+hitungBPHTB()+hitungPNBP()+hitungBBN()
    }

    fun goToHasil(){
        listener.goToHasil(
            hitungPokok(),
            hitungTenor(),
            hitungCicilan(),
            hitungPembayaranAwal(),
            hitungDP(),
            hitungProvisi(),
            hitungBPHTB(),
            hitungPNBP(),
            hitungBBN(),
            hargaRumah
        )
    }
}