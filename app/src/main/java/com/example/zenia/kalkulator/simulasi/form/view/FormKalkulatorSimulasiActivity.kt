package com.example.zenia.kalkulator.simulasi.form.view

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import com.example.zenia.R
import com.example.zenia.databinding.ActivityFormKalkulatorSimulasiBinding
import com.example.zenia.kalkulator.simulasi.result.HasilKalkulatorSimulasi
import kotlinx.android.synthetic.main.activity_form_kalkulator_simulasi.*
import kotlinx.android.synthetic.main.toolbar.view.*

class FormKalkulatorSimulasiActivity : AppCompatActivity(), FormKalkulatorSimulasiPresenter.Listener {
    private lateinit var presenter: FormKalkulatorSimulasiPresenter
    private lateinit var binding: ActivityFormKalkulatorSimulasiBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityFormKalkulatorSimulasiBinding.inflate(layoutInflater)

        setContentView(binding.root)

        presenter = FormKalkulatorSimulasiPresenter(this)

        val appBar = binding.menuToolbar.appBarLayout.toolbar
        setSupportActionBar(appBar)
        supportActionBar?.title = "Simulasi KPR"
        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        presenter.buttonState()

        binding.etUmurDP.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {

            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {

            }

            override fun afterTextChanged(s: Editable?) {
                binding.etUmurDP.removeTextChangedListener(this)

                presenter.setUmurDp(s.toString())

                binding.etUmurDP.setSelection(etUmurDP.text.length)
                binding.etUmurDP.addTextChangedListener(this)
            }

        })

        binding.etHargaRumah.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {

            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {

            }

            override fun afterTextChanged(s: Editable?) {
                binding.etHargaRumah.removeTextChangedListener(this)

                binding.etHargaRumah.setText(presenter.formatHargaRumah(s.toString()))

                binding.etHargaRumah.setSelection(etHargaRumah.text.length)
                binding.etHargaRumah.addTextChangedListener(this)
            }
        })
    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }

    override fun goToHasil(
        pokok: Long,
        tenor: Int,
        cicilan: Long,
        pembayaranAwal: Long,
        dp: Long,
        provisi: Long,
        bphtb: Long,
        pnbp: Long,
        bbn: Long,
        hargaRumah: Long
    ) {
        val intent = Intent(this, HasilKalkulatorSimulasi::class.java)

        val bundle = Bundle()
        bundle.putLong("pokok",pokok)
        bundle.putInt("tenor",tenor)
        bundle.putLong("cicilan",cicilan)
        bundle.putLong("pembayaran awal",pembayaranAwal)
        bundle.putLong("dp",dp)
        bundle.putLong("provisi",provisi)
        bundle.putLong("bphtb",bphtb)
        bundle.putLong("pnbp",pnbp)
        bundle.putLong("bbn",bbn)
        bundle.putLong("harga rumah", hargaRumah)

        intent.putExtras(bundle)
        startActivity(intent)
    }

    override fun showWarningUmur(msg: String) {
        binding.tvUmurDPNote.text = msg
    }

    override fun showWarningHargaRumah(msg: String) {
        binding.tvHargaRumahNote.text = msg
    }

    override fun disableButton() {
        binding.btnSubmit.isClickable = false
        binding.btnSubmit.setBackgroundResource(R.drawable.disabled_button)
        Log.d("Button", "disabled")
    }

    override fun enableButton() {
        binding.btnSubmit.isClickable = true
        binding.btnSubmit.setBackgroundResource(R.drawable.primary_button)
        binding.btnSubmit.setOnClickListener {
            presenter.goToHasil()
        }
        Log.d("Button", "enabled")
    }
}