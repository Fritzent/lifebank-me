package com.example.zenia.kalkulator.kemampuan.form.view

import android.content.Intent
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import com.example.zenia.R
import com.example.zenia.databinding.ActivityKalkulatorKemampuanBinding
import com.example.zenia.kalkulator.kemampuan.result.view.HitungKemampuanActivity
import kotlinx.android.synthetic.main.activity_kalkulator_kemampuan.*
import kotlinx.android.synthetic.main.toolbar.view.*

class KalkulatorKemampuanActivity : AppCompatActivity(), KalkulatorKemampuanPresenter.Listener {
    lateinit var presenter: KalkulatorKemampuanPresenter
    lateinit var binding: ActivityKalkulatorKemampuanBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityKalkulatorKemampuanBinding.inflate(layoutInflater)
        setContentView(binding.root)

        val appBar = binding.menuToolbar.appBarLayout.toolbar
        setSupportActionBar(appBar)
        supportActionBar?.title = "Kemampuan Menabung/Cicilan"
        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        presenter = KalkulatorKemampuanPresenter(this)

        presenter.buttonState()

        binding.infoCicilanBG.visibility = View.INVISIBLE
        binding.infoCicilanText.visibility = View.INVISIBLE

        binding.etUmurSekarang.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {

            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {

            }

            override fun afterTextChanged(s: Editable?) {
                etUmurSekarang.removeTextChangedListener(this)

                presenter.setUmur(s.toString())

                etUmurSekarang.setSelection(etUmurSekarang.text.length)
                etUmurSekarang.addTextChangedListener(this)
            }

        })

        binding.etPenghasilan.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {

            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {

            }

            override fun afterTextChanged(s: Editable?) {
                etPenghasilan.removeTextChangedListener(this)

                etPenghasilan.setText(presenter.formatUangPenghasilan(s.toString()))

                etPenghasilan.setSelection(etPenghasilan.text.length)
                etPenghasilan.addTextChangedListener(this)
            }

        })

        binding.infoCicilanLainnya.setOnClickListener{
            presenter.stateInfoChange()
        }

        binding.etCicilan.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {

            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {

            }

            override fun afterTextChanged(s: Editable?) {
                etCicilan.removeTextChangedListener(this)

                etCicilan.setText(presenter.formatUangCicilan(s.toString()))

                etCicilan.setSelection(etCicilan.text.length)
                etCicilan.addTextChangedListener(this)

                binding.infoCicilanBG.visibility = View.INVISIBLE
                binding.infoCicilanText.visibility = View.INVISIBLE
            }

        })
    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }

    override fun goToHasil(
        kemampuanCicil : Long,
        hargaRumah: Long,
        dp: Long,
        bulan: Int,
        tahun: Int,
        umur: Int,
        cicilanLainnya: Long
    ) {
        val intent = Intent(this, HitungKemampuanActivity::class.java)
        val bundle = Bundle()
        bundle.putLong("Kemampuan Cicil", kemampuanCicil)
        bundle.putLong("Harga Rumah", hargaRumah)
        bundle.putLong("DP", dp)
        bundle.putInt("Bulan", bulan)
        bundle.putInt("Tahun", tahun)
        bundle.putInt("Umur", umur)
        bundle.putLong("Cicilan Lainnya", cicilanLainnya)

        intent.putExtras(bundle)
        startActivity(intent)

        Log.d(
            "Hasil",
            "kemampuan cicil = $kemampuanCicil, Harga rumah = $hargaRumah, DP Rumah = $dp, Dp tercapai dalam : $tahun tahun, $bulan bulan"
        )
    }

    override fun showWarningUmur(msg: String) {
        binding.tvUmurNote.text = msg
    }

    override fun showWarningPenghasilan(msg: String) {
        binding.tvPenghasilanNote.text = msg
    }

    override fun disableButton() {
        binding.btnSubmit.isClickable = false
        binding.btnSubmit.setBackgroundResource(R.drawable.disabled_button)
    }

    override fun enableButton() {
        binding.btnSubmit.isClickable = true
        binding.btnSubmit.setBackgroundResource(R.drawable.primary_button)
        binding.btnSubmit.setOnClickListener {
            presenter.goToHasilActivity()
        }
    }

    override fun showInfo() {
        binding.infoCicilanBG.visibility = View.VISIBLE
        binding.infoCicilanText.visibility = View.VISIBLE
    }

    override fun dismissInfo() {
        binding.infoCicilanBG.visibility = View.INVISIBLE
        binding.infoCicilanText.visibility = View.INVISIBLE
    }


}