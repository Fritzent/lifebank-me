package com.example.zenia.network

import com.example.zenia.pojo.*
import okhttp3.MultipartBody
import okhttp3.RequestBody
import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.http.*


interface ApiService {
    //Method Post untuk File Datanya  (DONE)
    @Multipart
    @POST("file/upload")
    fun uploadFile(@Part imagename:MultipartBody.Part, @Header("Authorization") authHeader: String): Call<PostFileResponse>

    //Method Post untuk Create Tabungan (DONE)
    @POST("/api/mobile/saving/create-saving-plan")
    fun createTabunganPost(@Body body: PostCreateTabunganBody, @Header("Authorization") authHeader: String): Call<PostCreateTabunganResponse>

    //Method Get All Tabungan (DONE)
    @GET("/api/mobile/saving/saving-plan-list")
    fun collectTabunganGet(@Header("Authorization") authHeader: String) : Call<GetAllTabunganResponse>

    //Method Get Detail Tabungan (DONE)
    @GET("/api/mobile/saving/detail-saving-plan/{savingId}")
    fun detailTabunganGet(@Path("savingId") savingId: String, @Header("Authorization") authHeader: String) : Call<GetDetailTabunganResponse>

    //Method Auth Login (DONE)
    @POST("/api/auth/signin")
    fun authLogin(@Body body: PostAuthLoginBody) : Call<PostAuthLoginResponse>

    //Method  untuk API registration
    @POST("/api/auth/registration")
    fun registrationUser(@Body body: PostAuthRegistrationBody) : Call<PostAuthRegistrationResponse>

    //Method untuk API Validation OTP
    @POST("/api/auth/validate-token/{userId}")
    fun validationOtp(@Body body: PostAuthValidationOtpBody, @Path ("userId") userId: String ) : Call<PostAuthValidationOtpResponse>

    //Method untuk API request new OTP
    @GET("/api/auth/generate-otp/{userId}")
    fun requestNewOtp(@Path("userId") userId: String) : Call<GetRequestNewOtpResponse>

    //Method untuk API Compilation Regsitration
    @PUT("/api/auth/verification/{userId}")
    fun compilationRegistration(@Body body: PutCompilationRegistrationBody, @Path("userId") userId: String) : Call<PutCompilationRegistrationResponse>

    //Method Update Saving Status (DONE)
    @PUT("/api/mobile/saving/update-saving-plan-account/{savingId}")
    fun updateStatusTabungan(@Body body: PutUpdateTabunganStatusBody, @Path("savingId") savingId: String, @Header("Authorization") authHeader: String) : Call<PutUpdateTabunganStatusResponse>

    //Methode Update Tabungan (DONE)
    @PUT("/api/mobile/saving/update-saving-plan/{savingId}")
    fun updateTabunganPut(@Body body: PutUpdateTabunganBody, @Path("savingId") savingId: String, @Header("Authorization") authHeader: String) : Call<PutUpdateTabunganResponse>

    //Method Delete/Hentikan Tabungan
//    @POST("/api/saving/delete-saving-plan/{userId}/{savingId}")
//    fun deleteTabungan(@Path("userId") userId: String, ) : Call<>

    //Method Top Up/ Setor Tabungan
//    @POST("/api/mobile/saving/deposit-saving-plan/{{userId}}/{{savingId}}")
//    fun setorTabungan(@Path("userId") userId: String) : Call<>

//    //Method Update/Edit Tabungan
//    @PUT("/api/mobile/saving/update-saving-plan/{{savingId}}")
//    fun editTabungan(@Body body: PutUpdateTabunganBody, @Path("savingId") savingId: String) : Call<PutUpdateTabunganResponse>

    //Method Pencarian Tabungan/ Disbursement
//    @POST("/api/mobile/saving/disbursement-saving-plan/{userId}/{savingId}")
//    fun disbursementTabungan(@Path("savingId") savingId: String) : call<>

    //Method Auth Register
//    @POST("/api/auth/signup")
//    fun authRegister(@Body body: PostAuthRegisterBody) : Call<>

    //Methode Update Tabungan
    @PUT("/mobile/saving/update-saving-plan/{id}")
    fun updateTabunganPut(@Body body: PutUpdateTabunganBody, @Path("id") id: String) : Call<PutUpdateTabunganResponse>

    //Methode Update Tabungan Status
    @PUT("/mobile/saving/update-saving-plan-account/{tabungan_id}")
    fun updateTabunganStatusPut(@Body body: PutUpdateTabunganStatusBody, @Path("tabungan_id") id: String) : Call<PutUpdateTabunganStatusResponse>

    //Method get notif
    @GET("/api/mobile/inbox/{notificationType}")
    fun getNotification(@Path("notificationType") notificationType: String, @Header("Authorization") authHeader: String) : Call<GetAllNotifikasiResponse>

    @PUT("/api/mobile/checked-inbox/{notifID}")
    fun putNotification(@Path("notifID") notifID: String, @Header("Authorization") authHeader: String) : Call<PutUpdateNotif>

//    //Methode Post Transfer
//    @POST("/api/transfer/{id}")
//    fun postTransfer (@Body body: PostTransferBody, @Path("id") id: String) : Call<PostTransferResponse>

    //Methode Get Nomor Rekening Tujuan
//    @GET("/api/account/transfer")
//    fun getNoRekening (@Header ("Authorization") authHeader: String) : Call <GetNomorRekeningResponse>

    //get nomor rekening
    @POST("/api/mobile/transfer/validate-account-number")
    fun postRekeningNumber(@Body body: PostTransferGetUserBody, @Header("Authorization") authHeader: String) : Call<PostTransferGetUserResponse>

}