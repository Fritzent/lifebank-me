package com.example.zenia.pojo


import com.google.gson.annotations.SerializedName

data class PostTransferGetUserResponse(
    @SerializedName("code")
    val code: String,
    @SerializedName("data")
    val `data`: Data,
    @SerializedName("message")
    val message: String,
    @SerializedName("status")
    val status: String
) {
    data class Data(
        @SerializedName("name")
        val name: String,
        @SerializedName("rekeningNumber")
        val rekeningNumber: String,
        @SerializedName("saved")
        val saved: Boolean
    )
}