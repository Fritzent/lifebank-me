package com.example.zenia.pojo


import com.google.gson.annotations.SerializedName

data class PostAuthRegistrationBody(
    @SerializedName("email")
    val email: String,
    @SerializedName("phoneNumber")
    val phoneNumber: String
)