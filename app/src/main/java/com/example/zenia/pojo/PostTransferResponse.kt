package com.example.zenia.pojo


import com.google.gson.annotations.SerializedName

data class PostTransferResponse(
    @SerializedName("error")
    var error: String,
    @SerializedName("message")
    var message: String,
    @SerializedName("path")
    var path: String,
    @SerializedName("status")
    var status: Int,
    @SerializedName("timestamp")
    var timestamp: String
)