package com.example.zenia.pojo


import com.google.gson.annotations.SerializedName

data class GetDetailTabunganResponse(
    @SerializedName("code")
    val code: String,
    @SerializedName("data")
    val `data`: Data,
    @SerializedName("message")
    val message: String,
    @SerializedName("status")
    val status: String
) {
    data class Data(
        @SerializedName("dateTarget")
        val dateTarget: String,
        @SerializedName("frequency")
        val frequency: String,
        @SerializedName("id")
        val id: String,
        @SerializedName("initialDeposit")
        val initialDeposit: String,
        @SerializedName("installmentDeposit")
        val installmentDeposit: String,
        @SerializedName("method")
        val method: String,
        @SerializedName("percentageTarget")
        val percentageTarget: Double,
        @SerializedName("savingPlanImageUrl")
        val savingPlanImageUrl: Any,
        @SerializedName("savingTarget")
        val savingTarget: String,
        @SerializedName("status")
        val status: String,
        @SerializedName("title")
        val title: String,
        @SerializedName("totalDeposit")
        val totalDeposit: String,
        @SerializedName("transactions")
        val transactions: List<Transactions>
    ) {
        data class Transactions(
            @SerializedName("createdDate")
            val createdDate: Any,
            @SerializedName("id")
            val id: String,
            @SerializedName("totalPayment")
            val totalPayment: Int,
            @SerializedName("transactionCode")
            val transactionCode: String,
            @SerializedName("transactionName")
            val transactionName: String
        )
    }
}