package com.example.zenia.pojo


import com.google.gson.annotations.SerializedName

data class PostAuthLoginBody(
    @SerializedName("username")
    val username: String,
    @SerializedName("password")
    val password: String
)