package com.example.zenia.pojo


import com.google.gson.annotations.SerializedName

data class GetAllNotifikasiResponse(
    @SerializedName("code")
    val code: String,
    @SerializedName("data")
    val `data`: List<Data>,
    @SerializedName("message")
    val message: String,
    @SerializedName("status")
    val status: String
) {
    data class Data(
        @SerializedName("date")
        val date: String,
        @SerializedName("id")
        val id: String,
        @SerializedName("mobileDescription")
        val mobileDescription: String,
        @SerializedName("mobileTitle")
        val mobileTitle: String,
        @SerializedName("notificationType")
        val notificationType: String,
        @SerializedName("onReadStatus")
        val onReadStatus: Boolean,
        @SerializedName("type")
        val type: String
    )
}