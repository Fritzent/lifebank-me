package com.example.zenia.pojo


import com.google.gson.annotations.SerializedName

data class PostFileResponse(
    @SerializedName("code")
    val code: String,
    @SerializedName("data")
    val `data`: Data,
    @SerializedName("message")
    val message: String,
    @SerializedName("status")
    val status: String
) {
    data class Data(
        @SerializedName("fileUrl")
        val fileUrl: String
    )
}