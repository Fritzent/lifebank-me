package com.example.zenia.pojo


import com.google.gson.annotations.SerializedName

data class PostTransferBody(
    @SerializedName("account_name_destination")
    var accountNameDestination: String,
    @SerializedName("account_name_sender")
    var accountNameSender: String,
    @SerializedName("account_number_destination")
    var accountNumberDestination: String,
    @SerializedName("account_number_sender")
    var accountNumberSender: String,
    @SerializedName("amount")
    var amount: Int,
    @SerializedName("description")
    var description: String,
    @SerializedName("pin")
    var pin: String
)