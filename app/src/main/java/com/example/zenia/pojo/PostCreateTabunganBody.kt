package com.example.zenia.pojo


import com.google.gson.annotations.SerializedName

data class PostCreateTabunganBody(
    @SerializedName("dateTarget")
    val dateTarget: String,
    @SerializedName("frequency")
    val frequency: String,
    @SerializedName("initialDeposit")
    val initialDeposit: Int,
    @SerializedName("installmentDeposit")
    val installmentDeposit: Int,
    @SerializedName("method")
    val method: String,
    @SerializedName("savingPlanImageUrl")
    val savingPlanImageUrl: String,
    @SerializedName("savingTarget")
    val savingTarget: Int,
    @SerializedName("title")
    val title: String,
    @SerializedName("type")
    val type: String
)