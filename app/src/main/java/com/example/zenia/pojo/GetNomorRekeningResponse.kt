package com.example.zenia.pojo


import com.google.gson.annotations.SerializedName

data class GetNomorRekeningResponse(
    @SerializedName("code")
    val code: String,
    @SerializedName("data")
    val `data`: List<Data>,
    @SerializedName("message")
    val message: String,
    @SerializedName("status")
    val status: String
) {
    data class Data(
        @SerializedName("name")
        val name: String,
        @SerializedName("rekeningNumber")
        val rekeningNumber: String
    )
}