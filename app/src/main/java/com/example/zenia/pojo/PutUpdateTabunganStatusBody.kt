package com.example.zenia.pojo


import com.google.gson.annotations.SerializedName

data class PutUpdateTabunganStatusBody(
    @SerializedName("status")
    val status: String
)