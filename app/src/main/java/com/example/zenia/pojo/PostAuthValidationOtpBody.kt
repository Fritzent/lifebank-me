package com.example.zenia.pojo


import com.google.gson.annotations.SerializedName

data class PostAuthValidationOtpBody(
    @SerializedName("code")
    val code: String
)