package com.example.zenia.pojo


import com.google.gson.annotations.SerializedName

data class GetAllTabunganResponse(
    @SerializedName("code")
    val code: String,
    @SerializedName("data")
    val `data`: List<Data>,
    @SerializedName("message")
    val message: String,
    @SerializedName("status")
    val status: String
) {
    data class Data(
        @SerializedName("dateTarget")
        val dateTarget: String,
        @SerializedName("id")
        val id: String,
        @SerializedName("percentageTarget")
        val percentageTarget: Double,
        @SerializedName("savingTarget")
        val savingTarget: String,
        @SerializedName("status")
        val status: String,
        @SerializedName("title")
        val title: String,
        @SerializedName("totalDeposit")
        val totalDeposit: String,
        @SerializedName("savingPlanImageUrl")
        val savingPlanImageUrl: Any
    )
}