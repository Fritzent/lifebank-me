package com.example.zenia.pojo


import com.google.gson.annotations.SerializedName

data class PostAuthRegisterBody(
    @SerializedName("accountStatus")
    val accountStatus: String,
    @SerializedName("address")
    val address: String,
    @SerializedName("bornDate")
    val bornDate: String,
    @SerializedName("email")
    val email: String,
    @SerializedName("gender")
    val gender: String,
    @SerializedName("job")
    val job: String,
    @SerializedName("ktpNumber")
    val ktpNumber: String,
    @SerializedName("name")
    val name: String,
    @SerializedName("npwpNumber")
    val npwpNumber: String,
    @SerializedName("password")
    val password: String,
    @SerializedName("phoneNumber")
    val phoneNumber: String,
    @SerializedName("rekeningNumber")
    val rekeningNumber: String,
    @SerializedName("roles")
    val roles: String,
    @SerializedName("urlKtp")
    val urlKtp: String,
    @SerializedName("urlSelfie")
    val urlSelfie: String,
    @SerializedName("username")
    val username: String
)