package com.example.zenia.pojo


import com.google.gson.annotations.SerializedName

data class PutCompilationRegistrationBody(
    @SerializedName("bornDate")
    val bornDate: String,
    @SerializedName("ktpNumber")
    val ktpNumber: String,
    @SerializedName("name")
    val name: String,
    @SerializedName("npwpNumber")
    val npwpNumber: String,
    @SerializedName("password")
    val password: String,
    @SerializedName("pin")
    val pin: String,
    @SerializedName("urlKtp")
    val urlKtp: String,
    @SerializedName("urlSelfie")
    val urlSelfie: String,
    @SerializedName("username")
    val username: String
)