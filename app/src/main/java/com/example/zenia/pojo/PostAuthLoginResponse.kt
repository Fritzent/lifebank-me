package com.example.zenia.pojo


import com.google.gson.annotations.SerializedName

data class PostAuthLoginResponse(
    @SerializedName("code")
    val code: String,
    @SerializedName("data")
    val `data`: Data,
    @SerializedName("message")
    val message: String,
    @SerializedName("status")
    val status: String
) {
    data class Data(
        @SerializedName("accessToken")
        val accessToken: String,
        @SerializedName("email")
        val email: String,
        @SerializedName("id")
        val id: String,
        @SerializedName("pin")
        val pin: Any,
        @SerializedName("roles")
        val roles: List<String>,
        @SerializedName("tokenType")
        val tokenType: String,
        @SerializedName("username")
        val username: String
    )
}