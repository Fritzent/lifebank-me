package com.example.zenia.pojo


import com.google.gson.annotations.SerializedName

data class PostTransferGetUserBody(
    @SerializedName("accountNumber")
    val accountNumber: String
)