package com.example.zenia.pojo


import com.google.gson.annotations.SerializedName

data class PutUpdateTabunganStatusResponse(
    @SerializedName("code")
    val code: String,
    @SerializedName("message")
    val message: String,
    @SerializedName("status")
    val status: String
)