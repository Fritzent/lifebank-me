package com.example.zenia.pojo


import com.google.gson.annotations.SerializedName

data class PutUpdateTabunganResponse(
    @SerializedName("code")
    val code: String,
    @SerializedName("message")
    val message: String,
    @SerializedName("notification")
    val notification: Any,
    @SerializedName("status")
    val status: String
)