package com.example.zenia.pojo


import com.google.gson.annotations.SerializedName

data class GetRequestNewOtpResponse(
    @SerializedName("code")
    val code: String,
    @SerializedName("message")
    val message: String,
    @SerializedName("status")
    val status: String
)