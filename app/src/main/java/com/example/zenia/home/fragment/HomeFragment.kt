package com.example.zenia.home.fragment

import android.content.Context
import android.content.Intent
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.GridLayoutManager
import com.example.zenia.R
import com.example.zenia.authentication.login.LoginActivity
import com.example.zenia.bantuan.view.menuBantuan.MenuBantuanActivity
import com.example.zenia.home.activity.HomePresenter
import com.example.zenia.home.activity.adapter.AdapterInfo
import com.example.zenia.home.activity.adapter.AdapterKenalan
import com.example.zenia.home.activity.adapter.AdapterMenu
import com.example.zenia.home.activity.dataclass.InfoClass
import com.example.zenia.home.activity.dataclass.KenalanClass
import com.example.zenia.home.activity.dataclass.MenuClass
import kotlinx.android.synthetic.main.fragment_home.*

class HomeFragment(val presenter: HomePresenter) : Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        //TODO Note : disini code untuk handling di home kalau gk mau pake onViewCreated
        return inflater.inflate(R.layout.fragment_home, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val sharedPreferences = activity?.getSharedPreferences(LoginActivity.SP_NAME, Context.MODE_PRIVATE)
        val username = sharedPreferences?.getString("username", "No Data")

        TvNamaPengguna.text = username

        val menuList = arrayListOf(
            MenuClass(
                "Transfer",
                R.drawable.menu_transer
            ),
            MenuClass(
                "Top-Up",
                R.drawable.menu_topup
            ),
            MenuClass(
                "Paket Data",
                R.drawable.menu_paketdata
            ),
            MenuClass(
                "Tiket",
                R.drawable.menu_tiket
            ),
            MenuClass(
                "Kalkulator",
                R.drawable.menu_kalkulator
            ),
            MenuClass(
                "Tabungan",
                R.drawable.menu_tabungan
            ),
            MenuClass(
                "KPR",
                R.drawable.menu_kpr
            ),
            MenuClass(
                "Bantuan",
                R.drawable.menu_bantuan
            )
        )

        val infoList = arrayListOf(
            InfoClass(R.drawable.banner_info_1),
            InfoClass(R.drawable.banner_info_2),
            InfoClass(R.drawable.banner_info_3)
        )

        val kenalanList = arrayListOf(
            KenalanClass(R.drawable.banner_kenalan_1),
            KenalanClass(R.drawable.banner_kenalan_2),
            KenalanClass(R.drawable.banner_kenalan_3),
            KenalanClass(R.drawable.banner_kenalan_4),
            KenalanClass(R.drawable.banner_kenalan_5),
            KenalanClass(R.drawable.banner_kenalan_6),
            KenalanClass(R.drawable.banner_kenalan_7)
        )

        TvSelengkapnya.setOnClickListener {
            startActivity(Intent(activity, MenuBantuanActivity::class.java))
        }

        //set adapter menu
        rvContainerMenu.layoutManager = GridLayoutManager(activity, 4, GridLayoutManager.VERTICAL,false)
        rvContainerMenu.adapter =
            AdapterMenu(menuList)

        //set adapter banner info promo
        rvContainerInfo.layoutManager = GridLayoutManager(activity, 1, GridLayoutManager.HORIZONTAL,false)
        rvContainerInfo.adapter =
            AdapterInfo(infoList, presenter)

        //set adapter banner info kenalan
        rvContainerKenalan.layoutManager = GridLayoutManager(activity, 1, GridLayoutManager.HORIZONTAL,false)
        rvContainerKenalan.adapter =
            AdapterKenalan(kenalanList, presenter)

    }
}