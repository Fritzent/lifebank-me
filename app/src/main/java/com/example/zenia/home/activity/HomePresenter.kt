package com.example.zenia.home.activity


class HomePresenter (val listener: Listener) {
    interface Listener{
        //Listener Info Promo
        fun goToInfoPromoTabungan()
        fun goToInfoPromoKalkulator()
        fun goToInfoPromoKpr()

        //Listener Info Kenalan
        fun goToInfoKenalanPaketData()
        fun goToInfoKenalanTransfer()
        fun goToInfoKenalanTopUp()
        fun goToInfoKenalanTiket()
        fun goToInfoKenalanKalkulator()
        fun goToInfoKenalanKpr()
        fun goToInfoKenalanTabungan()
    }

    fun goToInfoPromo (position: Int){
        when (position) {
            0 -> {
               listener.goToInfoPromoTabungan()
            }
            1 -> {
                listener.goToInfoPromoKalkulator()
            }
            2 -> {
                listener.goToInfoPromoKpr()
            }
            else -> {

            }
        }
    }

    fun goToInfoKenalan (position: Int){
        when (position) {
            0 -> {
                listener.goToInfoKenalanTabungan()
            }
            1 -> {
                listener.goToInfoKenalanKpr()
            }
            2 -> {
                listener.goToInfoKenalanTopUp()
            }

            3 -> {
                listener.goToInfoKenalanTiket()
            }
            4 -> {
                listener.goToInfoKenalanTransfer()
            }
            5 -> {
                listener.goToInfoKenalanKalkulator()
            }

            6 -> {
                listener.goToInfoKenalanPaketData()
            }
            else -> {

            }
        }

    }



}
