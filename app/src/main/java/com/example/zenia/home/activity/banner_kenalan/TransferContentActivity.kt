package com.example.zenia.home.activity.banner_kenalan

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.example.zenia.R

class TransferContentActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_transfer_content)

        try {
            this.supportActionBar?.hide()
        } catch (e: NullPointerException) {}
    }
}