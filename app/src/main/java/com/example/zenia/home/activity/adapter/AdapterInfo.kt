package com.example.zenia.home.activity.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.example.zenia.R
import com.example.zenia.home.activity.HomePresenter
import com.example.zenia.home.activity.dataclass.InfoClass
import kotlinx.android.synthetic.main.info_item.view.*

class AdapterInfo(private val listInfo: ArrayList<InfoClass>, val presenter: HomePresenter): RecyclerView.Adapter<AdapterInfo.ViewHolder>() {

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.info_item, parent, false)

        return ViewHolder(
            view
        )
    }

    override fun getItemCount(): Int {
        return listInfo.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val image = holder.itemView.ivInfo

        Glide.with(holder.itemView.context)
            .load(listInfo[position].bannerInfo)
            .into(image)

        holder.itemView.setOnClickListener {
            presenter.goToInfoPromo(position)
        }

    }
}