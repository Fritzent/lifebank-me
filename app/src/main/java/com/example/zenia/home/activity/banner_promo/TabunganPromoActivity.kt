package com.example.zenia.home.activity.banner_promo

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.example.zenia.R

class TabunganPromoActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_tabungan_promo)

        try {
            this.supportActionBar?.hide()
        } catch (e: NullPointerException) {}
    }
}