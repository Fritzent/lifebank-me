package com.example.zenia.home.activity.banner_kenalan

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.example.zenia.R

class KalkulatorContentActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_kalkulator_content)

        try {
            this.supportActionBar?.hide()
        } catch (e: NullPointerException) {}
    }
}