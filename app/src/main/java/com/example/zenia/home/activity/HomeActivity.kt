package com.example.zenia.home.activity

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.util.Log
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.FragmentTransaction
import com.example.zenia.R
import com.example.zenia.authentication.login.LoginActivity
import com.example.zenia.databinding.ActivityHomeBinding
import com.example.zenia.home.activity.banner_kenalan.*
import com.example.zenia.home.activity.banner_promo.KalkulatorPromoActivity
import com.example.zenia.home.activity.banner_promo.KprPromoActivity
import com.example.zenia.home.activity.banner_promo.TabunganPromoActivity
import com.example.zenia.home.fragment.HomeFragment
import com.example.zenia.inbox.view.fragment.InboxFragment
import com.example.zenia.infoRekening.view.fragment.InfoRekeningFragment
import com.example.zenia.lainnya.view.menuLainnya.MenuLainnyaFragment

class HomeActivity : AppCompatActivity(), HomePresenter.Listener {

    lateinit var presenter: HomePresenter

    private lateinit var binding : ActivityHomeBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityHomeBinding.inflate(layoutInflater)
        val view = binding.root
        setContentView(view)

        presenter = HomePresenter(this)

        val botNav = binding.bottomNavigation

        try {
            this.supportActionBar?.hide()
        } catch (e: NullPointerException) {}

        val sharedPreferences = getSharedPreferences(LoginActivity.SP_NAME, Context.MODE_PRIVATE)
        val token = sharedPreferences.getString("token", "No Data")
        token?.let { Log.d("Token Home", it) }

        val homeFragment = HomeFragment(presenter)
        val inboxFragment = token?.let { InboxFragment.newInstance(it) }



        //default fragment
        supportFragmentManager
            .beginTransaction()
            .replace(R.id.frameHomecontainer, homeFragment)
            .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
            .commit()

        botNav.setOnNavigationItemSelectedListener {
            botNav.itemIconTintList = null
            return@setOnNavigationItemSelectedListener when (it.itemId) {
                R.id.navBeranda -> {
                    Log.d("Tujuan", "Beranda")
                    supportFragmentManager
                        .beginTransaction()
                        .replace(R.id.frameHomecontainer, homeFragment)
                        .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
                        .commit()
                    true
                }
                R.id.navRekening -> {
                    Log.d("Tujuan", "Rekening")
                    supportFragmentManager
                        .beginTransaction()
                        .replace(R.id.frameHomecontainer, InfoRekeningFragment())
                        .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
                        .commit()
                    true
                }

                R.id.navInbox -> {
                    Log.d("Tujuan", "Inbox")
                    inboxFragment?.let { it1 ->
                        supportFragmentManager
                            .beginTransaction()
                            .replace(R.id.frameHomecontainer, it1)
                            .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
                            .commit()
                    }
                    true
                }

                R.id.navLainnya -> {
                    supportFragmentManager
                        .beginTransaction()
                        .replace(R.id.frameHomecontainer, MenuLainnyaFragment())
                        .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
                        .commit()
                    true
                }

                else -> false
            }
        }
    }

    private var doubleBackToExitPressedOnce = false
    override fun onBackPressed() {
        if(doubleBackToExitPressedOnce){
            moveTaskToBack(true)
        }
        this.doubleBackToExitPressedOnce = true
        Toast.makeText(this, "Tekan lagi untuk keluar", Toast.LENGTH_SHORT).show()

        @Suppress("DEPRECATION")
        Handler().postDelayed({ doubleBackToExitPressedOnce = false }, 2000)
    }

    override fun goToInfoPromoTabungan() {
        val intent = Intent(this, TabunganPromoActivity::class.java)
        startActivity(intent)
    }

    override fun goToInfoPromoKalkulator() {
        val intent = Intent(this, KalkulatorPromoActivity::class.java)
        startActivity(intent)
    }

    override fun goToInfoPromoKpr() {
        val intent = Intent(this, KprPromoActivity::class.java)
        startActivity(intent)
    }

    //info kenalan
    override fun goToInfoKenalanPaketData() {
        val intent = Intent(this, PaketDataContentActivity::class.java)
        startActivity(intent)
    }

    override fun goToInfoKenalanTransfer() {
        val intent = Intent(this, TransferContentActivity::class.java)
        startActivity(intent)
    }

    override fun goToInfoKenalanTopUp() {
        val intent = Intent(this, TopUpContentActivity::class.java)
        startActivity(intent)
    }

    override fun goToInfoKenalanTiket() {
        val intent = Intent(this, TiketContentActivity::class.java)
        startActivity(intent)
    }

    override fun goToInfoKenalanKalkulator() {
        val intent = Intent(this, KalkulatorContentActivity::class.java)
        startActivity(intent)
    }

    override fun goToInfoKenalanKpr() {
        val intent = Intent(this, KprContentActivity::class.java)
        startActivity(intent)
    }

    override fun goToInfoKenalanTabungan() {
        val intent = Intent(this, TabunganContentActivity::class.java)
        startActivity(intent)
    }

}