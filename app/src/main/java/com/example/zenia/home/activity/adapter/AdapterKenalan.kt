package com.example.zenia.home.activity.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.example.zenia.R
import com.example.zenia.home.activity.HomePresenter
import com.example.zenia.home.activity.dataclass.KenalanClass
import kotlinx.android.synthetic.main.kenalan_item.view.*

class AdapterKenalan(private val listKenalan: ArrayList<KenalanClass>, val presenter: HomePresenter) : RecyclerView.Adapter<AdapterKenalan.ViewHolder>() {

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.kenalan_item, parent, false)

        return ViewHolder(
            view
        )
    }

    override fun getItemCount(): Int {
        return listKenalan.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val image = holder.itemView.ivKenalan

        Glide.with(holder.itemView.context)
            .load(listKenalan[position].bannerKenalan)
            .into(image)

        holder.itemView.setOnClickListener {
            presenter.goToInfoKenalan(position)
        }
    }
}