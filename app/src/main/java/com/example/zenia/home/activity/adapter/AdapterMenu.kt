package com.example.zenia.home.activity.adapter

import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.example.zenia.R
import com.example.zenia.bantuan.view.menuBantuan.MenuBantuanActivity
import com.example.zenia.home.activity.dataclass.MenuClass
import com.example.zenia.kalkulator.menu.view.MenuKalkulatorActivity
import com.example.zenia.kpr.OnBoardingKprActivity
import com.example.zenia.tabungan.TabunganActivity
import com.example.zenia.transfer.jenis_transfer.JenisTransferActivity
import kotlinx.android.synthetic.main.menu_item.view.*

class AdapterMenu(private val listMenu: ArrayList<MenuClass>): RecyclerView.Adapter<AdapterMenu.ViewHolder>() {

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.menu_item, parent, false)

        return ViewHolder(
            view
        )
    }

    override fun getItemCount(): Int {
        return listMenu.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val image = holder.itemView.ivMenu

        Glide.with(holder.itemView.context)
            .load(listMenu[position].icon)
            .into(image)

        holder.itemView.tvName.text = listMenu[position].name

        var menuStart = 1

        when {
            menuStart -1 == position -> {
                //here code to setOnClick In Menu RecyclerView
                holder.itemView.layout_menu_click.setOnClickListener {
                    it.context.startActivity(Intent(it.context.applicationContext, JenisTransferActivity::class.java))
                }
            }
            menuStart == position -> {
                holder.itemView.layout_menu_click.setOnClickListener {
                    it.context.startActivity(Intent(it.context.applicationContext, JenisTransferActivity::class.java))
                }
            }
            menuStart +1 == position -> {
                //here code to setOnClick In Menu RecyclerView
                holder.itemView.layout_menu_click.setOnClickListener {
                    it.context.startActivity(Intent(it.context.applicationContext, JenisTransferActivity::class.java))
                }
            }
            menuStart +2 == position -> {
                //here code to setOnClick In Menu RecyclerView
                holder.itemView.layout_menu_click.setOnClickListener {
                    it.context.startActivity(Intent(it.context.applicationContext, JenisTransferActivity::class.java))
                }
            }
            menuStart +3 == position -> {
                //here code to setOnClick In Menu RecyclerView
                holder.itemView.layout_menu_click.setOnClickListener {
                    it.context.startActivity(Intent(it.context.applicationContext, MenuKalkulatorActivity::class.java))
                }
            }
            menuStart +4 == position -> {
                //here code to setOnClick In Menu RecyclerView
                holder.itemView.layout_menu_click.setOnClickListener {
                    it.context.startActivity(Intent(it.context.applicationContext, TabunganActivity::class.java))
                }
            }
            menuStart +5 == position -> {
                //here code to setOnClick In Menu RecyclerView
                holder.itemView.layout_menu_click.setOnClickListener {
                    it.context.startActivity(Intent(it.context.applicationContext, OnBoardingKprActivity::class.java))
                }
            }
            menuStart +6 == position -> {
                holder.itemView.layout_menu_click.setOnClickListener {
                    it.context.startActivity(Intent(it.context.applicationContext, MenuBantuanActivity::class.java))
                }
            }
        }
    }
}