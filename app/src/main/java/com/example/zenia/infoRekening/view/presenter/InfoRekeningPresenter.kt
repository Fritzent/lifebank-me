package com.example.zenia.infoRekening.view.presenter

import android.content.Context
import android.util.Log
import com.example.zenia.R
import com.example.zenia.home.activity.HomeActivity
import com.example.zenia.infoRekening.model.DataMutasi
import com.example.zenia.infoRekening.model.DataTransaksi
import com.gkemon.XMLtoPDF.PdfGenerator
import com.gkemon.XMLtoPDF.PdfGeneratorListener
import com.gkemon.XMLtoPDF.model.FailureResponse
import com.gkemon.XMLtoPDF.model.SuccessResponse
import java.util.*
import java.util.concurrent.TimeUnit

class InfoRekeningPresenter(val listener: Listener) {
    interface Listener {
        fun showNoData()
        fun showRecycle()
        fun showTanggalAwal(text: String)
        fun showTanggalAkhir(text: String)
        fun showNotif()
        fun dismissNotif()
        fun showNotif2()
        fun logTanggal(text: String)
    }

    private var tanggalAwal = Calendar.getInstance().time
    private var tanggalAkhir = Calendar.getInstance().time

    private var itemList = mutableListOf<DataMutasi>()

    fun generatePDF(context : Context, activity: HomeActivity){
        PdfGenerator.getBuilder()
            .setContext(context)
            .fromViewIDSource()
            .fromViewID(activity, R.id.fragmentInfoRekening) /* "fromViewID()" takes array of view ids those MUST BE and MUST BE contained in the inserted "activity" .
    * You can also invoke "fromViewIDList()" method here which takes list of view ids instead of array. */
            .setCustomPageSize(
                1440,
                2960
            ) /* Here I used ".setCustomPageSize(3000,3000)" to set custom page size.*/
            .setFileName("Mutasi Rekening ${Calendar.getInstance()}")
            .setFolderName("Zenia")
            .openPDFafterGeneration(true)
            .build(object : PdfGeneratorListener() {
                override fun onFailure(failureResponse: FailureResponse) {
                    super.onFailure(failureResponse)
                }

                override fun onSuccess(response: SuccessResponse) {
                    super.onSuccess(response)
                }
            })
    }

    fun setTanggalAwal(year: Int, month: Int, day: Int) {
        val bulan = bulanString(month)
        val date = "$day $bulan  $year"
        listener.showTanggalAwal(date)


        val cal = Calendar.getInstance()
        cal.set(year, month, day)
        tanggalAwal = cal.time

        cekDiff()

    }

    fun setTanggalAkhir(year: Int, month: Int, day: Int) {
        val bulan = bulanString(month)
        val date = "$day $bulan  $year"
        listener.showTanggalAkhir(date)

        val cal = Calendar.getInstance()
        cal.set(year, month, day)
        tanggalAkhir = cal.time

        cekDiff()

    }

    private fun bulanString(bulan: Int): String {
        return when (bulan + 1) {
            1 -> "Jan"
            2 -> "Feb"
            3 -> "Mar"
            4 -> "Apr"
            5 -> "Mei"
            6 -> "Jun"
            7 -> "Jul"
            8 -> "Agu"
            9 -> "Sep"
            10 -> "Okt"
            11 -> "Nov"
            12 -> "Des"
            else -> ""
        }
    }

    fun cekDiff() {
        Log.d("tanggal", "$tanggalAwal, $tanggalAkhir")
        listener.logTanggal("$tanggalAwal, $tanggalAkhir")

        val dif = tanggalAkhir.time - tanggalAwal.time
        val days = TimeUnit.DAYS.convert(dif, TimeUnit.MILLISECONDS)
        Log.d("Tanggal", "$days, $dif")

        when {
            tanggalAkhir.time < tanggalAwal.time -> {
                listener.showNotif2()
            }
            days > 31 -> {
                listener.showNotif()
            }
            else -> {
                listener.dismissNotif()
            }
        }
    }

    fun dataSet(): MutableList<DataMutasi> {
        val dataTransaksi1 = DataTransaksi("ABCD", "- Rp 50.000")
        val dataTransaksi2 = DataTransaksi("EFGH", "+ Rp 50.000")

        val item1 = DataMutasi(null, true, "15 Sep 2020")
        val item2 = DataMutasi(dataTransaksi1, false, null)
        val item3 = DataMutasi(dataTransaksi1, false, null)
        val item4 = DataMutasi(null, true, "15 Sep 2020")
        val item5 = DataMutasi(dataTransaksi2, false, null)
        val item6 = DataMutasi(dataTransaksi2, false, null)

        itemList.add(item1)
        itemList.add(item2)
        itemList.add(item3)
        itemList.add(item4)
        itemList.add(item5)
        itemList.add(item6)
        itemList.add(item1)
        itemList.add(item2)
        itemList.add(item3)
        itemList.add(item4)
        itemList.add(item5)
        itemList.add(item6)

        listener.showRecycle()

        return itemList
    }
}