package com.example.zenia.infoRekening.model

data class DataTransaksi (val info:String, val nominalTransaksi:String)

data class DataMutasi(var dataTransaksi: DataTransaksi?, var isHeader: Boolean, var tanggalTransaksi: String?)
