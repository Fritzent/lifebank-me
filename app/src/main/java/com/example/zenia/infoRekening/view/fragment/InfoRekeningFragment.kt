package com.example.zenia.infoRekening.view.fragment

import android.app.DatePickerDialog
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.provider.SyncStateContract
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.zenia.R
import com.example.zenia.home.activity.HomeActivity
import com.example.zenia.infoRekening.view.adapter.MyAdapter
import com.example.zenia.infoRekening.view.presenter.InfoRekeningPresenter
import com.gkemon.XMLtoPDF.PdfGenerator
import com.gkemon.XMLtoPDF.PdfGeneratorListener
import com.gkemon.XMLtoPDF.model.FailureResponse
import com.gkemon.XMLtoPDF.model.SuccessResponse
import kotlinx.android.synthetic.main.fragment_info_rekening.*
import java.util.*


// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 * Use the [InfoRekeningFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class InfoRekeningFragment : Fragment(), InfoRekeningPresenter.Listener {
    // TODO: Rename and change types of parameters
    private var param1: String? = null
    private var param2: String? = null

    private lateinit var recyclerView: RecyclerView
    private lateinit var viewManager: RecyclerView.LayoutManager

    private lateinit var presenter: InfoRekeningPresenter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            param1 = it.getString(ARG_PARAM1)
            param2 = it.getString(ARG_PARAM2)
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_info_rekening, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        presenter = InfoRekeningPresenter(this)

        notif.visibility = View.GONE

        viewManager = LinearLayoutManager(this.context)
        val myAdapter = MyAdapter(presenter.dataSet())

        recyclerView = rvMutasiRekening.apply {
            setHasFixedSize(true)
            layoutManager = viewManager
            adapter = myAdapter
            visibility = View.VISIBLE
        }

        btnTanggalAwal.setOnClickListener {
            val cal: Calendar = Calendar.getInstance()
            val year: Int = cal.get(Calendar.YEAR)
            val month: Int = cal.get(Calendar.MONTH)
            val day: Int = cal.get(Calendar.DAY_OF_MONTH)

            val dialog = activity?.let { it1 ->
                DatePickerDialog(
                    it1,
                    android.R.style.Theme_Holo_Light_Dialog_NoActionBar_MinWidth,
                    { _, year, month, day ->
                        presenter.setTanggalAwal(year, month, day)
                    },
                    year, month, day
                )
            }



            cal.add(Calendar.DATE, -31)
            dialog?.datePicker?.minDate = cal.timeInMillis

            cal.add(Calendar.DATE, 31)
            dialog?.datePicker?.maxDate=cal.timeInMillis

            dialog?.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
            dialog.show()

            presenter.cekDiff()
        }

        btnTanggalAkhir.setOnClickListener {
            val cal: Calendar = Calendar.getInstance()
            val year: Int = cal.get(Calendar.YEAR)
            val month: Int = cal.get(Calendar.MONTH)
            val day: Int = cal.get(Calendar.DAY_OF_MONTH)

            val dialog = activity?.let { it1 ->
                DatePickerDialog(
                    it1,
                    android.R.style.Theme_Holo_Light_Dialog_NoActionBar_MinWidth,
                    { _, year, month, day ->
                        presenter.setTanggalAkhir(year, month, day)
                    },
                    year, month, day
                )
            }

            cal.add(Calendar.DATE, -31)
            dialog?.datePicker?.minDate = cal.timeInMillis

            cal.add(Calendar.DATE, 31)
            dialog?.datePicker?.maxDate=cal.timeInMillis

            dialog?.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
            dialog.show()
        }

        btnDownload.setOnClickListener {
            val cal: Calendar = Calendar.getInstance()
            val time = cal.time

            val stageWidth = layoutSave.width
            val stageHeight = layoutSave.height

            btnDownload.visibility = View.GONE

            Log.d("ukuran layar", "$stageHeight $stageWidth")

            PdfGenerator.getBuilder()
                .setContext(context)
                .fromViewIDSource()
                .fromViewID((activity as HomeActivity), R.id.layoutSave) /* "fromViewID()" takes array of view ids those MUST BE and MUST BE contained in the inserted "activity" .
                                            * You can also invoke "fromViewIDList()" method here which takes list of view ids instead of array. */
                .setCustomPageSize(
                    stageWidth,
                    stageHeight
                ) /* Here I used ".setCustomPageSize(3000,3000)" to set custom page size.*/
                .setFileName("Mutasi Rekening $time")
                .setFolderName("Zenia")
                .openPDFafterGeneration(true)
                .build(object : PdfGeneratorListener() {
                    override fun onFailure(failureResponse: FailureResponse) {
                        super.onFailure(failureResponse)
                        Toast.makeText(activity, "Gagal", Toast.LENGTH_LONG).show()
                    }

                    override fun showLog(log: String?) {
                        super.showLog(log)

                        Log.d("Log Pdf", log.toString())
                    }

                    override fun onSuccess(response: SuccessResponse) {
                        super.onSuccess(response)
                        btnDownload.visibility = View.VISIBLE

                        Toast.makeText(activity, "Berhasil", Toast.LENGTH_LONG).show()
                    }
                })

        }


    }

    companion object {
        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         *
         * @param param1 Parameter 1.
         * @param param2 Parameter 2.
         * @return A new instance of fragment InfoRekeningFragment.
         */
        // TODO: Rename and change types and number of parameters
        @JvmStatic
        fun newInstance(param1: String, param2: String) =
            InfoRekeningFragment().apply {
                arguments = Bundle().apply {
                    putString(ARG_PARAM1, param1)
                    putString(ARG_PARAM2, param2)
                }
            }
    }

    override fun showNoData() {
        noDataLayout.visibility = View.VISIBLE
        rvMutasiRekening.visibility = View.GONE
    }

    override fun showRecycle() {
        noDataLayout.visibility = View.GONE
        rvMutasiRekening.visibility = View.VISIBLE
    }

    override fun showTanggalAwal(text: String) {
        tvTanggalAwal.text = text
    }

    override fun showTanggalAkhir(text: String) {
        tvTanggalAkhir.text = text
    }

    override fun showNotif() {
        notif.visibility = View.VISIBLE
        notif.text = "Maksimum periode pencarian mutasi rekening adalah 31 hari!"
    }

    override fun dismissNotif() {
        notif.visibility = View.GONE
    }

    override fun showNotif2() {
        notif.visibility = View.VISIBLE
        notif.text = "Tanggal Awal tidak bisa lebih besar dari tanggal kecil"
    }

    override fun logTanggal(text: String) {
        Log.d("tanggal", text)
    }
}
