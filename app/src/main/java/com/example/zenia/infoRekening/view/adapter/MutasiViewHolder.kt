package com.example.zenia.infoRekening.view.adapter

import android.graphics.BitmapFactory
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.annotation.IdRes
import androidx.recyclerview.widget.RecyclerView
import com.example.zenia.infoRekening.model.DataTransaksi

class MyViewHolder (itemView: View) : RecyclerView.ViewHolder(itemView) {


    private val viewMap: MutableMap<Int, View> = HashMap()

    init {
        findViewItems(itemView)
    }

    private fun findViewItems(itemView: View) {
        addToMap(itemView)
        if (itemView is ViewGroup) {
            val childCount = itemView.childCount
            (0 until childCount)
                .map { itemView.getChildAt(it) }
                .forEach { findViewItems(it) }
        }
    }

    private fun addToMap(itemView: View) {
        viewMap[itemView.id] = itemView
    }

    fun setHeader(@IdRes id: Int, text: String) {

        val view = (viewMap[id] ?: throw IllegalArgumentException("View for $id not found")) as? TextView
            ?: throw IllegalArgumentException("View for $id is not a TextView")
        view.text = text

    }

    fun setItems(item: DataTransaksi, @IdRes infoTRansaksiId: Int,@IdRes nominalTransaksiId: Int) {

        val info = (viewMap[infoTRansaksiId] ?: throw IllegalArgumentException("View for $infoTRansaksiId not found")) as? TextView ?: throw IllegalArgumentException("View for $infoTRansaksiId is not a TextView")
        info.text = item.info

        val nominal = (viewMap[nominalTransaksiId] ?: throw IllegalArgumentException("View for $nominalTransaksiId not found")) as? TextView ?: throw IllegalArgumentException("View for $nominalTransaksiId is not a TextView")
        nominal.text = item.nominalTransaksi

    }
}
