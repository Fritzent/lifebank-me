package com.example.zenia.infoRekening.view.adapter

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.zenia.R
import com.example.zenia.infoRekening.model.DataMutasi

enum class RowType {
    HEADER,
    ROW
}

class MyAdapter(private val dataset: MutableList<DataMutasi>) : RecyclerView.Adapter<MyViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {

        val layoutInflater = LayoutInflater.from(parent.context)

        val inflatedView : View = when (viewType) {
            RowType.ROW.ordinal -> layoutInflater.inflate(R.layout.item_mutasi_rekening, parent,false)
            else -> layoutInflater.inflate(R.layout.header_mutasi_rekening, parent,false)
        }
        return MyViewHolder(inflatedView)
    }

    override fun getItemCount() = dataset.size

    @SuppressLint("ResourceType")
    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {

        val item = dataset[position]

        if (item.isHeader) {
            item.tanggalTransaksi?.let {
                holder.setHeader(R.id.tvTanggalTransaksi, item.tanggalTransaksi!!
            ) }
        }else {
            holder.setItems(item.dataTransaksi!!, R.id.keteranganTransaksi, R.id.nominalTransaksi)
        }

        holder.itemView.setOnClickListener {
            println("Pressed at row $position")
        }
    }

    override fun getItemViewType(position: Int): Int {
        return if (dataset[position].isHeader) {
            0
        }else {
            1
        }
    }

}
