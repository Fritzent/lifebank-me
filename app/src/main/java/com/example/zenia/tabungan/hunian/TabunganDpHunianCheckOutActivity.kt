package com.example.zenia.tabungan.hunian

import android.app.ProgressDialog
import android.content.Context
import android.content.Intent
import android.graphics.Bitmap
import android.os.Bundle
import android.os.Environment
import android.provider.MediaStore
import android.util.Log
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.net.toUri
import com.example.zenia.authentication.login.LoginActivity
import com.example.zenia.authentication.login.LoginActivity.Companion.TOKEN
import com.example.zenia.databinding.ActivityTabunganDpHunianCheckOutBinding
import com.example.zenia.network.ApiClient
import com.example.zenia.pojo.PostCreateTabunganBody
import com.example.zenia.pojo.PostCreateTabunganResponse
import com.example.zenia.pojo.PostFileResponse
import com.example.zenia.tabungan.TabunganActivity
import okhttp3.MediaType.Companion.toMediaTypeOrNull
import okhttp3.MultipartBody
import okhttp3.RequestBody
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.io.ByteArrayOutputStream
import java.io.File
import java.io.FileOutputStream
import java.io.IOException


class TabunganDpHunianCheckOutActivity : AppCompatActivity() {

    private lateinit var binding: ActivityTabunganDpHunianCheckOutBinding
    companion object {
        private const val DATE_TARGET = "2020-09-12"
        private const val TYPE = "hunian"
        private var URI_IMAGE = ""
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityTabunganDpHunianCheckOutBinding.inflate(layoutInflater)
        val view = binding.root
        setContentView(view)

        @Suppress("DEPRECATION")
        val progressDialog = ProgressDialog(this@TabunganDpHunianCheckOutActivity)
        progressDialog.setTitle("Processing")
        @Suppress("DEPRECATION")
        progressDialog.setMessage("Mengupload Data, Tunggu Sebentar")

        binding.iconBack.setOnClickListener {
            finish()
        }

        if(intent.extras != null) {
            val packageIntent = intent.extras

            if (packageIntent != null) {
                binding.dpHunianTabunganTitleCreate.text = packageIntent.getString("Judul_Tabungan")
                binding.dpHunianTabunganPriceTargetCreate.text = packageIntent.getInt("Target_Tabungan").toString()
                binding.metodeSetoranValueCheckout.text = packageIntent.getString("Metode_Setoran")
                binding.frekuensiSetoranValueCheckout.text = packageIntent.getString("Frekuensi_Setoran")
                binding.setoranAwalValueCheckout.text = packageIntent.getLong("Setoran_Awal").toString()
                binding.nominalSetoranValueCheckout.text = packageIntent.getInt("Nominal_Setoran").toString()
                URI_IMAGE = packageIntent.getString("Uri_Image").toString()
                binding.dpHunianTabunganImageCreate.setImageURI(URI_IMAGE.toUri())
            }
        }

        Log.d("CEKING", "This is the URI_IMAGE $URI_IMAGE")
        binding.btnCreateTabungan.setOnClickListener {
            val sharedPreferences = getSharedPreferences(LoginActivity.SP_NAME, Context.MODE_PRIVATE)
            val userId = sharedPreferences.getString(TOKEN, "No Data")

            @Suppress("DEPRECATION")
            val bitmap = MediaStore.Images.Media.getBitmap(this.contentResolver, URI_IMAGE.toUri())
            val fileImage : File = createTempFile(bitmap)

            @Suppress("DEPRECATION")
            val requestFile = RequestBody.create("multipart/form-data".toMediaTypeOrNull(), fileImage)
            val profilePic = MultipartBody.Part.createFormData("file", fileImage.name, requestFile)

            progressDialog.show()

            ApiClient.apiService.uploadFile(profilePic, authHeader = "Bearer $userId").enqueue(
                object : Callback<PostFileResponse> {
                    override fun onFailure(call: Call<PostFileResponse>, t: Throwable) {
                        progressDialog.dismiss()
                        Toast.makeText(applicationContext, "Error Consume API di Upload Gambar", Toast.LENGTH_LONG).show()
                    }

                    override fun onResponse(
                        call: Call<PostFileResponse>,
                        response: Response<PostFileResponse>
                    ) {
                        if(response.isSuccessful && response.body()?.status == "Success") {

                            val objectPut = PostCreateTabunganBody(
                                DATE_TARGET,
                                binding.frekuensiSetoranValueCheckout.text.toString(),
                                binding.setoranAwalValueCheckout.text.toString().toInt(),
                                binding.nominalSetoranValueCheckout.text.toString().toInt(),
                                binding.metodeSetoranValueCheckout.text.toString(),
                                response.body()!!.data.fileUrl,
                                binding.dpHunianTabunganPriceTargetCreate.text.toString().toInt(),
                                binding.dpHunianTabunganTitleCreate.text.toString(),
                                TYPE
                            )

                            if (userId != null) {
                                ApiClient.apiService.createTabunganPost(objectPut, authHeader = "Bearer $userId").enqueue(object :
                                    Callback<PostCreateTabunganResponse> {
                                    override fun onFailure(call: Call<PostCreateTabunganResponse>, t: Throwable) {
                                        progressDialog.dismiss()
                                        Toast.makeText(applicationContext, "Error Consume API", Toast.LENGTH_LONG).show()
                                    }

                                    override fun onResponse(
                                        call: Call<PostCreateTabunganResponse>,
                                        response: Response<PostCreateTabunganResponse>
                                    ) {
//                                        Toast.makeText(applicationContext, "Sukses Consume API", Toast.LENGTH_LONG).show()
                                        Log.d("MBA Nita", "Ini Responsenya ${response.body()}")
                                        progressDialog.dismiss()
                                        startActivity(Intent(applicationContext, TabunganActivity::class.java))
                                        finish()
                                    }
                                })
                            }
                        }
                    }

                })
        }
    }
    private fun createTempFile(bitmap: Bitmap): File {
        val file = File(
            getExternalFilesDir(Environment.DIRECTORY_PICTURES)
            , System.currentTimeMillis().toString() + ".image.jpeg"
        )
        val bos = ByteArrayOutputStream()
        bitmap.compress(Bitmap.CompressFormat.JPEG, 100, bos)
        //here code to handle if the file more than 400kb its while reduce
        var option = 90;
        while (bos.toByteArray().size / 1024 > 400) {   //Loop if compressed picture is greater than 400kb, than to compression
            bos.reset();//Reset baos is empty baos
            bitmap.compress(Bitmap.CompressFormat.JPEG, option, bos);//The compression options%, storing the compressed data to the baos
            option -= 10;//Every time reduced by 10
        }

        val bitmapdata: ByteArray = bos.toByteArray()
        //write the bytes in file
        try {
            val fos = FileOutputStream(file)
            fos.write(bitmapdata)
            fos.flush()
            fos.close()
        } catch (e: IOException) {
            e.printStackTrace()
        }
        return file
    }

}