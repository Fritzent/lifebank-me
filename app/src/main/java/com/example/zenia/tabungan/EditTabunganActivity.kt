package com.example.zenia.tabungan

import android.app.Activity
import android.app.ProgressDialog
import android.content.Context
import android.content.Intent
import android.graphics.Bitmap
import android.net.Uri
import android.os.Bundle
import android.os.Environment
import android.provider.MediaStore
import android.view.View
import android.widget.RadioButton
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.bumptech.glide.Glide
import com.example.zenia.authentication.login.LoginActivity
import com.example.zenia.databinding.ActivityEditTabunganBinding
import com.example.zenia.network.ApiClient
import com.example.zenia.pojo.GetDetailTabunganResponse
import com.example.zenia.pojo.PostFileResponse
import com.example.zenia.pojo.PutUpdateTabunganBody
import com.example.zenia.pojo.PutUpdateTabunganResponse
import com.example.zenia.tabungan.hunian.TabunganDpHunianActivity
import com.google.android.material.snackbar.Snackbar
import com.jaredrummler.materialspinner.MaterialSpinner
import okhttp3.MediaType.Companion.toMediaTypeOrNull
import okhttp3.MultipartBody
import okhttp3.RequestBody
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.io.ByteArrayOutputStream
import java.io.File
import java.io.FileOutputStream
import java.io.IOException


class EditTabunganActivity : AppCompatActivity() {

    private lateinit var binding: ActivityEditTabunganBinding

    companion object {
        private var ITEM_ID = ""
        private var TOKEN = ""
        private lateinit var IMAGE_URI: Uri
        private var TITLE = ""
        private var TARGET_TABUNGAN = 0
        private var METODE_SETORAN = ""
        private var FREKUENSI_TABUNGAN = ""
        private var NOMINAL_SETORAN = 0
        private lateinit var FILE : File
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityEditTabunganBinding.inflate(layoutInflater)
        val view = binding.root
        setContentView(view)

        val packageIntent = intent.extras

        if(packageIntent != null) {
            ITEM_ID = packageIntent.getString("Item_id").toString()
            TOKEN = packageIntent.getString("Token").toString()
        }

        //this is code to get the data first
        ApiClient.apiService.detailTabunganGet(ITEM_ID, authHeader = "Bearer $TOKEN").enqueue(
            object : Callback<GetDetailTabunganResponse> {
                override fun onFailure(call: Call<GetDetailTabunganResponse>, t: Throwable) {
                    Toast.makeText(applicationContext, "Error Consume API", Toast.LENGTH_LONG).show()
                }

                override fun onResponse(call: Call<GetDetailTabunganResponse>, response: Response<GetDetailTabunganResponse>) {
                    if(response.isSuccessful && response.body()?.status == "success") {
                        val savingUrl = response.body()?.data?.savingPlanImageUrl

                        Glide.with(this@EditTabunganActivity).load(savingUrl).into(binding.imageEditTabungan)
                        binding.inputEditJudulTabungan.hint = response.body()?.data?.title
                        binding.inputEditTargetTabungan.hint = response.body()?.data?.savingTarget
                        binding.frekuensiSetoranListValue.hint = response.body()?.data?.frequency
                        binding.nominalSetoranValue.hint = response.body()?.data?.installmentDeposit

                        if(response.body()?.data?.method == "manual") {
                            val b =
                                findViewById<RadioButton>(binding.rbManual.id)
                            b.isChecked = true
                        } else if(response.body()?.data?.method == "autodebet") {
                            val b =
                                findViewById<RadioButton>(binding.rbAutodebet.id)
                            b.isChecked = true
                        }

                    } else {
                        Toast.makeText(applicationContext, "Failed to get data", Toast.LENGTH_LONG).show()
                    }
                }
            })

        //here code need to handle for the dropdown
        binding.frekuensiSetoranListValue.setItems("Harian", "Mingguan", "Bulanan")

        binding.frekuensiSetoranListValue.setOnItemSelectedListener { view, _, _, item ->
            if (view != null) {
                FREKUENSI_TABUNGAN = item as String
                Snackbar.make(view, "Clicked $item", Snackbar.LENGTH_LONG).show()
            }
        }

        binding.iconBack.setOnClickListener {
            finish()
        }
        binding.layoutClick.setOnClickListener {
            openGallery()
        }

        @Suppress("DEPRECATION")
        val progressDialog = ProgressDialog(this@EditTabunganActivity)
        progressDialog.setTitle("Processing")
        @Suppress("DEPRECATION")
        progressDialog.setMessage("Mengupload Data, Tunggu Sebentar")

        var radioValue: String

        binding.btnLanjut.setOnClickListener {
            val sharedPreferences = getSharedPreferences(LoginActivity.SP_NAME, Context.MODE_PRIVATE)
            val userId = sharedPreferences.getString(LoginActivity.TOKEN, "No Data")

            TITLE = binding.inputEditJudulTabungan.text.toString()
            TARGET_TABUNGAN = binding.inputEditTargetTabungan.text.toString().toInt()
            FREKUENSI_TABUNGAN
//            FREKUENSI_TABUNGAN = binding.frekuensiSetoranListValue.toString()
            NOMINAL_SETORAN = binding.nominalSetoranValue.text.toString().toInt()
            val id: Int = binding.layoutRadioButton.checkedRadioButtonId
            if(id!=-1){
                val radio: RadioButton = findViewById(id)
                radioValue = radio.text as String
                METODE_SETORAN = radioValue
            }
            @Suppress("DEPRECATION")
            val requestFile = RequestBody.create("multipart/form-data".toMediaTypeOrNull(), FILE)
            val profilePic = MultipartBody.Part.createFormData("file", FILE.name, requestFile)

            progressDialog.show()

            ApiClient.apiService.uploadFile(profilePic, authHeader = "Bearer $userId").enqueue(
                object : Callback<PostFileResponse> {
                    override fun onFailure(call: Call<PostFileResponse>, t: Throwable) {
                        progressDialog.dismiss()
                        Toast.makeText(applicationContext, "Error Consume API di Upload Gambar", Toast.LENGTH_LONG).show()
                    }

                    override fun onResponse(
                        call: Call<PostFileResponse>,
                        response: Response<PostFileResponse>
                    ) {
                        if(response.isSuccessful && response.body()?.status == "Success") {
                            val objectPut = PutUpdateTabunganBody(
                                FREKUENSI_TABUNGAN,
                                NOMINAL_SETORAN,
                                METODE_SETORAN,
                                response.body()!!.data.fileUrl,
                                TARGET_TABUNGAN,
                                TITLE
                            )
                            if(userId != null) {
                                ApiClient.apiService.updateTabunganPut(objectPut, ITEM_ID, authHeader = "Bearer $userId").enqueue(
                                    object : Callback<PutUpdateTabunganResponse> {
                                        override fun onFailure(
                                            call: Call<PutUpdateTabunganResponse>,
                                            t: Throwable
                                        ) {
                                            progressDialog.dismiss()
                                            Toast.makeText(applicationContext, "Error Consume API", Toast.LENGTH_LONG).show()
                                        }

                                        override fun onResponse(
                                            call: Call<PutUpdateTabunganResponse>,
                                            response: Response<PutUpdateTabunganResponse>
                                        ) {
                                            progressDialog.dismiss()
                                            startActivity(Intent(applicationContext, TabunganActivity::class.java))
                                            finish()
                                        }

                                    }
                                )
                            }
                        }
                    }
                }
            )
        }

    }

    private fun openGallery() {
        Intent(Intent.ACTION_PICK).also {
            it.type = "image/*"
            val mimeTypes = arrayOf("image/jpg", "image/png")
            it.putExtra(Intent.EXTRA_MIME_TYPES, mimeTypes)
            startActivityForResult(it, TabunganDpHunianActivity.REQUEST_PICK_IMAGE)
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == TabunganDpHunianActivity.REQUEST_PICK_IMAGE) {
                val uri = data?.data

                if (uri != null) {
                    IMAGE_URI = uri
                }

                @Suppress("DEPRECATION")
                val bitmap = MediaStore.Images.Media.getBitmap(this.contentResolver, IMAGE_URI)
                FILE = createTempFile(bitmap)

                binding.iconCameraEditTabungan.visibility = View.GONE
                binding.textDiEditTabungan.visibility = View.GONE
                binding.blackScreen.visibility = View.GONE

                //TODO HANDLE TO RESIZE IMAGE FILE SIZE IN HERE
                Glide.with(this).load(uri).into(binding.imageEditTabungan)
                binding.layoutClick.isEnabled = false

            }
        }
    }

    private fun createTempFile(bitmap: Bitmap): File {
        val file = File(
            getExternalFilesDir(Environment.DIRECTORY_PICTURES)
            , System.currentTimeMillis().toString() + ".image.jpeg"
        )
        val bos = ByteArrayOutputStream()
        bitmap.compress(Bitmap.CompressFormat.JPEG, 100, bos)
        //here code to handle if the file more than 400kb its while reduce
        var option = 90
        while (bos.toByteArray().size / 1024 > 400) {   //Loop if compressed picture is greater than 400kb, than to compression
            bos.reset()//Reset baos is empty baos
            bitmap.compress(Bitmap.CompressFormat.JPEG, option, bos)//The compression options%, storing the compressed data to the baos
            option -= 10//Every time reduced by 10
        }

        val bitmapdata: ByteArray = bos.toByteArray()
        //write the bytes in file
        try {
            val fos = FileOutputStream(file)
            fos.write(bitmapdata)
            fos.flush()
            fos.close()
        } catch (e: IOException) {
            e.printStackTrace()
        }
        return file
    }
}