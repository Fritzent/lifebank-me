package com.example.zenia.tabungan

import android.content.Intent
import android.graphics.Color
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.example.zenia.R
import com.example.zenia.pojo.GetAllTabunganResponse
import kotlinx.android.synthetic.main.tabungan_impian_item_list.view.*

class TabunganAdapter(private val listItem: List<GetAllTabunganResponse.Data>, private val token: String): RecyclerView.Adapter<TabunganAdapter.ViewHolder>() {
    class ViewHolder(itemView: View):RecyclerView.ViewHolder(itemView)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.tabungan_impian_item_list, parent, false)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int {
        return listItem.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        //TODO HERE CODE TO GIVE VALUE TO THE RECYCLERVIEW
        Log.d("CEKING", "INI LIST DATANYA ${listItem[position]}")
        val date = listItem[position].dateTarget
        val match = Regex("T00").find(date)

        val firstPart = match?.range?.start?.let { date.substring(0, it)}

        Log.d("MBA NITA", "INI YA MBA ${listItem[position].status == "Aktif"}")
        Log.d("MBA NITA", "INI YA MBA ${listItem[position].status}")

        if(listItem[position].status == "Aktif") {
            holder.itemView.tabungan_impian_list_title.text = listItem[position].title
            holder.itemView.tabungan_impian_list_status.text = listItem[position].status
            holder.itemView.tabungan_impian_list_date.text = firstPart
//            holder.itemView.tabungan_impian_list_date.text = listItem[position].dateTarget
            holder.itemView.text_jumlah_tabungan_terkumpul.text = listItem[position].totalDeposit
            holder.itemView.text_nominal_tabungan_target.text = listItem[position].savingTarget
            Glide.with(holder.itemView.context).load(listItem[position].savingPlanImageUrl).into(holder.itemView.tabungan_impian_list_image)

            holder.itemView.setOnClickListener {
                val bundle = Bundle()
                bundle.putString("Item_Id", listItem[position].id)
                bundle.putString("Token", token)
                val intent = Intent(holder.itemView.context.applicationContext, DetailTabunganImpianActivity::class.java)
                intent.putExtras(bundle)
                holder.itemView.context.startActivity(intent)
            }
        }else if (listItem[position].status == "Ditunda") {
            holder.itemView.tabungan_impian_list_title.text = listItem[position].title
            holder.itemView.tabungan_impian_list_status.text = listItem[position].status
            holder.itemView.tabungan_impian_list_date.text = firstPart
            holder.itemView.text_jumlah_tabungan_terkumpul.text = listItem[position].totalDeposit
            holder.itemView.text_nominal_tabungan_target.text = listItem[position].savingTarget
            Glide.with(holder.itemView.context).load(listItem[position].savingPlanImageUrl).into(holder.itemView.tabungan_impian_list_image)

            holder.itemView.setOnClickListener {
                val bundle = Bundle()
                bundle.putString("Item_Id", listItem[position].id)
                bundle.putString("Token", token)
                val intent = Intent(holder.itemView.context.applicationContext, DetailTabunganImpianActivity::class.java)
                intent.putExtras(bundle)
                holder.itemView.context.startActivity(intent)
            }

            //HERE CODE TO HANDLE THE VIEW IN USER
            holder.itemView.tabungan_impian_list_wrapper.setBackgroundResource(R.color.colorWhiteEF)
            holder.itemView.tabungan_impian_list_status.setBackgroundResource(R.color.colorWhiteEF)
            holder.itemView.tabungan_impian_list_status.setTextColor(Color.parseColor("#AFAFAF"))
            holder.itemView.tabungan_impian_list_title.setTextColor(Color.parseColor("#888888"))
            holder.itemView.tabungan_impian_list_date.setTextColor(Color.parseColor("#888888"))
            holder.itemView.text_jumlah_tabungan_terkumpul.setTextColor(Color.parseColor("#888888"))
            holder.itemView.text_nominal_tabungan_target.setTextColor(Color.parseColor("#888888"))
            holder.itemView.text_rp_tabungan_target.setTextColor(Color.parseColor("#888888"))
            holder.itemView.text_rp_tabungan_terkumpul.setTextColor(Color.parseColor("#888888"))
        }
    }
}