package com.example.zenia.tabungan

import android.content.Context
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.zenia.authentication.login.LoginActivity
import com.example.zenia.databinding.ActivityTabunganBinding
import com.example.zenia.home.activity.HomeActivity
import com.example.zenia.pojo.GetAllTabunganResponse
import com.example.zenia.tabungan.custome.CustomeTabunganActivity
import com.example.zenia.tabungan.hunian.TabunganDpHunianActivity

class TabunganActivity : AppCompatActivity(), TabunganActivityPresenter.Listener {

    private lateinit var binding: ActivityTabunganBinding
    private lateinit var presenter: TabunganActivityPresenter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityTabunganBinding.inflate(layoutInflater)
        val view = binding.root
        setContentView(view)

        presenter = TabunganActivityPresenter(this)

        binding.layoutEmptyTabungan.visibility = View.GONE

        binding.btnBuatCustomTabungan.setOnClickListener {
            startActivity(Intent(applicationContext, CustomeTabunganActivity::class.java))
        }
        binding.btnBuatTabunganDpHunian.setOnClickListener {
            startActivity(Intent(applicationContext, TabunganDpHunianActivity::class.java))
        }

        binding.iconBack.setOnClickListener {
            finish()
        }
    }

    override fun onResume() {
        super.onResume()
        val sharedPreferences = getSharedPreferences(LoginActivity.SP_NAME, Context.MODE_PRIVATE)
        presenter.tabunganList(sharedPreferences)
    }

    override fun onBackPressed() {
        super.onBackPressed()
        startActivity(Intent(applicationContext, HomeActivity::class.java))
    }

    override fun onTabunganListSuccess(successMessage: String) {
//        Toast.makeText(this, successMessage, Toast.LENGTH_LONG).show()
    }

    override fun onTabunganListFailure(failureMessage: String) {
        Toast.makeText(this, failureMessage, Toast.LENGTH_LONG).show()
    }

    override fun onTabunganListEmpty(emptyMessage: String) {
        binding.rcListTabungan.visibility = View.GONE
        binding.layoutEmptyTabungan.visibility = View.VISIBLE
        binding.textEmpty.text = emptyMessage
    }

    override fun showTabunganList(list: List<GetAllTabunganResponse.Data>, token: String) {
        binding.rcListTabungan.adapter = TabunganAdapter(list, token)
        binding.rcListTabungan.layoutManager =
            LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false)
    }
}