package com.example.zenia.tabungan

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.LayoutInflater
import android.widget.ImageView
import com.example.zenia.R
import com.example.zenia.databinding.ActivitySetorTabunganCheckoutBinding
import com.google.android.material.bottomsheet.BottomSheetDialog

class SetorTabunganCheckoutActivity : AppCompatActivity() {

    private lateinit var binding : ActivitySetorTabunganCheckoutBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivitySetorTabunganCheckoutBinding.inflate(layoutInflater)
        val view = binding.root
        setContentView(view)

        binding.btnShared.setOnClickListener {
            val bottomSheetDialog = BottomSheetDialog(this, R.style.BottomSheetDialogTheme)

            val bottomSheetView = LayoutInflater.from(applicationContext).inflate(
                R.layout.layout_bottom_sheet_share,
                findViewById(R.id.buttom_sheet_share)
            )
            //TODO Contoh Kalau Di Bottom Sheet nya Ada button atau action
            bottomSheetView.findViewById<ImageView>(R.id.btn_exit_share).setOnClickListener {
                bottomSheetDialog.dismiss()
            }
            bottomSheetDialog.setContentView(bottomSheetView)
            bottomSheetDialog.show()
        }
    }
}