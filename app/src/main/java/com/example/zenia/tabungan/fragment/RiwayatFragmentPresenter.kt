package com.example.zenia.tabungan.fragment

import android.content.Context
import android.util.Log
import com.example.zenia.network.ApiClient
import com.example.zenia.pojo.GetDetailTabunganResponse
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class RiwayatFragmentPresenter(val listener: Listener, val viewContext: Context) {

    fun getRiwayat(userId : String, token: String) {

        ApiClient.apiService.detailTabunganGet(userId, authHeader = "Bearer $token").enqueue(object :
            Callback<GetDetailTabunganResponse> {
            override fun onFailure(call: Call<GetDetailTabunganResponse>, t: Throwable) {
                listener.onTabunganListFailure(t.toString())
            }

            override fun onResponse(
                call: Call<GetDetailTabunganResponse>,
                response: Response<GetDetailTabunganResponse>
            ) {
                if (response.isSuccessful && response.body()?.status == "success") {

                    response.body()?.data?.let {
                        listener.showTabunganList(it.transactions, viewContext)
                        listener.onTabunganListSuccess("${response.body()?.status}")
                    }

                    if (response.body()!!.data.transactions.isNotEmpty()) {
                        Log.d("SETUP", "Ceking Response Result ${response.body()?.data}")
                        listener.showTabunganList(response.body()!!.data.transactions, viewContext)
                        listener.onTabunganListSuccess("${response.body()?.status}")
                    } else {
                        listener.onTabunganListEmpty("Tidak ada riwayat pembayaran")
                    }

                } else {
                    listener.onTabunganListFailure("${response.body()?.status}")
                }
            }
        })
    }

    interface Listener{
        fun onTabunganListSuccess(successMessage: String)
        fun onTabunganListFailure(failureMessage: String)
        fun onTabunganListEmpty(emptyMessage: String)
        fun showTabunganList(list: List<GetDetailTabunganResponse.Data.Transactions>, viewContext: Context)
    }
}