package com.example.zenia.tabungan.custome

import android.content.Intent
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.widget.RadioButton
import androidx.appcompat.app.AppCompatActivity
import com.example.zenia.R
import com.example.zenia.databinding.ActivityCustomeTabunganDetailBinding
import com.google.android.material.snackbar.Snackbar

class CustomeTabunganDetailActivity : AppCompatActivity() {

    private lateinit var binding : ActivityCustomeTabunganDetailBinding

    companion object {
        private var FREKUENSI_TABUNGAN = ""
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityCustomeTabunganDetailBinding.inflate(layoutInflater)
        val view = binding.root
        setContentView(view)

        //here code need to handle for the dropdown
        binding.frekuensiSetoranListValue.setItems("Harian", "Mingguan", "Bulanan")

        binding.frekuensiSetoranListValue.setOnItemSelectedListener { view, _, _, item ->
            if (view != null) {
                FREKUENSI_TABUNGAN = item as String
                Snackbar.make(view, "Clicked $item", Snackbar.LENGTH_LONG).show()
            }
        }

        binding.setoranAwalValue.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(p0: Editable?) {

            }

            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {

            }

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
                setupLayoutSelector()
            }
        })

        binding.frekuensiSetoranListValue.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(p0: Editable?) {

            }

            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {

            }

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
                setupLayoutSelector()
            }
        })

        binding.nominalSetoranValue.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(p0: Editable?) {

            }

            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {

            }

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
                setupLayoutSelector()
            }
        })

        var radioValue = ""

        binding.btnLanjut.setOnClickListener {
            var id: Int = binding.layoutRadioButton.checkedRadioButtonId
            if(id!=-1){
                val radio: RadioButton = findViewById(id)
                radioValue = radio.text as String
            }

            val packageIntent = intent.extras

            val bundle = Bundle()
            if (packageIntent != null) {
                bundle.putString("Judul_Tabungan", packageIntent.getString("Judul_Tabungan"))
                bundle.putString("Uri_Image", packageIntent.getString("Uri_Image"))
                bundle.putInt("Target_Tabungan", packageIntent.getInt("Target_Tabungan"))
            }

            bundle.putString("Metode_Setoran", radioValue)
            bundle.putInt("Setoran_Awal_Tabungan", binding.setoranAwalValue.text.toString().toInt())
            bundle.putString("Frekuensi_Setoran", FREKUENSI_TABUNGAN)
            bundle.putInt("Nominal_Setoran", binding.nominalSetoranValue.text.toString().toInt())
            val intent = Intent(applicationContext, CustomeTabunganCheckOutActivity::class.java)
            intent.putExtras(bundle)
            startActivity(intent)
//            startActivity(Intent(applicationContext, CustomeTabunganCheckOutActivity::class.java))
        }
    }

    fun setupLayoutSelector(){
        if(binding.setoranAwalValue.text.isEmpty()
            && binding.frekuensiSetoranListValue.text.isEmpty()
            && binding.nominalSetoranValue.text.isEmpty()) {
            binding.btnLanjut.setBackgroundResource(R.drawable.custome_button_solid_gray)
        } else {
            binding.btnLanjut.setBackgroundResource(R.drawable.custome_button_solid_blue)
        }
    }
}