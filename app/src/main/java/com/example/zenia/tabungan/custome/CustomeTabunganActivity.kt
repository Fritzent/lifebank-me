package com.example.zenia.tabungan.custome

import android.content.Intent
import android.graphics.Bitmap
import android.net.Uri
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.provider.MediaStore
import android.text.Editable
import android.text.TextWatcher
import android.view.View
import com.bumptech.glide.Glide
import com.example.zenia.R
import com.example.zenia.databinding.ActivityCustomeTabunganBinding
import com.example.zenia.tabungan.hunian.TabunganDpHunianActivity
import java.io.ByteArrayOutputStream

class CustomeTabunganActivity : AppCompatActivity() {

    private lateinit var binding: ActivityCustomeTabunganBinding

    companion object {
        private lateinit var URI : Uri
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityCustomeTabunganBinding.inflate(layoutInflater)
        val view = binding.root
        setContentView(view)

        binding.imageTabunganChoose.visibility = View.GONE

        binding.judulTabunganValue.requestFocus()
        binding.btnLanjut.isClickable = false
        binding.btnLanjut.isEnabled = false

        binding.judulTabunganValue.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(p0: Editable?) {

            }

            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
            }

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
                if(binding.judulTabunganValue.text.isNotEmpty() && binding.targetTabunganValue.text.isEmpty()) {
                    binding.btnLanjut.setBackgroundResource(R.drawable.custome_button_solid_blue)
                    binding.btnLanjut.isClickable = true
                    binding.btnLanjut.isEnabled = true
                } else if(binding.judulTabunganValue.text.isNotEmpty() && binding.targetTabunganValue.text.isNotEmpty()) {
                    binding.btnLanjut.setBackgroundResource(R.drawable.custome_button_solid_blue)
                    binding.btnLanjut.isClickable = true
                    binding.btnLanjut.isEnabled = true
                } else if(binding.judulTabunganValue.text.isEmpty() && binding.targetTabunganValue.text.isNotEmpty()) {
                    binding.btnLanjut.setBackgroundResource(R.drawable.custome_button_solid_blue)
                    binding.btnLanjut.isClickable = true
                    binding.btnLanjut.isEnabled = true
                }
                else {
                    binding.btnLanjut.setBackgroundResource(R.drawable.custome_button_solid_gray)
                    binding.btnLanjut.isClickable = false
                    binding.btnLanjut.isEnabled = false
                }
            }
        })

        binding.targetTabunganValue.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(p0: Editable?) {

            }

            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {

            }

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
                if(binding.targetTabunganValue.text.isNotEmpty() && binding.judulTabunganValue.text.isEmpty()) {
                    binding.btnLanjut.setBackgroundResource(R.drawable.custome_button_solid_blue)
                    binding.btnLanjut.isClickable = true
                    binding.btnLanjut.isEnabled = true
                } else if(binding.targetTabunganValue.text.isNotEmpty() && binding.judulTabunganValue.text.isNotEmpty()) {
                    binding.btnLanjut.setBackgroundResource(R.drawable.custome_button_solid_blue)
                    binding.btnLanjut.isClickable = true
                    binding.btnLanjut.isEnabled = true
                } else if(binding.targetTabunganValue.text.isEmpty() && binding.judulTabunganValue.text.isNotEmpty()) {
                    binding.btnLanjut.setBackgroundResource(R.drawable.custome_button_solid_blue)
                    binding.btnLanjut.isClickable = true
                    binding.btnLanjut.isEnabled = true
                }
                else {
                    binding.btnLanjut.setBackgroundResource(R.drawable.custome_button_solid_gray)
                    binding.btnLanjut.isEnabled = false
                    binding.btnLanjut.isClickable = false
                }
            }
        })

        binding.layoutCustomeTabunganImage.setOnClickListener {
            //TODO Konsep untuk bagian yang foto mirip kyk code dibawah ini
            openGallery()
        }


        binding.btnLanjut.setOnClickListener {
            val bundle = Bundle()
            bundle.putString("Judul_Tabungan", binding.judulTabunganValue.text.toString())
            bundle.putInt("Target_Tabungan", binding.targetTabunganValue.text.toString().toInt())
            bundle.putString("Uri_Image", URI.toString())
            val intent = Intent(applicationContext, CustomeTabunganDetailActivity::class.java)
            intent.putExtras(bundle)
            startActivity(intent)
        }
    }
    private fun openGallery() {
        val intent = Intent(Intent.ACTION_PICK)
        intent.type = "image/*"
        startActivityForResult(intent, TabunganDpHunianActivity.REQUEST_PICK_IMAGE)
    }
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == RESULT_OK) {
            if (requestCode == TabunganDpHunianActivity.REQUEST_PICK_IMAGE) {
                val uri = data?.data

                @Suppress("DEPRECATION")
                val imagePath = MediaStore.Images.Media.getBitmap(contentResolver,uri)
                val stream = ByteArrayOutputStream()
                val cekFormat = imagePath.compress(Bitmap.CompressFormat.PNG, 100, stream)
                val byteArray = stream.toByteArray()

                if (uri != null) {
                    URI = uri
                }
                binding.iconCamera.visibility = View.GONE
                binding.textCamera.visibility = View.GONE
                binding.imageTabunganChoose.visibility = View.VISIBLE

                //TODO HANDLE TO RESIZE IMAGE FILE SIZE IN HERE
                Glide.with(this).load(uri).into(binding.imageTabunganChoose)
//                binding.imageTabunganChoose.setImageURI(uri)

                binding.layoutCustomeTabunganImage.isEnabled = false

            }
        }
    }
}