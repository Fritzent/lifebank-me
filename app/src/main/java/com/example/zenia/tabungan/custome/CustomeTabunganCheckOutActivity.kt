package com.example.zenia.tabungan.custome

import android.app.ProgressDialog
import android.content.Context
import android.content.Intent
import android.graphics.Bitmap
import android.net.Uri
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Environment
import android.provider.MediaStore
import android.widget.Toast
import androidx.core.net.toUri
import com.bumptech.glide.Glide
import com.example.zenia.R
import com.example.zenia.authentication.login.LoginActivity
import com.example.zenia.databinding.ActivityCustomeTabunganCheckOutBinding
import com.example.zenia.network.ApiClient
import com.example.zenia.pojo.PostCreateTabunganBody
import com.example.zenia.pojo.PostCreateTabunganResponse
import com.example.zenia.pojo.PostFileResponse
import com.example.zenia.tabungan.TabunganActivity
import com.example.zenia.tabungan.hunian.TabunganDpHunianCheckOutActivity
import okhttp3.MediaType.Companion.toMediaTypeOrNull
import okhttp3.MultipartBody
import okhttp3.RequestBody
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.io.ByteArrayOutputStream
import java.io.File
import java.io.FileOutputStream
import java.io.IOException
import java.net.URI

class CustomeTabunganCheckOutActivity : AppCompatActivity() {

    private lateinit var binding : ActivityCustomeTabunganCheckOutBinding

    companion object {
        private const val DATE_TARGET = "2020-09-12"
        private const val TYPE = "custom"
        private var TITLE = ""
        private var TARGET_TABUNGAN = 0
        private lateinit var URI : Uri
        private var METODE = ""
        private var SETORAN_AWAL = 0
        private var FREKUENSI_SETORAN = ""
        private var NOMINAL_SETORAN = 0
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityCustomeTabunganCheckOutBinding.inflate(layoutInflater)
        val view = binding.root
        setContentView(view)

        @Suppress("DEPRECATION")
        val progressDialog = ProgressDialog(this@CustomeTabunganCheckOutActivity)
        progressDialog.setTitle("Processing")
        @Suppress("DEPRECATION")
        progressDialog.setMessage("Mengupload Data, Tunggu Sebentar")

        if(intent.extras != null) {
            val packageIntent = intent.extras

            if(packageIntent != null) {
                TITLE = packageIntent.getString("Judul_Tabungan").toString()
                TARGET_TABUNGAN = packageIntent.getInt("Target_Tabungan")
                URI = packageIntent.getString("Uri_Image").toString().toUri()
                METODE = packageIntent.getString("Metode_Setoran").toString()
                SETORAN_AWAL = packageIntent.getInt("Setoran_Awal_Tabungan")
                FREKUENSI_SETORAN = packageIntent.getString("Frekuensi_Setoran").toString()
                NOMINAL_SETORAN = packageIntent.getInt("Nominal_Setoran")
            }
        }

        Glide.with(this).load(URI).into(binding.customeTabunganImageCreate)
        binding.customeTabunganTitleCreate.text = TITLE
        binding.customeTabunganPriceTargetCreate.text = TARGET_TABUNGAN.toString()
        binding.suggestTargetTabunganValue.text = DATE_TARGET
        binding.metodeSetoranValueCheckout.text = METODE
        binding.frekuensiSetoranValueCheckout.text = FREKUENSI_SETORAN
        binding.setoranAwalValueCheckout.text = SETORAN_AWAL.toString()
        binding.nominalSetoranValueCheckout.text = NOMINAL_SETORAN.toString()

        binding.btnCreateTabungan.setOnClickListener {
            val sharedPreferences = getSharedPreferences(LoginActivity.SP_NAME, Context.MODE_PRIVATE)
            val userId = sharedPreferences.getString(LoginActivity.TOKEN, "No Data")

            @Suppress("DEPRECATION")
            val bitmap = MediaStore.Images.Media.getBitmap(this.contentResolver, URI)
            val fileImage : File = createTempFile(bitmap)

            @Suppress("DEPRECATION")
            val requestFile = RequestBody.create("multipart/form-data".toMediaTypeOrNull(), fileImage)
            val profilePic = MultipartBody.Part.createFormData("file", fileImage.name, requestFile)

            progressDialog.show()

            ApiClient.apiService.uploadFile(profilePic, authHeader = "Bearer $userId").enqueue(
                object : Callback<PostFileResponse> {
                    override fun onFailure(call: Call<PostFileResponse>, t: Throwable) {
                        progressDialog.dismiss()
                        Toast.makeText(applicationContext, "Error Consume API di Upload Gambar", Toast.LENGTH_LONG).show()
                    }

                    override fun onResponse(
                        call: Call<PostFileResponse>,
                        response: Response<PostFileResponse>
                    ) {
                        if(response.isSuccessful && response.body()?.status == "Success") {
                            val objectPut = PostCreateTabunganBody(
                                DATE_TARGET,
                                FREKUENSI_SETORAN,
                                SETORAN_AWAL,
                                NOMINAL_SETORAN,
                                METODE,
                                response.body()!!.data.fileUrl,
                                TARGET_TABUNGAN,
                                TITLE,
                                TYPE
                            )
                            if(userId != null) {
                                ApiClient.apiService.createTabunganPost(objectPut, authHeader = "Bearer $userId").enqueue(
                                    object : Callback<PostCreateTabunganResponse> {
                                        override fun onFailure(
                                            call: Call<PostCreateTabunganResponse>,
                                            t: Throwable
                                        ) {
                                            Toast.makeText(applicationContext, "Error Consume API", Toast.LENGTH_LONG).show()
                                        }

                                        override fun onResponse(
                                            call: Call<PostCreateTabunganResponse>,
                                            response: Response<PostCreateTabunganResponse>
                                        ) {
                                            progressDialog.dismiss()
                                            startActivity(Intent(applicationContext, TabunganActivity::class.java))
                                            finish()
                                        }

                                    })
                            }
                        }
                    }
                })
        }
    }
    private fun createTempFile(bitmap: Bitmap): File {
        val file = File(
            getExternalFilesDir(Environment.DIRECTORY_PICTURES)
            , System.currentTimeMillis().toString() + ".image.jpeg"
        )
        val bos = ByteArrayOutputStream()
        bitmap.compress(Bitmap.CompressFormat.JPEG, 100, bos)
        val bitmapdata: ByteArray = bos.toByteArray()
        //write the bytes in file

        var option = 90;
        while (bos.toByteArray().size / 1024 > 400) {   //Loop if compressed picture is greater than 400kb, than to compression
            bos.reset();//Reset baos is empty baos
            bitmap.compress(Bitmap.CompressFormat.JPEG, option, bos);//The compression options%, storing the compressed data to the baos
            option -= 10;//Every time reduced by 10
        }

        try {
            val fos = FileOutputStream(file)
            fos.write(bitmapdata)
            fos.flush()
            fos.close()
        } catch (e: IOException) {
            e.printStackTrace()
        }
        return file
    }
}