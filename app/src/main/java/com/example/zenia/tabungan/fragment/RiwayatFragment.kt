package com.example.zenia.tabungan.fragment

import android.content.Context
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.zenia.R
import com.example.zenia.pojo.GetDetailTabunganResponse
import kotlinx.android.synthetic.main.fragment_riwayat.*

class RiwayatFragment : Fragment(), RiwayatFragmentPresenter.Listener {

    private lateinit var presenter: RiwayatFragmentPresenter

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_riwayat, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        presenter = context?.let { RiwayatFragmentPresenter(this, it) }!!
        val itemId = arguments?.getString(DATA_ITEM)
        val token = arguments?.getString(DATA_TOKEN)

        layout_empty_riwayat.visibility = View.GONE

        if(itemId != null) {
            if (token != null) {
                presenter.getRiwayat(itemId, token)
            }
        }
    }

    companion object {
        private var DATA_TOKEN = "TOKENKU"
        private var DATA_ITEM = "ITEMKU"
        @JvmStatic
        fun newInstance(itemId: String, token: String) =
            RiwayatFragment().apply {
                arguments = Bundle().apply {
                    putString(DATA_ITEM, itemId)
                    putString(DATA_TOKEN, token)
                }
            }
    }

    override fun onTabunganListSuccess(successMessage: String) {
//        Toast.makeText(activity, successMessage, Toast.LENGTH_LONG).show()
    }

    override fun onTabunganListFailure(failureMessage: String) {
//        Toast.makeText(activity, failureMessage, Toast.LENGTH_LONG).show()
    }

    override fun onTabunganListEmpty(emptyMessage: String) {
        layout_empty_riwayat.visibility = View.VISIBLE
        text_empty_riwayat.text = emptyMessage
    }

    override fun showTabunganList(list: List<GetDetailTabunganResponse.Data.Transactions>, viewContext: Context) {
        rc_riwayat_tabungan?.adapter = RiwayatFragmentAdapter(list,viewContext)
        rc_riwayat_tabungan?.setHasFixedSize(true)
        rc_riwayat_tabungan?.layoutManager = LinearLayoutManager(activity, LinearLayoutManager.VERTICAL, false)

//        Log.d("CEK", "INI DATA LIST NYA : ${list}")

    }
}