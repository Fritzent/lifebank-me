package com.example.zenia.tabungan.fragment

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.Toast
import androidx.fragment.app.Fragment
import com.bumptech.glide.Glide
import com.example.zenia.R
import com.example.zenia.network.ApiClient
import com.example.zenia.pojo.GetDetailTabunganResponse
import com.google.android.material.bottomsheet.BottomSheetDialog
import kotlinx.android.synthetic.main.fragment_detail.*
import kotlinx.android.synthetic.main.fragment_detail.view.*
import kotlinx.android.synthetic.main.tabungan_impian_item_list.view.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class DetailFragment : Fragment() {

//    fun DetailFragment() {
//
//    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val view = inflater.inflate(R.layout.fragment_detail, container, false)

//        val itemId = arguments?.getString(DATA_ITEM)
//        val token = arguments?.getString(DATA_TOKEN)
//
//        if (itemId != null) {
//            ApiClient.apiService.detailTabunganGet(itemId, authHeader = "Bearer $token").enqueue(object :
//                Callback<GetDetailTabunganResponse> {
//                override fun onFailure(call: Call<GetDetailTabunganResponse>, t: Throwable) {
//                    Toast.makeText(context, "Error DetailFragment : ${t.message}", Toast.LENGTH_LONG).show()
//                }
//
//                override fun onResponse(
//                    call: Call<GetDetailTabunganResponse>,
//                    response: Response<GetDetailTabunganResponse>
//                ) {
//                    if(response.isSuccessful && response.body()?.status == "success"){
//                        //TODO HANDEL THIS CODE TO RUN IN THE BACKGROUN THREAD
//                        val valueData = response.body()?.data
//                        view.top_section_detail.tabungan_impian_list_title.text = valueData?.title
//                        view.top_section_detail.tabungan_impian_list_status.text = valueData?.status
//                        view.top_section_detail.tabungan_impian_list_date.text = valueData?.dateTarget
//                        if(valueData?.savingPlanImageUrl == null) {
//                            view.top_section_detail.tabungan_impian_list_image.setImageResource(R.drawable.dummyhouse)
//                        } else {
//                            Glide.with(view.context).load(valueData.savingPlanImageUrl).into(view.top_section_detail.tabungan_impian_list_image)
//                        }
//                        view.top_section_detail.text_jumlah_tabungan_terkumpul.text = valueData?.totalDeposit
//                        view.top_section_detail.text_nominal_tabungan_target.text = valueData?.savingTarget
//                        view.metode_setoran_value_checkout.text = valueData?.method
//                        view.frekuensi_setoran_value_checkout.text = valueData?.frequency
//                        view.setoran_awal_value_checkout.text = valueData?.initialDeposit
//                        view.nominal_setoran_value_checkout.text = valueData?.installmentDeposit
//                    } else {
//                        Toast.makeText(context, "Error Consume Api Di DetailFragment", Toast.LENGTH_LONG).show()
//                    }
//                }
//            })
//        }

        // here code for other costume
        view.btn_setor_tabungan.setOnClickListener {
            //TODO here code to call the bottom sheet activity
            val bottomSheetDialog = BottomSheetDialog(view.context, R.style.BottomSheetDialogTheme)

            val bottomSheetView = LayoutInflater.from(view.context.applicationContext).inflate(
                R.layout.layout_bottom_sheet_button_setor,
                view.findViewById(R.id.button_sheet_setor)
            )
            //TODO Contoh Kalau Di Bottom Sheet nya Ada button atau action
            bottomSheetView.findViewById<ImageView>(R.id.btn_exit_setor).setOnClickListener {
                bottomSheetDialog.dismiss()
            }
            bottomSheetDialog.setContentView(bottomSheetView)
            bottomSheetDialog.show()
        }
        return view
    }

    override fun onResume() {
        super.onResume()
        val itemId = arguments?.getString(DATA_ITEM)
        val token = arguments?.getString(DATA_TOKEN)

        if (itemId != null) {
            ApiClient.apiService.detailTabunganGet(itemId, authHeader = "Bearer $token").enqueue(object :
                Callback<GetDetailTabunganResponse> {
                override fun onFailure(call: Call<GetDetailTabunganResponse>, t: Throwable) {
                    Toast.makeText(context, "Error DetailFragment : ${t.message}", Toast.LENGTH_LONG).show()
                }

                override fun onResponse(
                    call: Call<GetDetailTabunganResponse>,
                    response: Response<GetDetailTabunganResponse>
                ) {
                    if(response.isSuccessful && response.body()?.status == "success"){
                        //TODO HANDEL THIS CODE TO RUN IN THE BACKGROUN THREAD
                        val valueData = response.body()?.data
                        top_section_detail.tabungan_impian_list_title.text = valueData?.title
                        top_section_detail.tabungan_impian_list_status.text = valueData?.status
                        top_section_detail.tabungan_impian_list_date.text = valueData?.dateTarget
                        if(valueData?.savingPlanImageUrl == null) {
                            top_section_detail.tabungan_impian_list_image.setImageResource(R.drawable.dummyhouse)
                        } else {
                            context?.let { Glide.with(it).load(valueData.savingPlanImageUrl).into(top_section_detail.tabungan_impian_list_image) }
                        }
                        top_section_detail.text_jumlah_tabungan_terkumpul.text = valueData?.totalDeposit
                        top_section_detail.text_nominal_tabungan_target.text = valueData?.savingTarget
                        metode_setoran_value_checkout.text = valueData?.method
                        frekuensi_setoran_value_checkout.text = valueData?.frequency
                        setoran_awal_value_checkout.text = valueData?.initialDeposit
                        nominal_setoran_value_checkout.text = valueData?.installmentDeposit
                    } else {
                        Toast.makeText(context, "Error Consume Api Di DetailFragment", Toast.LENGTH_LONG).show()
                    }
                }
            })
        }
    }

    companion object {
        private var DATA_TOKEN = "TOKENKU"
        private var DATA_ITEM = "ITEMKU"

        @JvmStatic
        fun newInstance(itemId: String, token: String) =
            DetailFragment().apply {
                arguments = Bundle().apply {
                    putString(DATA_ITEM, itemId)
                    putString(DATA_TOKEN, token)
                }
            }
    }
}