package com.example.zenia.tabungan

import android.content.Context
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter
import com.example.zenia.tabungan.fragment.DetailFragment
import com.example.zenia.tabungan.fragment.RiwayatFragment

@Suppress("DEPRECATION")
internal class AdapterDetailTabunganImpianActivity(var context: Context, fm: FragmentManager, private var totalTabs: Int, private var itemId: String, private var token: String)
    : FragmentPagerAdapter(fm) {
    override fun getItem(position: Int): Fragment {
        return when (position) {
            0 -> {
                DetailFragment.newInstance(itemId, token)
            }
            1 -> {
                RiwayatFragment.newInstance(itemId, token)
            }
            else -> getItem(position)
        }
    }

    override fun getCount(): Int {
        return totalTabs
    }


}