package com.example.zenia.tabungan

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.widget.Button
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.example.zenia.R
import com.example.zenia.authentication.login.LoginActivity
import com.example.zenia.databinding.ActivityDetailTabunganImpianBinding
import com.example.zenia.network.ApiClient
import com.example.zenia.pojo.GetDetailTabunganResponse
import com.example.zenia.pojo.PutUpdateTabunganStatusBody
import com.example.zenia.pojo.PutUpdateTabunganStatusResponse
import com.google.android.material.bottomsheet.BottomSheetDialog
import com.google.android.material.tabs.TabLayout
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class DetailTabunganImpianActivity : AppCompatActivity() {

    private lateinit var binding : ActivityDetailTabunganImpianBinding

    companion object {
        private var ITEM_ID = ""
        private var TOKEN = ""
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityDetailTabunganImpianBinding.inflate(layoutInflater)
        val view = binding.root
        setContentView(view)

//        val sharedPreferences = getSharedPreferences(LoginActivity.SP_NAME, Context.MODE_PRIVATE)
//        val userId = sharedPreferences.getString(TOKEN, "No Data")

        binding.iconBack.setOnClickListener {
            finish()
        }

        val packageIntent = intent.extras

        if(packageIntent != null) {
            ITEM_ID = packageIntent.getString("Item_Id").toString()
            TOKEN = packageIntent.getString("Token").toString()
        }

        //here the handling the tab layout
        tablayoutHandling(ITEM_ID, TOKEN)

        binding.btnMenuLainnya.setOnClickListener {
            //TODO FIXING THIS
            //TODO here code to call the bottom sheet activity
            val bottomSheetDialog = BottomSheetDialog(this, R.style.BottomSheetDialogTheme)

            val bottomSheetView = LayoutInflater.from(applicationContext).inflate(
                R.layout.layout_bottom_sheet,
                findViewById(R.id.bottom_sheet)
            )
            //TODO Contoh Kalau Di Bottom Sheet nya Ada button atau action
            bottomSheetView.findViewById<ImageView>(R.id.btn_exit).setOnClickListener {
                bottomSheetDialog.dismiss()
            }
            bottomSheetView.findViewById<LinearLayout>(R.id.menu_hentikan_tabungan).setOnClickListener {
                val bottomSheetDialogHentikanTabungan = BottomSheetDialog(this, R.style.BottomSheetDialogTheme)
                val bottomSheetViewHentikanTabungan = LayoutInflater.from(applicationContext).inflate(
                    R.layout.layout_bottom_sheet_hentikan_tabungan,
                    findViewById(R.id.bottom_sheet_hentikan_tabungan)
                )

                bottomSheetViewHentikanTabungan.findViewById<ImageView>(R.id.btn_exit_hentikan_tabungan).setOnClickListener {
                    bottomSheetDialogHentikanTabungan.dismiss()
                }

                bottomSheetDialogHentikanTabungan.setContentView(bottomSheetViewHentikanTabungan)
                bottomSheetDialogHentikanTabungan.show()
            }
            bottomSheetView.findViewById<LinearLayout>(R.id.menu_tunda_tabungan).setOnClickListener {
                val bottomSheetDialogTundaTabungan = BottomSheetDialog(this, R.style.BottomSheetDialogTheme)
                val bottomSheetViewTundaTabungan = LayoutInflater.from(applicationContext).inflate(
                    R.layout.layout_bottom_sheet_tunda_tabungan,
                    findViewById(R.id.bottom_sheet_tunda_tabungan)
                )

                bottomSheetViewTundaTabungan.findViewById<ImageView>(R.id.btn_exit_tunda_tabungan).setOnClickListener {
                    bottomSheetDialogTundaTabungan.dismiss()
                }
                bottomSheetViewTundaTabungan.findViewById<Button>(R.id.btn_hentikan).setOnClickListener {
                    //todo here code to tunda tabungan
                    val objectPut = PutUpdateTabunganStatusBody(
                        "Ditunda"
                    )
                    Log.d("CEKING", "INI DATA OBJECT PUTNYA $objectPut")
                    Log.d("CEKING", "INI DATA ITEM ID NYA $ITEM_ID")
                    Log.d("CEKING", "INI DATA TOKENNYA $TOKEN")

                    ApiClient.apiService.updateStatusTabungan(objectPut, ITEM_ID , authHeader = "Bearer $TOKEN").enqueue(
                        object : Callback<PutUpdateTabunganStatusResponse> {
                            override fun onFailure(
                                call: Call<PutUpdateTabunganStatusResponse>,
                                t: Throwable
                            ) {
                                Toast.makeText(applicationContext, "Error Consume API", Toast.LENGTH_LONG).show()
                            }

                            override fun onResponse(
                                call: Call<PutUpdateTabunganStatusResponse>,
                                response: Response<PutUpdateTabunganStatusResponse>
                            ) {
                                if(response.isSuccessful && response.body()?.status == "success" ) {
                                    bottomSheetDialogTundaTabungan.dismiss()
                                    Toast.makeText(applicationContext, "Tabungan Berhasil Di tunda", Toast.LENGTH_LONG).show()
                                } else {
                                    Toast.makeText(applicationContext, "Tabungan Gagal Di tunda", Toast.LENGTH_LONG).show()
                                }
                            }

                        })
                }

                bottomSheetDialogTundaTabungan.setContentView(bottomSheetViewTundaTabungan)
                bottomSheetDialogTundaTabungan.show()
            }
            bottomSheetView.findViewById<LinearLayout>(R.id.menu_edit_tabungan).setOnClickListener {
                // Add Bundle here to send all the data to EditTabunganActivity
                val bundle = Bundle()
                bundle.putString("Item_id", ITEM_ID)
                bundle.putString("Token", TOKEN)
                val intent = Intent(applicationContext, EditTabunganActivity::class.java)
                intent.putExtras(bundle)
                startActivity(intent)
            }
            bottomSheetDialog.setContentView(bottomSheetView)
            bottomSheetDialog.show()
        }

    }

    private fun tablayoutHandling(itemId: String, token: String) {
        binding.tabLayoutDetailTabungan.addTab(binding.tabLayoutDetailTabungan.newTab().setText(R.string.text_detail))
        binding.tabLayoutDetailTabungan.addTab(binding.tabLayoutDetailTabungan.newTab().setText(R.string.text_riwayat))
        binding.tabLayoutDetailTabungan.tabGravity = TabLayout.GRAVITY_FILL

        val adapter = AdapterDetailTabunganImpianActivity(
            this,
            supportFragmentManager,
            binding.tabLayoutDetailTabungan.tabCount,
            itemId,
            token)
        binding.viewPagerDetailTabungan.adapter = adapter
        binding.viewPagerDetailTabungan.addOnPageChangeListener(
            TabLayout.TabLayoutOnPageChangeListener(
                binding.tabLayoutDetailTabungan
            )
        )
        binding.tabLayoutDetailTabungan.addOnTabSelectedListener(
            object : TabLayout.OnTabSelectedListener {
                override fun onTabSelected(tab: TabLayout.Tab) {

                    binding.viewPagerDetailTabungan.currentItem = tab.position
                }
                override fun onTabUnselected(tab: TabLayout.Tab) {}
                override fun onTabReselected(tab: TabLayout.Tab) {}
            }
        )
    }
}