package com.example.zenia.tabungan

import android.content.SharedPreferences
import com.example.zenia.authentication.login.LoginActivity.Companion.ID
import com.example.zenia.authentication.login.LoginActivity.Companion.TOKEN
import com.example.zenia.network.ApiClient
import com.example.zenia.pojo.GetAllTabunganResponse
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class TabunganActivityPresenter(val listener: Listener) {

    fun tabunganList(sharedPreferences: SharedPreferences){

        val userId = sharedPreferences.getString(TOKEN, "No Data")

        if (userId != null) {
            ApiClient.apiService.collectTabunganGet( authHeader = "Bearer $userId").enqueue(object :
                Callback<GetAllTabunganResponse> {
                override fun onFailure(call: Call<GetAllTabunganResponse>, t: Throwable) {
                    listener.onTabunganListFailure(t.toString())
                }

                override fun onResponse(
                    call: Call<GetAllTabunganResponse>,
                    response: Response<GetAllTabunganResponse>
                ) {
                    if(response.isSuccessful && response.body()?.status == "Success") {

//                        response.body()?.data?.let {
//                            listener.showTabunganList(it,userId)
//                            listener.onTabunganListSuccess("${response.body()?.status}")
//                        }

                        when {
                            response.body()!!.data.isNotEmpty() -> {
                                listener.showTabunganList(response.body()!!.data,userId)
                                listener.onTabunganListSuccess("${response.body()?.status}")
                            }
                            response.body()!!.data.isEmpty() -> {
                                listener.onTabunganListEmpty("Tidak ada tabungan impian")
                            }
                            else -> {
                                listener.onTabunganListEmpty("Tidak ada tabungan impian")
                            }
                        }

                    }else {
                        listener.onTabunganListFailure("${response.body()?.status}")
                    }
                }
            })
        }
    }

    interface Listener{
        fun onTabunganListSuccess(successMessage: String)
        fun onTabunganListFailure(failureMessage: String)
        fun onTabunganListEmpty(emptyMessage: String)
        fun showTabunganList(list: List<GetAllTabunganResponse.Data>, token: String)
    }
}