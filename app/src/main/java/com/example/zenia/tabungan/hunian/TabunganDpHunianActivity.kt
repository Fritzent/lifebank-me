package com.example.zenia.tabungan.hunian

import android.app.Activity
import android.content.Intent
import android.graphics.Color
import android.net.Uri
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import com.bumptech.glide.Glide
import com.example.zenia.R
import com.example.zenia.databinding.ActivityTabunganDpHunianBinding
import java.lang.Long.parseLong
import java.text.DecimalFormat
import java.text.NumberFormat
import java.util.*


class TabunganDpHunianActivity : AppCompatActivity() {

    private lateinit var binding: ActivityTabunganDpHunianBinding

    companion object {
        const val REQUEST_PICK_IMAGE = 202
        private const val TARGET_TABUNGAN = 50000000
        private const val NOMINAL_SETORAN = 30000
        private const val METHOD_SETOR = "Autodebet"
        private const val FREKUENSI_SETOR = "Harian"
        private const val JUDUL_TABUNGAN = "Tabungan DP Hunian"
        private var SET_SETORAN_AWAL : Long = 0
        private lateinit var URI : Uri
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityTabunganDpHunianBinding.inflate(layoutInflater)
        val view = binding.root
        setContentView(view)

        //TODO setup for the layout here like visibility or other
        binding.imageTabunganChoose.visibility = View.GONE
        binding.btnLanjut.isClickable = false
        binding.btnLanjut.isEnabled = false

        binding.valueJudulTabungan.text = JUDUL_TABUNGAN
        binding.valueMetodeSetoran.text = METHOD_SETOR
        binding.valueFrekuensiSetoran.text = FREKUENSI_SETOR
        binding.valueTargetTabungan.text = "Rp ${TARGET_TABUNGAN}"
        binding.valueNominalSetoran.text = "Rp ${NOMINAL_SETORAN}"

        binding.iconBack.setOnClickListener {
            finish()
        }

        binding.layoutTabunganDpHunianImage.setOnClickListener {
            openGallery()
        }

        binding.valueSetoranAwal.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(p0: Editable?) {
                binding.valueSetoranAwal.removeTextChangedListener(this)

                try {
                    var originalString = p0.toString()
                    val longval : Long

                    if(originalString.contains(".")) {
                        originalString = originalString.replace("[.]".toRegex(), "")
                    }
                    if(originalString.contains("Rp")) {
                        originalString = originalString.replace("Rp ".toRegex(), "")
                    }

                    SET_SETORAN_AWAL = originalString.toLong()

                    if(SET_SETORAN_AWAL >= 1000000000) {
                        binding.valueSetoranAwal.error = "Melebihi Batas Setoran"
                        binding.valueSetoranAwal.setTextColor(Color.parseColor("#EE0D0D"))
                        binding.btnLanjut.setBackgroundResource(R.drawable.custome_button_solid_gray)
                        binding.btnLanjut.isEnabled = false
                        binding.btnLanjut.isClickable = false
                    } else {
                        binding.valueSetoranAwal.setTextColor(Color.parseColor("#333333"))
                        binding.btnLanjut.setBackgroundResource(R.drawable.custome_button_solid_blue)
                        binding.btnLanjut.isEnabled = true
                        binding.btnLanjut.isClickable = true
                    }

                    longval = parseLong(originalString)

                    val formatter: DecimalFormat = NumberFormat.getCurrencyInstance(Locale("in", "ID")) as DecimalFormat
                    var formattedString: String = formatter.format(longval)

                    if(formattedString.contains("Rp")) {
                        formattedString = formattedString.replace("Rp".toRegex(), "Rp ")
                    }
                    if(formattedString.contains(",00")) {
                        formattedString = formattedString.replace(",00".toRegex(), "")
                    }

                    binding.valueSetoranAwal.setText(formattedString)
                    binding.valueSetoranAwal.setSelection(binding.valueSetoranAwal.text.length)

                } catch ( nfe : NumberFormatException) {
                    nfe.printStackTrace()
                }
                binding.valueSetoranAwal.addTextChangedListener(this)
            }

            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {

            }

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
                if(binding.valueSetoranAwal.text.isNotEmpty()) {
                    binding.btnLanjut.setBackgroundResource(R.drawable.custome_button_solid_blue)
                }
                else {
                    binding.btnLanjut.setBackgroundResource(R.drawable.custome_button_solid_gray)
                }
            }
        })

        binding.btnLanjut.setOnClickListener {
            val bundle = Bundle()
            bundle.putString("Judul_Tabungan", JUDUL_TABUNGAN)
            bundle.putInt("Target_Tabungan", TARGET_TABUNGAN)
            bundle.putLong("Setoran_Awal", SET_SETORAN_AWAL)
            bundle.putString("Metode_Setoran", METHOD_SETOR)
            bundle.putString("Frekuensi_Setoran", FREKUENSI_SETOR)
            bundle.putInt("Nominal_Setoran", NOMINAL_SETORAN)
            bundle.putString("Uri_Image", URI.toString())
            val intentData = Intent(applicationContext, TabunganDpHunianCheckOutActivity::class.java)
            intentData.putExtras(bundle)
            startActivity(intentData)
        }

    }
    private fun openGallery() {
        Intent(Intent.ACTION_PICK).also {
            it.type = "image/*"
            val mimeTypes = arrayOf("image/jpg", "image/png")
            it.putExtra(Intent.EXTRA_MIME_TYPES, mimeTypes)
            startActivityForResult(it, REQUEST_PICK_IMAGE)
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == REQUEST_PICK_IMAGE) {
                val uri = data?.data

                if (uri != null) {
                    URI = uri
                }

                binding.iconCamera.visibility = View.GONE
                binding.textCamera.visibility = View.GONE
                binding.imageTabunganChoose.visibility = View.VISIBLE

                //TODO HANDLE TO RESIZE IMAGE FILE SIZE IN HERE
                Glide.with(this).load(uri).into(binding.imageTabunganChoose)
                binding.layoutTabunganDpHunianImage.isEnabled = false

            }
        }
    }
}