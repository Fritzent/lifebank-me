package com.example.zenia.tabungan.fragment

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.zenia.R
import com.example.zenia.pojo.GetDetailTabunganResponse
import kotlinx.android.synthetic.main.riwayat_item_list.view.*

class RiwayatFragmentAdapter(private val listItem: List<GetDetailTabunganResponse.Data.Transactions>, val context: Context): RecyclerView.Adapter<RiwayatFragmentAdapter.ViewHolder> (){
    class ViewHolder(itemView: View):RecyclerView.ViewHolder(itemView)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(context).inflate(R.layout.riwayat_item_list, parent, false)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int {
        return listItem.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.itemView.jenis_setoran_tabungan.text = listItem[position].transactionName
        holder.itemView.total_payment.text = listItem[position].totalPayment.toString()
        holder.itemView.transaction_code.text = listItem[position].transactionCode
    }

}