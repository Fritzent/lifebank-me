package com.example.zenia.bantuan.view.tiket

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.fragment.app.FragmentTransaction
import com.example.zenia.R
import com.example.zenia.bantuan.view.tiket.fragment.BantuanBayarTiketFragment
import com.example.zenia.bantuan.view.tiket.fragment.BantuanTiketGagalFragment
import com.example.zenia.bantuan.view.tiket.fragment.MenuBantuanTiketFragment
import com.example.zenia.bantuan.view.transfer.fragment.BantuanCaraTransferFragment
import com.example.zenia.bantuan.view.transfer.fragment.BantuanTransferGagalFragment
import com.example.zenia.bantuan.view.transfer.fragment.MenuBantuanTransferFragment
import com.example.zenia.databinding.ActivityBantuanTiketBinding
import kotlinx.android.synthetic.main.toolbar.view.*

class BantuanTiketActivity : AppCompatActivity() {

    private lateinit var binding: ActivityBantuanTiketBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = ActivityBantuanTiketBinding.inflate(layoutInflater)

        setContentView(binding.root)

        val appBar = binding.menuToolbar.appBarLayout.toolbar
        setSupportActionBar(appBar)
        supportActionBar?.title = "Tiket"
        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        //default fragment
        supportFragmentManager
            .beginTransaction()
            .replace(R.id.fragmentBantuanContainer, MenuBantuanTiketFragment())
            .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
            .commit()

    }
    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }

    fun toGoCaraBayarTiket(){
        supportFragmentManager
            .beginTransaction()
            .replace(R.id.fragmentBantuanContainer, BantuanBayarTiketFragment())
            .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
            .commit()

        supportActionBar?.title = "Cara Bayar Tiket"
    }

    fun toGoTiketGagal(){
        supportFragmentManager
            .beginTransaction()
            .replace(R.id.fragmentBantuanContainer, BantuanTiketGagalFragment())
            .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
            .commit()

        supportActionBar?.title = "Bayar Tiket Gagal"
    }
}