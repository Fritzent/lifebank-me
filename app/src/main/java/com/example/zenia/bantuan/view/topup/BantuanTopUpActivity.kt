package com.example.zenia.bantuan.view.topup

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.fragment.app.FragmentTransaction
import com.example.zenia.R
import com.example.zenia.bantuan.view.tiket.fragment.BantuanBayarTiketFragment
import com.example.zenia.bantuan.view.tiket.fragment.BantuanTiketGagalFragment
import com.example.zenia.bantuan.view.topup.fragment.BantuanCaraTopUpFragment
import com.example.zenia.bantuan.view.topup.fragment.BantuanTopUpGagalFragment
import com.example.zenia.bantuan.view.topup.fragment.MenuBantuanTopUpFragment
import com.example.zenia.bantuan.view.transfer.fragment.MenuBantuanTransferFragment
import com.example.zenia.databinding.ActivityBantuanTopUpBinding
import kotlinx.android.synthetic.main.toolbar.view.*

class BantuanTopUpActivity : AppCompatActivity() {
    private lateinit var binding: ActivityBantuanTopUpBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = ActivityBantuanTopUpBinding.inflate(layoutInflater)
        setContentView(binding.root)

        val appBar = binding.menuToolbar.appBarLayout.toolbar
        setSupportActionBar(appBar)
        supportActionBar?.title = "Top Up"
        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        //default fragment
        supportFragmentManager
            .beginTransaction()
            .replace(R.id.fragmentBantuanContainer, MenuBantuanTopUpFragment())
            .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
            .commit()

    }
    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }

    fun toFragmentCaraTopUp(){
        supportFragmentManager
            .beginTransaction()
            .replace(R.id.fragmentBantuanContainer, BantuanCaraTopUpFragment())
            .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
            .commit()

        supportActionBar?.title = "Cara Top-Up"
    }

    fun toFragmentTopUpGagal(){
        supportFragmentManager
            .beginTransaction()
            .replace(R.id.fragmentBantuanContainer, BantuanTopUpGagalFragment())
            .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
            .commit()

        supportActionBar?.title = "Top-Up Gagal"
    }
}