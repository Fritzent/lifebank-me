package com.example.zenia.bantuan.view.paketData

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.fragment.app.FragmentTransaction
import com.example.zenia.R
import com.example.zenia.bantuan.view.paketData.fragment.BantuanCaraPaketData
import com.example.zenia.bantuan.view.paketData.fragment.BantuanPaketDataGagalFragment
import com.example.zenia.bantuan.view.paketData.fragment.MenuBantuanPaketDataFragment
import com.example.zenia.bantuan.view.transfer.fragment.MenuBantuanTransferFragment
import com.example.zenia.databinding.ActivityBantuanPaketDataBinding
import kotlinx.android.synthetic.main.toolbar.view.*

class BantuanPaketDataActivity : AppCompatActivity() {
    private lateinit var binding: ActivityBantuanPaketDataBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityBantuanPaketDataBinding.inflate(layoutInflater)

        setContentView(binding.root)

        val appBar = binding.menuToolbar.appBarLayout.toolbar
        setSupportActionBar(appBar)
        supportActionBar?.title = "Paket Data"
        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        //default fragment
        supportFragmentManager
            .beginTransaction()
            .replace(R.id.fragmentBantuanContainer, MenuBantuanPaketDataFragment())
            .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
            .commit()
    }


    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }

    fun goToCaraPaketData(){
        supportFragmentManager
            .beginTransaction()
            .replace(R.id.fragmentBantuanContainer, BantuanCaraPaketData())
            .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
            .commit()

        supportActionBar?.title = "Cara Beli Paket Data"
    }

    fun goToPaketDataGagal(){
        supportFragmentManager
            .beginTransaction()
            .replace(R.id.fragmentBantuanContainer, BantuanPaketDataGagalFragment())
            .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
            .commit()

        supportActionBar?.title = "Pembayaran Paket Data Gagal"
    }
}