package com.example.zenia.bantuan.view.menuBantuan

class MenuBantuanPresenter(val listener:Listener) {
    interface Listener{
        fun goToBantuan(position:Int)
    }

    fun goToBantuan(position: Int){
        listener.goToBantuan(position)
    }
}