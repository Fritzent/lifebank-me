package com.example.zenia.bantuan.view.kpr

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.fragment.app.FragmentTransaction
import com.example.zenia.R
import com.example.zenia.bantuan.view.kpr.fragment.BantuanPembayaranKPRFragment
import com.example.zenia.bantuan.view.kpr.fragment.BantuanPengajuanKPRFragment
import com.example.zenia.bantuan.view.kpr.fragment.MenuBantuanKPRFragment
import com.example.zenia.bantuan.view.paketData.fragment.BantuanCaraPaketData
import com.example.zenia.bantuan.view.paketData.fragment.BantuanPaketDataGagalFragment
import com.example.zenia.bantuan.view.paketData.fragment.MenuBantuanPaketDataFragment
import com.example.zenia.databinding.ActivityBantuanKPRBinding
import kotlinx.android.synthetic.main.toolbar.view.*

class BantuanKPRActivity : AppCompatActivity() {
    private lateinit var binding: ActivityBantuanKPRBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = ActivityBantuanKPRBinding.inflate(layoutInflater)
        setContentView(binding.root)

        val appBar = binding.menuToolbar.appBarLayout.toolbar
        setSupportActionBar(appBar)
        supportActionBar?.title = "KPR"
        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        //default fragment
        supportFragmentManager
            .beginTransaction()
            .replace(R.id.fragmentBantuanContainer, MenuBantuanKPRFragment())
            .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
            .commit()
    }


    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }

    fun goFragmentPembayaranKPR() {
        supportFragmentManager
            .beginTransaction()
            .replace(R.id.fragmentBantuanContainer, BantuanPembayaranKPRFragment())
            .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
            .commit()

        supportActionBar?.title = "Cara Pengajuan KPR"
    }

    fun goFragmentPengajuan() {
        supportFragmentManager
            .beginTransaction()
            .replace(R.id.fragmentBantuanContainer, BantuanPengajuanKPRFragment())
            .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
            .commit()

        supportActionBar?.title = "Cara Pembayaran KPR"
    }
}