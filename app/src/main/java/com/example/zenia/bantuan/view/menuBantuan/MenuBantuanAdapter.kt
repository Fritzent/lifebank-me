package com.example.zenia.bantuan.view.menuBantuan

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.example.zenia.R
import com.example.zenia.bantuan.model.menu.MenuBantuan
import kotlinx.android.synthetic.main.item_menu_bantuan.view.*

class MenuBantuanAdapter(private val listMenuBantuan: ArrayList<MenuBantuan>, val presenter: MenuBantuanPresenter): RecyclerView.Adapter<MenuBantuanAdapter.ViewHolder>() {

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_menu_bantuan, parent, false)

        return ViewHolder(
            view
        )
    }

    override fun getItemCount(): Int {
        return listMenuBantuan.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val image = holder.itemView.logo_menu

        Glide.with(holder.itemView.context)
            .load(listMenuBantuan[position].icon)
            .into(image)

        holder.itemView.tvTitle.text = listMenuBantuan[position].name

        holder.itemView.setOnClickListener {
            presenter.goToBantuan(position)
        }
    }
}