package com.example.zenia.bantuan.view.kalkulator

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.fragment.app.FragmentTransaction
import com.example.zenia.R
import com.example.zenia.bantuan.view.kalkulator.fragment.MenuBantuanKalkulatorFragment
import com.example.zenia.bantuan.view.transfer.fragment.BantuanCaraTransferFragment
import com.example.zenia.bantuan.view.transfer.fragment.BantuanTransferGagalFragment
import com.example.zenia.bantuan.view.transfer.fragment.MenuBantuanTransferFragment
import com.example.zenia.databinding.ActivityBantuanKalkulatorBinding
import kotlinx.android.synthetic.main.toolbar.view.*

class BantuanKalkulatorActivity : AppCompatActivity() {
    private lateinit var binding: ActivityBantuanKalkulatorBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityBantuanKalkulatorBinding.inflate(layoutInflater)

        setContentView(binding.root)


        val appBar = binding.menuToolbar.appBarLayout.toolbar
        setSupportActionBar(appBar)
        supportActionBar?.title = "Kalkulator"
        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        //default fragment
        supportFragmentManager
            .beginTransaction()
            .replace(R.id.fragmentBantuanContainer, MenuBantuanKalkulatorFragment())
            .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
            .commit()

    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }

    fun toFragmentBantuanKemampuan() {
        supportFragmentManager
            .beginTransaction()
            .replace(R.id.fragmentBantuanContainer, BantuanCaraTransferFragment())
            .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
            .commit()

        supportActionBar?.title = "Cara Hitung Kemampuan Cicil"
    }

    fun toFragmentBantuanDP() {
        supportFragmentManager
            .beginTransaction()
            .replace(R.id.fragmentBantuanContainer, BantuanTransferGagalFragment())
            .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
            .commit()

        supportActionBar?.title = "Cara Hitung Setoran DP"
    }

    fun toFragmentBantuanKPR() {
        supportFragmentManager
            .beginTransaction()
            .replace(R.id.fragmentBantuanContainer, BantuanTransferGagalFragment())
            .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
            .commit()

        supportActionBar?.title = "Cara Hitung Simulasi KPR "
    }
}