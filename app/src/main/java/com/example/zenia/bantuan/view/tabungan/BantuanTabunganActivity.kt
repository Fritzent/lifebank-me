package com.example.zenia.bantuan.view.tabungan

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.fragment.app.FragmentTransaction
import com.example.zenia.R
import com.example.zenia.bantuan.view.kalkulator.fragment.MenuBantuanKalkulatorFragment
import com.example.zenia.bantuan.view.tabungan.fragment.BantuanCaraBuatTabunganFragment
import com.example.zenia.bantuan.view.tabungan.fragment.BantuanCaraHentikanTabunganFragment
import com.example.zenia.bantuan.view.tabungan.fragment.BantuanMencairkanTabunganFragment
import com.example.zenia.bantuan.view.tabungan.fragment.MenuBantuanTabunganFragment
import com.example.zenia.bantuan.view.transfer.fragment.BantuanCaraTransferFragment
import com.example.zenia.bantuan.view.transfer.fragment.BantuanTransferGagalFragment
import com.example.zenia.databinding.ActivityBantuanTabunganBinding
import kotlinx.android.synthetic.main.toolbar.view.*

class BantuanTabunganActivity : AppCompatActivity() {
    private lateinit var binding: ActivityBantuanTabunganBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityBantuanTabunganBinding.inflate(layoutInflater)

        setContentView(binding.root)

        val appBar = binding.menuToolbar.appBarLayout.toolbar
        setSupportActionBar(appBar)
        supportActionBar?.title = "Tabungan"
        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        //default fragment
        supportFragmentManager
            .beginTransaction()
            .replace(R.id.fragmentBantuanContainer, MenuBantuanTabunganFragment())
            .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
            .commit()

    }
    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }

    fun toFragmentBantuanBuatTabungan(){
        supportFragmentManager
            .beginTransaction()
            .replace(R.id.fragmentBantuanContainer, BantuanCaraBuatTabunganFragment())
            .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
            .commit()

        supportActionBar?.title = "Cara Membuat Tabungan"
    }

    fun toFragmentBantuanHentikanTabungan(){
        supportFragmentManager
            .beginTransaction()
            .replace(R.id.fragmentBantuanContainer, BantuanCaraHentikanTabunganFragment())
            .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
            .commit()

        supportActionBar?.title = "Cara Menghetikan Tabungan"
    }
    fun toFragmentBantuanMencairkanTabungan(){
        supportFragmentManager
            .beginTransaction()
            .replace(R.id.fragmentBantuanContainer, BantuanMencairkanTabunganFragment())
            .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
            .commit()

        supportActionBar?.title = "Cara Mencairkan Tabungan"
    }
}