package com.example.zenia.bantuan.view.transfer

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.FragmentTransaction
import com.example.zenia.R
import com.example.zenia.bantuan.view.transfer.fragment.BantuanCaraTransferFragment
import com.example.zenia.bantuan.view.transfer.fragment.BantuanTransferGagalFragment
import com.example.zenia.bantuan.view.transfer.fragment.MenuBantuanTransferFragment
import com.example.zenia.databinding.ActivityBantuanTransferBinding
import kotlinx.android.synthetic.main.toolbar.view.*

class BantuanTransferActivity : AppCompatActivity() {
    private lateinit var binding: ActivityBantuanTransferBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityBantuanTransferBinding.inflate(layoutInflater)

        setContentView(binding.root)

        val appBar = binding.menuToolbar.appBarLayout.toolbar
        setSupportActionBar(appBar)
        supportActionBar?.title = "Transfer"
        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        //default fragment
        supportFragmentManager
            .beginTransaction()
            .replace(R.id.fragmentBantuanContainer, MenuBantuanTransferFragment())
            .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
            .commit()

    }
    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }

    fun toFragmentCaraTransfer(){
        supportFragmentManager
            .beginTransaction()
            .replace(R.id.fragmentBantuanContainer, BantuanCaraTransferFragment())
            .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
            .commit()

        supportActionBar?.title = "Cara Transfer"
    }

    fun toFragmentTransferGagal(){
        supportFragmentManager
            .beginTransaction()
            .replace(R.id.fragmentBantuanContainer, BantuanTransferGagalFragment())
            .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
            .commit()

        supportActionBar?.title = "Transfer Gagal"
    }
}