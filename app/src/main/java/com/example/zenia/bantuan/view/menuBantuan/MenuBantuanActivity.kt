package com.example.zenia.bantuan.view.menuBantuan

import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.zenia.R
import com.example.zenia.bantuan.model.menu.MenuBantuan
import com.example.zenia.bantuan.view.kalkulator.BantuanKalkulatorActivity
import com.example.zenia.bantuan.view.kpr.BantuanKPRActivity
import com.example.zenia.bantuan.view.paketData.BantuanPaketDataActivity
import com.example.zenia.bantuan.view.tabungan.BantuanTabunganActivity
import com.example.zenia.bantuan.view.tiket.BantuanTiketActivity
import com.example.zenia.bantuan.view.topup.BantuanTopUpActivity
import com.example.zenia.bantuan.view.transfer.BantuanTransferActivity
import com.example.zenia.databinding.ActivityMenuBantuanBinding
import kotlinx.android.synthetic.main.toolbar.view.*

class MenuBantuanActivity : AppCompatActivity(), MenuBantuanPresenter.Listener {
    lateinit var binding: ActivityMenuBantuanBinding
    lateinit var presenter: MenuBantuanPresenter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMenuBantuanBinding.inflate(layoutInflater)
        presenter = MenuBantuanPresenter(this)

        setContentView(binding.root)

        val appBar = binding.menuToolbar.appBarLayout.toolbar
        setSupportActionBar(appBar)
        supportActionBar?.title = "Bantuan"
        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        val menuBantuanList = arrayListOf(
            MenuBantuan(
                "Kalkulator",
                R.drawable.menu_kalkulator
            ),
            MenuBantuan(
                "Tabungan",
                R.drawable.menu_tabungan
            ),
            MenuBantuan(
                "KPR",
                R.drawable.menu_kpr
            ),
            MenuBantuan(
                "Transfer",
                R.drawable.menu_transer
            ),
            MenuBantuan(
                "Top-Up",
                R.drawable.menu_topup
            ),
            MenuBantuan(
                "Paket Data",
                R.drawable.menu_paketdata
            ),
            MenuBantuan(
                "Tiket",
                R.drawable.menu_tiket
            )
        )

        binding.rvMenuBantuan.layoutManager =
            LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false)
        binding.rvMenuBantuan.adapter = MenuBantuanAdapter(menuBantuanList, presenter)
    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }

    override fun goToBantuan(position: Int) {
        when (position) {
            0 -> startActivity(Intent(this, BantuanKalkulatorActivity::class.java))
            1 -> startActivity(Intent(this, BantuanTabunganActivity::class.java))
            2 -> startActivity(Intent(this, BantuanKPRActivity::class.java))
            3 -> startActivity(Intent(this, BantuanTransferActivity::class.java))
            4 -> startActivity(Intent(this, BantuanTopUpActivity::class.java))
            5 -> startActivity(Intent(this, BantuanPaketDataActivity::class.java))
            6 -> startActivity(Intent(this, BantuanTiketActivity::class.java))
        }
    }
}