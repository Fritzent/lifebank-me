package com.example.zenia.authentication.register

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import android.widget.Toast
import com.example.zenia.authentication.register.fragment.FormStepOneFragment
import com.example.zenia.databinding.ActivityRegisterBinding

class RegisterActivity : AppCompatActivity() {

    private lateinit var binding : ActivityRegisterBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityRegisterBinding.inflate(layoutInflater)
        val view = binding.root
        setContentView(view)

        //CHANGE THE VALUE OF THE PROGRESS TO HANDLE THE STATUS PROGRESSBAR
        binding.progressBar.progress = 16

        setupFrameLayout()

        binding.iconBack.setOnClickListener {
            finish()
        }
    }

    private var doubleBackToExitPressedOnce = false
    override fun onBackPressed() {
        if(doubleBackToExitPressedOnce){
            moveTaskToBack(true)
        }
        this.doubleBackToExitPressedOnce = true
        Toast.makeText(this, "Tekan Kembali Untuk Membatalkan Proses", Toast.LENGTH_SHORT).show()

        Handler().postDelayed({ doubleBackToExitPressedOnce = false }, 2000)
    }

    private fun setupFrameLayout() {
        supportFragmentManager.beginTransaction().add(binding.frameLayout.id,FormStepOneFragment(),"FormOne").commit()
    }
}