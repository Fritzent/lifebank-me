package com.example.zenia.authentication.register.fragment

import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.text.method.HideReturnsTransformationMethod
import android.text.method.PasswordTransformationMethod
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.MotionEvent
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.core.content.ContextCompat
import com.example.zenia.R
import com.example.zenia.authentication.login.LoginActivity
import com.google.android.material.bottomsheet.BottomSheetDialog
import kotlinx.android.synthetic.main.fragment_form_step_one.view.*
import kotlinx.android.synthetic.main.fragment_form_step_three.view.*
import java.util.*

class FormStepThreeFragment : Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val view = inflater.inflate(R.layout.fragment_form_step_three, container, false)

        var cekNamaEmpty = true
        var cekTanggalEmpty = true
        var cekKtpEmpty = true
        var cekNpwpEmpty = true

        view.et_input_nama_lengkap.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(p0: Editable?) {
                if(cekNamaEmpty && cekTanggalEmpty && cekKtpEmpty && cekNpwpEmpty) {
                    view.btn_lanjut.setBackgroundResource(R.drawable.custome_button_solid_gray)
                } else {
                    view.btn_lanjut.setBackgroundResource(R.drawable.custome_button_solid_blue)
                }
            }

            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {

            }

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
                if(p0!!.length <= 0) {
                    cekNamaEmpty = true
                } else if(p0.length > 0) {
                    cekNamaEmpty = false
                }
                if(view.et_input_nama_lengkap.text.isNotEmpty()) {
                    view.et_input_nama_lengkap.backgroundTintList =
                        ContextCompat.getColorStateList(view.context.applicationContext, R.color.colorBlue)
                } else if(view.et_input_nama_lengkap.text.isEmpty()) {
                    view.et_input_nama_lengkap.backgroundTintList =
                        ContextCompat.getColorStateList(view.context.applicationContext, R.color.colorWhiteC4)
                }
            }
        })

        view.et_input_nomor_ktp.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(p0: Editable?) {
                if(cekNamaEmpty && cekTanggalEmpty && cekKtpEmpty && cekNpwpEmpty) {
                    view.btn_lanjut.setBackgroundResource(R.drawable.custome_button_solid_gray)
                } else {
                    view.btn_lanjut.setBackgroundResource(R.drawable.custome_button_solid_blue)
                }
            }

            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {

            }

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
                if(p0!!.length <= 0) {
                    cekKtpEmpty = true
                } else if(p0.length > 0) {
                    cekKtpEmpty = false
                }
                if(view.et_input_nomor_ktp.text.isNotEmpty()) {
                    view.et_input_nomor_ktp.backgroundTintList =
                        ContextCompat.getColorStateList(view.context.applicationContext, R.color.colorBlue)
                } else if(view.et_input_nomor_ktp.text.isEmpty()) {
                    view.et_input_nomor_ktp.backgroundTintList =
                        ContextCompat.getColorStateList(view.context.applicationContext, R.color.colorWhiteC4)
                }
            }
        })

        view.et_input_nomor_npwp.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(p0: Editable?) {
                if(cekNamaEmpty && cekTanggalEmpty && cekKtpEmpty && cekNpwpEmpty) {
                    view.btn_lanjut.setBackgroundResource(R.drawable.custome_button_solid_gray)
                } else {
                    view.btn_lanjut.setBackgroundResource(R.drawable.custome_button_solid_blue)
                }
            }

            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {

            }

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
                if(p0!!.length <= 0) {
                    cekNpwpEmpty = true
                } else if(p0.length > 0) {
                    cekNpwpEmpty = false
                }
                if(view.et_input_nomor_npwp.text.isNotEmpty()) {
                    view.et_input_nomor_npwp.backgroundTintList =
                        ContextCompat.getColorStateList(view.context.applicationContext, R.color.colorBlue)
                } else if(view.et_input_nomor_npwp.text.isEmpty()) {
                    view.et_input_nomor_npwp.backgroundTintList =
                        ContextCompat.getColorStateList(view.context.applicationContext, R.color.colorWhiteC4)
                }
            }
        })

        view.et_input_tanggal_lahir.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(p0: Editable?) {
                if(cekNamaEmpty && cekTanggalEmpty && cekKtpEmpty && cekNpwpEmpty) {
                    view.btn_lanjut.setBackgroundResource(R.drawable.custome_button_solid_gray)
                } else {
                    view.btn_lanjut.setBackgroundResource(R.drawable.custome_button_solid_blue)
                }
            }

            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {

            }

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
                if(p0!!.length <= 0) {
                    cekTanggalEmpty = true
                } else if(p0.length > 0) {
                    cekTanggalEmpty = false
                }
                if(view.et_input_tanggal_lahir.text.isNotEmpty()) {
                    view.et_input_tanggal_lahir.backgroundTintList =
                        ContextCompat.getColorStateList(view.context.applicationContext, R.color.colorBlue)
                } else if(view.et_input_tanggal_lahir.text.isEmpty()) {
                    view.et_input_tanggal_lahir.backgroundTintList =
                        ContextCompat.getColorStateList(view.context.applicationContext, R.color.colorWhiteC4)
                }
            }
        })

        view.et_input_tanggal_lahir.setOnTouchListener(View.OnTouchListener { _, event ->
            if(event.rawX >= view.et_input_tanggal_lahir.right - view.et_input_tanggal_lahir.compoundDrawables[DRAWABLE_END].bounds.width()){
                val bottomSheetDialog = BottomSheetDialog(view.context, R.style.BottomSheetDialogTheme)
                val bottomSheetView = LayoutInflater.from(view.context).inflate(
                    R.layout.layout_bottom_sheet_date_picker,
                    view.findViewById(R.id.bottom_sheet_date_picker)
                )
                var dataGet = ""
                val datePicker = bottomSheetView.findViewById<DatePicker>(R.id.dp_pick_date)
                val today = Calendar.getInstance()
                datePicker.init(today.get(Calendar.YEAR), today.get(Calendar.MONTH), today.get(Calendar.DAY_OF_MONTH)){
                        view, year, month, day ->
                    val month = month + 1
                    val msg = "$year-$day-$month"
                    dataGet = msg
                }
                bottomSheetView.findViewById<ImageView>(R.id.btn_exit_date_sheet).setOnClickListener {
                    bottomSheetDialog.dismiss()
                }
                bottomSheetView.findViewById<TextView>(R.id.btn_selesai).setOnClickListener {
                    view.et_input_tanggal_lahir.setText(dataGet)
                    bottomSheetDialog.dismiss()
                }

                bottomSheetDialog.setContentView(bottomSheetView)
                bottomSheetDialog.show()
            }
            false
        })

        view.btn_lanjut.setOnClickListener {
            val fm = fragmentManager

            val progressBarUpdate = activity?.findViewById<ProgressBar>(R.id.progress_bar)
            if(progressBarUpdate != null) {
                progressBarUpdate.progress = 64
            }

            fm!!.beginTransaction()
                .replace(R.id.frame_layout, FormStepFourFragment(), "FormFour").commit()
        }

        return view
    }

    companion object {

        const val DRAWABLE_END = 2
        @JvmStatic
        fun newInstance(param1: String, param2: String) =
            FormStepThreeFragment().apply {
                arguments = Bundle().apply {
                }
            }
    }
}