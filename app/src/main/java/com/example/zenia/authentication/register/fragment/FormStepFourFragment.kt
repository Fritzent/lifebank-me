package com.example.zenia.authentication.register.fragment

import android.Manifest.permission.CAMERA
import android.Manifest.permission.READ_EXTERNAL_STORAGE
import android.app.Activity
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.net.Uri
import android.os.Bundle
import android.os.Environment
import android.provider.MediaStore
import android.util.Log
import android.util.TypedValue
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ProgressBar
import android.widget.Toast
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.core.content.FileProvider
import androidx.fragment.app.Fragment
import com.example.zenia.R
import kotlinx.android.synthetic.main.fragment_form_step_four.*
import kotlinx.android.synthetic.main.fragment_form_step_four.view.*
import java.io.File
import java.io.IOException
import java.text.SimpleDateFormat
import java.util.*

class FormStepFourFragment : Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val view = inflater.inflate(R.layout.fragment_form_step_four, container, false)
        view.layout_preview_image.visibility = View.GONE
        view.section_foto_ktp.setOnClickListener {
            if (checkPersmission()) takePicture() else requestPermission()
        }
        view.section_foto_selfie.setOnClickListener {
            if (checkPersmission()) takePictureSelfie() else requestPermissionSelfie()
        }
        return view
    }

    override fun onResume() {
        super.onResume()
        checkingStatus()
    }

    private fun checkingStatus() {
        if(statusKTP) {
            iv_detector_ktp.setImageResource(R.drawable.greencek)
            section_foto_ktp.isEnabled = false
        } else {
            iv_detector_ktp.setImageResource(R.drawable.custome_background_dot)
            section_foto_ktp.isEnabled = true
        }
        if(statusSelfie) {
            iv_detector_selfie.setImageResource(R.drawable.greencek)
            section_foto_selfie.isEnabled = false
        } else {
            iv_detector_selfie.setImageResource(R.drawable.custome_background_dot)
            section_foto_selfie.isEnabled = true
        }
        if(statusKTP && statusSelfie) {
            btn_lanjut.isEnabled = true
            btn_lanjut.setBackgroundResource(R.drawable.custome_button_solid_blue)
            btn_lanjut.setOnClickListener {
                val fm = fragmentManager

                val progressBarUpdate = activity?.findViewById<ProgressBar>(R.id.progress_bar)
                if(progressBarUpdate != null) {
                    progressBarUpdate.progress = 80
                }

                fm!!.beginTransaction()
                    .replace(R.id.frame_layout, FormStepFiveFragment(), "FormFive").commit()
            }
        } else {
            btn_lanjut.isEnabled = false
            btn_lanjut.setBackgroundResource(R.drawable.custome_button_solid_gray)
        }
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        when (requestCode) {
            PERMISSION_REQUEST_CODE -> {

                if ((grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED)
                    && grantResults[1] == PackageManager.PERMISSION_GRANTED) {

                    takePicture()

                } else {
                    Toast.makeText(this.context, "Permission Denied", Toast.LENGTH_SHORT).show()
                }
                return
            }
            PERMISSION_REQUEST_CODE_SELFIE -> {

                if ((grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED)
                    && grantResults[1] == PackageManager.PERMISSION_GRANTED) {

                    takePictureSelfie()

                } else {
                    Toast.makeText(this.context, "Permission Denied", Toast.LENGTH_SHORT).show()
                }
                return
            }

            else -> {

            }
        }
    }

    private fun takePicture() {

        val intent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
        val file: File = createFile()

        val uri: Uri? = this.context?.let {
            FileProvider.getUriForFile(
                it,
                "com.example.zenia",
                file
            )
        }
        intent.putExtra(MediaStore.EXTRA_OUTPUT, uri)
        startActivityForResult(intent, REQUEST_IMAGE_CAPTURE)

    }

    private fun takePictureSelfie() {

        val intent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
        val file: File = createFileSelfie()

        val uri: Uri? = this.context?.let {
            FileProvider.getUriForFile(
                it,
                "com.example.zenia",
                file
            )
        }
        intent.putExtra(MediaStore.EXTRA_OUTPUT, uri)
        startActivityForResult(intent, REQUEST_IMAGE_CAPTURE_SELFIE)

    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (requestCode == REQUEST_IMAGE_CAPTURE && resultCode == Activity.RESULT_OK) {
            statusKTP = true
            layout_menu_form.visibility = View.GONE
            layout_preview_image.visibility = View.VISIBLE
            tv_information.visibility = View.VISIBLE

            if(statusKTP) {
                image_priview.requestLayout()
                image_priview.layoutParams.height = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 135f, resources.displayMetrics).toInt()
                image_priview.layoutParams.width = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 240f, resources.displayMetrics).toInt()
            }

            btn_confirm.setOnClickListener {
                layout_menu_form.visibility = View.VISIBLE
                layout_preview_image.visibility = View.GONE
            }
            retake_picture.setOnClickListener {
                if (checkPersmission()) takePicture() else requestPermission()
            }
            //To get the File for further usage
            val auxFile = File(mCurrentPhotoPath)

            Log.d("CEKING", "THIS IS FILE KTP : $auxFile")


            val bitmap: Bitmap = BitmapFactory.decodeFile(mCurrentPhotoPath)
            image_priview.setImageBitmap(bitmap)

        }
        if(requestCode == REQUEST_IMAGE_CAPTURE_SELFIE && resultCode == Activity.RESULT_OK) {
            statusSelfie = true
            layout_menu_form.visibility = View.GONE
            layout_preview_image.visibility = View.VISIBLE
            tv_information.visibility = View.GONE

            if(statusSelfie) {
                image_priview.requestLayout()
                image_priview.layoutParams.height = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 300f, resources.displayMetrics).toInt()
                image_priview.layoutParams.width = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 200f, resources.displayMetrics).toInt()
            }

            btn_confirm.setOnClickListener {
                layout_menu_form.visibility = View.VISIBLE
                layout_preview_image.visibility = View.GONE
            }
            retake_picture.setOnClickListener {
                if (checkPersmission()) takePictureSelfie() else requestPermission()
            }
            //To get the File for further usage
            val auxFile = File(mCurrentPhotoPathSelfie)

            Log.d("CEKING", "THIS IS FILE SELFIE : $auxFile")


            val bitmap: Bitmap = BitmapFactory.decodeFile(mCurrentPhotoPathSelfie)
            image_priview.setImageBitmap(bitmap)
        }
    }

    private fun checkPersmission(): Boolean {
        return (this.context?.let { ContextCompat.checkSelfPermission(it, CAMERA) } ==
                PackageManager.PERMISSION_GRANTED && ContextCompat.checkSelfPermission(
            this.context!!,
            READ_EXTERNAL_STORAGE
        ) == PackageManager.PERMISSION_GRANTED)
    }

    private fun requestPermission() {
        activity?.parent?.let { ActivityCompat.requestPermissions(it, arrayOf(READ_EXTERNAL_STORAGE, CAMERA), PERMISSION_REQUEST_CODE) }
    }

    private fun requestPermissionSelfie() {
        activity?.parent?.let { ActivityCompat.requestPermissions(it, arrayOf(READ_EXTERNAL_STORAGE, CAMERA), PERMISSION_REQUEST_CODE_SELFIE) }
    }

    @Throws(IOException::class)
    private fun createFile(): File {
        // Create an image file name
        val timeStamp: String = SimpleDateFormat("yyyyMMdd_HHmmss").format(Date())
        val storageDir: File? = activity?.getExternalFilesDir(Environment.DIRECTORY_PICTURES)
        return File.createTempFile(
            "JPEG_${timeStamp}_", /* prefix */
            ".jpg", /* suffix */
            storageDir /* directory */
        ).apply {
            // Save a file: path for use with ACTION_VIEW intents
            mCurrentPhotoPath = absolutePath
        }
    }

    @Throws(IOException::class)
    private fun createFileSelfie(): File {
        // Create an image file name
        val timeStamp: String = SimpleDateFormat("yyyyMMdd_HHmmss").format(Date())
        val storageDir: File? = activity?.getExternalFilesDir(Environment.DIRECTORY_PICTURES)
        return File.createTempFile(
            "JPEG_${timeStamp}_", /* prefix */
            ".jpg", /* suffix */
            storageDir /* directory */
        ).apply {
            // Save a file: path for use with ACTION_VIEW intents
            mCurrentPhotoPathSelfie = absolutePath
        }
    }

    companion object {
        const val REQUEST_IMAGE_CAPTURE = 1
        const val REQUEST_IMAGE_CAPTURE_SELFIE = 2
        private const val PERMISSION_REQUEST_CODE: Int = 102
        private const val PERMISSION_REQUEST_CODE_SELFIE: Int = 101
        private var mCurrentPhotoPath = ""
        private var mCurrentPhotoPathSelfie = ""
        private var statusKTP = false
        private var statusSelfie = false
        @JvmStatic
        fun newInstance() =
            FormStepFourFragment().apply {
                arguments = Bundle().apply {
                }
            }
    }
}