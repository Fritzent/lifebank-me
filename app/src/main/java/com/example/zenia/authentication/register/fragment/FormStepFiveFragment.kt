package com.example.zenia.authentication.register.fragment

import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ProgressBar
import com.example.zenia.R
import kotlinx.android.synthetic.main.fragment_form_step_five.view.*

class FormStepFiveFragment : Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val view = inflater.inflate(R.layout.fragment_form_step_five, container, false)

        view.input_username.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(p0: Editable?) {

            }

            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {

            }

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {

            }
        })

        view.input_password.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(p0: Editable?) {

            }

            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {

            }

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {

            }
        })

        view.input_confirm_password.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(p0: Editable?) {

            }

            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {

            }

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {

            }
        })

        view.btn_lanjut.setOnClickListener {
            val fm = fragmentManager

            val progressBarUpdate = activity?.findViewById<ProgressBar>(R.id.progress_bar)
            if(progressBarUpdate != null) {
                progressBarUpdate.progress = 100
            }

            fm!!.beginTransaction()
                .replace(R.id.frame_layout, FormStepSixFragment(), "FormSix").commit()
        }

        return view
    }

    companion object {
        @JvmStatic
        fun newInstance() =
            FormStepFiveFragment().apply {
                arguments = Bundle().apply {
                }
            }
    }
}