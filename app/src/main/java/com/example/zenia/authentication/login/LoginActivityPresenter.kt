package com.example.zenia.authentication.login

import android.app.ProgressDialog
import android.content.Context
import android.content.SharedPreferences
import com.example.zenia.authentication.login.LoginActivity.Companion.EMAIL
import com.example.zenia.authentication.login.LoginActivity.Companion.ID
import com.example.zenia.authentication.login.LoginActivity.Companion.TOKEN
import com.example.zenia.authentication.login.LoginActivity.Companion.USERNAME
import com.example.zenia.network.ApiClient
import com.example.zenia.pojo.PostAuthLoginBody
import com.example.zenia.pojo.PostAuthLoginResponse
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class LoginActivityPresenter(val listener: Listener, val context: Context) {

    fun loginPerson(email : String, password: String, sharedPreferences: SharedPreferences){

        @Suppress("DEPRECATION")
        val progressDialog = ProgressDialog(context)
        progressDialog.setTitle("Processing")
        @Suppress("DEPRECATION")
        progressDialog.setMessage("Tunggu Sebentar")

        progressDialog.show()

        val person = PostAuthLoginBody(
            email,
            password
        )

        ApiClient.apiService.authLogin(person).enqueue(object :
            Callback<PostAuthLoginResponse> {
            override fun onFailure(call: Call<PostAuthLoginResponse>, t: Throwable) {
                progressDialog.dismiss()
                listener.onLoginFailure(t.toString(), "Terjadi Kesalahan Pada Api Service")
            }

            override fun onResponse(
                call: Call<PostAuthLoginResponse>,
                response: Response<PostAuthLoginResponse>
            ) {
                if(response.isSuccessful && response.body()?.status == "success") {
                    val editor = sharedPreferences.edit()
                    editor.putString(ID, response.body()?.data?.id)
                    editor.putString(USERNAME, response.body()?.data?.username)
                    editor.putString(EMAIL, response.body()?.data?.email)
                    editor.putString(TOKEN, response.body()?.data?.accessToken)

                    val toastStatus = editor.commit()

                    progressDialog.dismiss()

                    if(toastStatus){
                        listener.onLoginSuccess("Login Success", "Data Berhasil Disimpan")
                    }else{
                        listener.onLoginSuccess("Login Success", "Data Gagal Disimpan")
                    }
                }else {
                    listener.onLoginFailure("Login Failed", "Data Tidak Ada")
                }
            }
        })
    }
    interface Listener{
        fun onLoginSuccess(successMessage: String, successSaveData: String)
        fun onLoginFailure(failureMessage: String, failedSaveData: String)
    }
}