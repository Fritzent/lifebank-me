package com.example.zenia.authentication.register.fragment

import android.app.ProgressDialog
import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.os.Bundle
import android.os.Handler
import android.text.Editable
import android.text.InputType
import android.text.TextWatcher
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.WindowManager
import android.widget.ProgressBar
import android.widget.Toast
import androidx.fragment.app.Fragment
import com.example.zenia.R
import com.example.zenia.authentication.login.LoginActivity
import com.example.zenia.home.activity.HomeActivity
import com.example.zenia.network.ApiClient
import com.example.zenia.pojo.*
import com.google.gson.Gson
import kotlinx.android.synthetic.main.fragment_form_step_two.view.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response


class FormStepTwoFragment : Fragment() {
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val view = inflater.inflate(R.layout.fragment_form_step_two, container, false)

        val sharedPreference = activity?.getSharedPreferences(SP_NAME, Context.MODE_PRIVATE)
        if (sharedPreference != null) {
            USER_ID = sharedPreference.getString(ID, "No Data").toString()
        }

        @Suppress("DEPRECATION")
        val progressDialog = ProgressDialog(activity)
        progressDialog.setTitle("Processing")
        @Suppress("DEPRECATION")
        progressDialog.setMessage("Silahkan Tunggu Sebentar...")

        //this is to set the keypad to number
        view.tv_error.visibility = View.GONE
        view.pin_input.inputType = InputType.TYPE_CLASS_NUMBER
        view.pin_input.requestFocus()

        view.tv_watcher.setText(R.string.tv_watcher_form_two)

        //this is to handle the soft input keyboard
        if(view.pin_input.isFocusable) {
            view.pin_input.showSoftInputOnFocus
        }



        view.pin_input.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(p0: Editable?) {
                if (p0 != null) {
                    if(p0.length == 6) {
                        //here need code for the progress bar to show
                        progressDialog.show()

                        PIN_OTP = view.pin_input.text.toString()

                        val otpBody = PostAuthValidationOtpBody (
                            PIN_OTP
                        )

                        ApiClient.apiService.validationOtp(otpBody, userId = USER_ID).enqueue(
                            object : Callback<PostAuthValidationOtpResponse> {
                                override fun onFailure(
                                    call: Call<PostAuthValidationOtpResponse>,
                                    t: Throwable
                                ) {
                                    // here code if its error in consume API
                                    //here need code for the progress bar to dismis
                                    progressDialog.dismiss()
                                    Toast.makeText(activity, "Error in Consume Api", Toast.LENGTH_LONG).show()
                                }

                                override fun onResponse(
                                    call: Call<PostAuthValidationOtpResponse>,
                                    response: Response<PostAuthValidationOtpResponse>
                                ) {
                                    if(response.isSuccessful && response.body()?.status == "Success") {
                                        val fm = fragmentManager

                                        val progressBarUpdate = activity?.findViewById<ProgressBar>(R.id.progress_bar)
                                        if (progressBarUpdate != null) {
                                            progressBarUpdate.progress = 48
                                        }

                                        progressDialog.dismiss()

                                        fm!!.beginTransaction()
                                            .replace(R.id.frame_layout, FormStepThreeFragment(), "FormThree").commit()
                                    } else if(response.isSuccessful && response.body()?.status == "Error") {
                                        progressDialog.dismiss()

                                        view.tv_error.text = response.body()!!.message
                                        view.tv_error.visibility = View.VISIBLE
                                        view.pin_input.setLineColor(Color.parseColor("#EE0D0D"))
                                        view.pin_input.cursorColor = Color.parseColor("#EE0D0D")
                                        view.pin_input.requestFocus()

                                        view.tv_error.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_baseline_error, 0, 0, 0)
                                        view.tv_error.setTextColor(Color.parseColor("#EE0D0D"))

                                        @Suppress("DEPRECATION")
                                        Handler().postDelayed({
                                            view.tv_error.visibility = View.GONE
                                        }, 2000)

                                    }

                                }
                            }
                        )
                    } else {
                        view.tv_error.visibility = View.GONE
                        view.pin_input.setLineColor(Color.parseColor("#3BA1FF"))
                        view.pin_input.cursorColor = Color.parseColor("#3BA1FF")
                    }
                }
            }

            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {

            }

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {

            }

        })

        view.btn_kirim_ulang.setOnClickListener {
            progressDialog.show()
            ApiClient.apiService.requestNewOtp(userId = USER_ID).enqueue(object :
                Callback<GetRequestNewOtpResponse> {
                override fun onFailure(call: Call<GetRequestNewOtpResponse>, t: Throwable) {
                    progressDialog.dismiss()
                    Toast.makeText(activity, "Error in Consume API", Toast.LENGTH_LONG).show()
                }

                override fun onResponse(
                    call: Call<GetRequestNewOtpResponse>,
                    response: Response<GetRequestNewOtpResponse>
                ) {
                    if(response.isSuccessful && response.body()?.status == "Success") {
                        view.tv_error.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_info_blue, 0, 0, 0)
                        view.tv_error.setTextColor(Color.parseColor("#3BA1FF"))
                        view.tv_error.text = response.body()!!.message

                        view.tv_error.visibility = View.VISIBLE

                        progressDialog.dismiss()

                        @Suppress("DEPRECATION")
                        Handler().postDelayed({
                            view.tv_error.visibility = View.GONE
                        }, 2000)
                    }
                }
            })
        }

        return view
    }

    companion object {
        private const val SP_NAME = "data_register"
        private const val ID = "id"
        private lateinit var PIN_OTP : String
        private lateinit var USER_ID : String
        @JvmStatic
        fun newInstance(param1: String, param2: String) =
            FormStepTwoFragment().apply {
                arguments = Bundle().apply {
                }
            }
    }
}