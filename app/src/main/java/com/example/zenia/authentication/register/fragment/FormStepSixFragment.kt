package com.example.zenia.authentication.register.fragment

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.example.zenia.R
import kotlinx.android.synthetic.main.fragment_form_step_six.view.*

class FormStepSixFragment : Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val view = inflater.inflate(R.layout.fragment_form_step_six, container, false)
        view.section_konfirmasi_pin.visibility = View.GONE
        view.tv_instruction.setText(R.string.text_buat_pin)
        return view
    }

    companion object {
        @JvmStatic
        fun newInstance(param1: String, param2: String) =
            FormStepSixFragment().apply {
                arguments = Bundle().apply {
                }
            }
    }
}