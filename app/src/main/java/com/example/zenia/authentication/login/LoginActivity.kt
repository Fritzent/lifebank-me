package com.example.zenia.authentication.login

import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.text.Editable
import android.text.TextWatcher
import android.text.method.HideReturnsTransformationMethod
import android.text.method.PasswordTransformationMethod
import android.view.MotionEvent
import android.view.View.OnTouchListener
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import com.example.zenia.R
import com.example.zenia.authentication.register.RegisterActivity
import com.example.zenia.databinding.ActivityLoginBinding
import com.example.zenia.home.activity.HomeActivity


class LoginActivity : AppCompatActivity(), LoginActivityPresenter.Listener {

    companion object {
        const val ID = "id"
        const val USERNAME = "username"
        const val EMAIL = "email"
        const val TOKEN = "token"
        const val SP_NAME = "data_user"
        const val DRAWABLE_END = 2
    }
    private lateinit var binding: ActivityLoginBinding
    private lateinit var presenter: LoginActivityPresenter

    @SuppressLint("ClickableViewAccessibility")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityLoginBinding.inflate(layoutInflater)
        val view = binding.root
        setContentView(view)

        presenter = LoginActivityPresenter(this, this)
        val sharedPreferences = getSharedPreferences(SP_NAME, Context.MODE_PRIVATE)

        binding.inputUsername.requestFocus()

        binding.inputUsername.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(p0: Editable?) {
            }

            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
            }

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
                if(binding.inputUsername.text.isEmpty() && binding.inputPassword.text.isEmpty()){
                    binding.btnLogin.setBackgroundResource(R.drawable.custome_button_solid_gray)
                    binding.inputUsername.backgroundTintList =
                        ContextCompat.getColorStateList(applicationContext, R.color.colorWhiteC4)
                    binding.inputPassword.backgroundTintList =
                        ContextCompat.getColorStateList(applicationContext, R.color.colorWhiteC4)
                } else {
                    binding.btnLogin.setBackgroundResource(R.drawable.custome_button_solid_blue)
                    if(binding.inputUsername.text.isNotEmpty() && binding.inputPassword.text.isEmpty()){
                        binding.inputUsername.backgroundTintList =
                            ContextCompat.getColorStateList(applicationContext, R.color.colorBlue)
                        binding.inputPassword.backgroundTintList =
                            ContextCompat.getColorStateList(applicationContext, R.color.colorWhiteC4)
                    } else if(binding.inputUsername.text.isEmpty() && binding.inputPassword.text.isNotEmpty()){
                        binding.inputUsername.backgroundTintList =
                            ContextCompat.getColorStateList(applicationContext, R.color.colorWhiteC4)
                        binding.inputPassword.backgroundTintList =
                            ContextCompat.getColorStateList(applicationContext, R.color.colorBlue)
                    } else {
                        binding.inputUsername.backgroundTintList =
                            ContextCompat.getColorStateList(applicationContext, R.color.colorBlue)
                        binding.inputPassword.backgroundTintList =
                            ContextCompat.getColorStateList(applicationContext, R.color.colorBlue)
                    }
                }
            }
        })

        binding.inputPassword.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(p0: Editable?) {

            }

            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
            }

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
                if(binding.inputUsername.text.isEmpty() && binding.inputPassword.text.isEmpty()){
                    binding.btnLogin.setBackgroundResource(R.drawable.custome_button_solid_gray)
                    binding.inputUsername.backgroundTintList =
                        ContextCompat.getColorStateList(applicationContext, R.color.colorWhiteAF)
                    binding.inputPassword.backgroundTintList =
                        ContextCompat.getColorStateList(applicationContext, R.color.colorWhiteAF)
                } else {
                    binding.btnLogin.setBackgroundResource(R.drawable.custome_button_solid_blue)
                    if(binding.inputUsername.text.isNotEmpty() && binding.inputPassword.text.isEmpty()){
                        binding.inputUsername.backgroundTintList =
                            ContextCompat.getColorStateList(applicationContext, R.color.colorBlue)
                        binding.inputPassword.backgroundTintList =
                            ContextCompat.getColorStateList(applicationContext, R.color.colorWhiteAF)
                    } else if(binding.inputUsername.text.isEmpty() && binding.inputPassword.text.isNotEmpty()){
                        binding.inputUsername.backgroundTintList =
                            ContextCompat.getColorStateList(applicationContext, R.color.colorWhiteAF)
                        binding.inputPassword.backgroundTintList =
                            ContextCompat.getColorStateList(applicationContext, R.color.colorBlue)
                    } else {
                        binding.inputUsername.backgroundTintList =
                            ContextCompat.getColorStateList(applicationContext, R.color.colorBlue)
                        binding.inputPassword.backgroundTintList =
                            ContextCompat.getColorStateList(applicationContext, R.color.colorBlue)
                    }
                }
            }
        })

        var cekClick = "one"

        //Here Code To Handle Toggle Password Hide/Show
        binding.inputPassword.setOnTouchListener(OnTouchListener { _, event ->
            if (event.action == MotionEvent.ACTION_UP) {
                if (event.rawX >= binding.inputPassword.right - binding.inputPassword.compoundDrawables[DRAWABLE_END].bounds.width() && cekClick == "one"
                ) {
                    cekClick = "two"
                    binding.inputPassword.transformationMethod = HideReturnsTransformationMethod.getInstance()
                    return@OnTouchListener true
                }
                if(event.rawX >= binding.inputPassword.right - binding.inputPassword.compoundDrawables[DRAWABLE_END].bounds.width() && cekClick == "two") {
                    cekClick = "one"
                    binding.inputPassword.transformationMethod = PasswordTransformationMethod.getInstance()
                    return@OnTouchListener true
                }

            }
            false
        })

        binding.btnLogin.setOnClickListener {
            if(binding.inputUsername.text.isEmpty() && binding.inputPassword.text.isEmpty()){
                Toast.makeText(this, "Username dan Password Kosong", Toast.LENGTH_LONG).show()
                binding.inputUsername.requestFocus()
            } else if(binding.inputUsername.text.isEmpty() && binding.inputPassword.text.isNotEmpty()){
                Toast.makeText(this, "Username anda kosong", Toast.LENGTH_LONG).show()
                binding.inputUsername.requestFocus()
            } else if(binding.inputUsername.text.isNotEmpty() && binding.inputPassword.text.isEmpty()){
                Toast.makeText(this, "Password anda kosong", Toast.LENGTH_LONG).show()
                binding.inputPassword.requestFocus()
            } else{
                presenter.loginPerson(binding.inputUsername.text.toString(), binding.inputPassword.text.toString(), sharedPreferences)
            }
        }

        binding.textDaftar.setOnClickListener {
            startActivity(Intent(applicationContext, RegisterActivity::class.java))
        }
    }

    private var doubleBackToExitPressedOnce = false
    override fun onBackPressed() {
        if(doubleBackToExitPressedOnce){
            moveTaskToBack(true)
        }
        this.doubleBackToExitPressedOnce = true
        Toast.makeText(this, "Tekan lagi untuk keluar", Toast.LENGTH_SHORT).show()

        @Suppress("DEPRECATION")
        Handler().postDelayed({ doubleBackToExitPressedOnce = false }, 2000)
    }

    override fun onLoginSuccess(successMessage: String, successSaveData: String) {
//        Toast.makeText(this, "$successMessage dan $successSaveData", Toast.LENGTH_LONG).show()
        startActivity(Intent(applicationContext, HomeActivity::class.java))
        finish()
    }

    override fun onLoginFailure(failureMessage: String, failedSaveData: String) {
        Toast.makeText(this, "$failureMessage dan $failedSaveData", Toast.LENGTH_LONG).show()
    }
}