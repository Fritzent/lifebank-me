package com.example.zenia.authentication.register.fragment

import android.app.ProgressDialog
import android.content.Context
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ProgressBar
import android.widget.Toast
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import com.example.zenia.R
import com.example.zenia.network.ApiClient
import com.example.zenia.pojo.PostAuthRegistrationBody
import com.example.zenia.pojo.PostAuthRegistrationResponse
import com.google.gson.Gson
import kotlinx.android.synthetic.main.fragment_form_step_one.view.*
import kotlinx.android.synthetic.main.fragment_form_step_one.view.tv_watcher
import kotlinx.android.synthetic.main.fragment_form_step_two.view.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response


class FormStepOneFragment : Fragment() {
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val view = inflater.inflate(R.layout.fragment_form_step_one, container, false)

        @Suppress("DEPRECATION")
        val progressDialog = ProgressDialog(activity)
        progressDialog.setTitle("Processing")
        @Suppress("DEPRECATION")
        progressDialog.setMessage("Silahkan Tunggu Sebentar...")

        var cekEmailEmpty = true
        var cekHandphoneEmpty = true

        if(cekEmailEmpty && cekHandphoneEmpty) {
            view.tv_watcher.setText(R.string.tv_watcher_form_one)
        }

        view.et_input_email.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(p0: Editable?) {
                if(cekEmailEmpty && cekHandphoneEmpty) {
                    view.btn_lanjut_one.setBackgroundResource(R.drawable.custome_button_solid_gray)
                } else {
                    view.btn_lanjut_one.setBackgroundResource(R.drawable.custome_button_solid_blue)
                }

                EMAIL = view.et_input_email.text.toString()

                Log.d("CEKING", "THIS IS THE VALUE OF EMAIL :$EMAIL")
            }

            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
            }

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
                view.tv_watcher.setText(R.string.tv_watcher_form_one_email)
                if(p0!!.isEmpty()) {
                    cekEmailEmpty = true
                } else if(p0.isNotEmpty()) {
                    cekEmailEmpty = false
                }
                if(view.et_input_email.text.isNotEmpty()) {
                    view.et_input_email.backgroundTintList =
                        ContextCompat.getColorStateList(view.context.applicationContext, R.color.colorBlue)
                } else if(view.et_input_email.text.isEmpty()) {
                    view.et_input_email.backgroundTintList =
                        ContextCompat.getColorStateList(view.context.applicationContext, R.color.colorWhiteC4)
                }
            }
        })

        view.et_nomor_handphone.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(p0: Editable?) {
                if(cekEmailEmpty && cekHandphoneEmpty) {
                    view.btn_lanjut_one.setBackgroundResource(R.drawable.custome_button_solid_gray)
                } else {
                    view.btn_lanjut_one.setBackgroundResource(R.drawable.custome_button_solid_blue)
                }

                PHONENUMBER = view.et_nomor_handphone.text.toString()

                Log.d("CEKING", "THIS IS THE VALUE OF PHONENUMBER: $PHONENUMBER")
            }

            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {

            }

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
                view.tv_watcher.setText(R.string.tv_watcher_form_one_nomor_handphone)
                if(p0!!.isEmpty()) {
                    cekHandphoneEmpty = true
                } else if(p0.isNotEmpty()) {
                    cekHandphoneEmpty = false
                }
                if(view.et_nomor_handphone.text.isNotEmpty()) {
                    view.et_nomor_handphone.backgroundTintList =
                        ContextCompat.getColorStateList(view.context.applicationContext, R.color.colorBlue)
                } else if(view.et_nomor_handphone.text.isEmpty()) {
                    view.et_nomor_handphone.backgroundTintList =
                        ContextCompat.getColorStateList(view.context.applicationContext, R.color.colorWhiteC4)
                }
            }
        })

        //handle click in form register one
        view.btn_lanjut_one.setOnClickListener {
            //here code to using registration API
            progressDialog.show()

            val body = PostAuthRegistrationBody (
                EMAIL,
                PHONENUMBER
            )

            ApiClient.apiService.registrationUser(body).enqueue(object :
                Callback<PostAuthRegistrationResponse> {
                override fun onFailure(call: Call<PostAuthRegistrationResponse>, t: Throwable) {
                    progressDialog.dismiss()
                    Toast.makeText(activity, "Error in Consume Api", Toast.LENGTH_LONG).show()
                }

                override fun onResponse(
                    call: Call<PostAuthRegistrationResponse>,
                    response: Response<PostAuthRegistrationResponse>
                ) {
                    val sharedPreferences = activity?.getSharedPreferences(SP_NAME, Context.MODE_PRIVATE)
                    val editor = sharedPreferences?.edit()

                    if(response.isSuccessful && response.body()?.status.equals("Success")) {
                        editor?.putString(ID, response.body()?.userId)
                        val commitedUserId = editor?.commit()

                        if(commitedUserId!!){
                            val fm = fragmentManager

                            val progressBarUpdate = activity?.findViewById<ProgressBar>(R.id.progress_bar)
                            if (progressBarUpdate != null) {
                                progressBarUpdate.progress = 32
                            }

                            progressDialog.dismiss()

                            fm!!.beginTransaction()
                                .replace(R.id.frame_layout, FormStepTwoFragment(), "FormTwo").commit()
                        } else {
                            progressDialog.dismiss()
                            Toast.makeText(activity, "Error Saving UserId to SP", Toast.LENGTH_LONG).show()
                        }
                    } else {
                        val errorBodyResponse: PostAuthRegistrationResponse = Gson().fromJson(
                            response.errorBody()!!.charStream(),
                            PostAuthRegistrationResponse::class.java
                        )
                        progressDialog.dismiss()
                        //Here code to check the error validate
                        if(errorBodyResponse.message == "Gagal, email telah digunakan") {
                            view.et_input_email.requestFocus()
                            view.et_input_email.error = errorBodyResponse.message
                        } else if(errorBodyResponse.message == "Gagal, nomor handphone telah digunakan") {
                            view.et_nomor_handphone.requestFocus()
                            view.et_nomor_handphone.error = errorBodyResponse.message
                        }

                    }
                }
            })
        }

        return  view
    }

    companion object {
        const val SP_NAME = "data_register"
        const val ID = "id"
        private lateinit var EMAIL: String
        private lateinit var PHONENUMBER: String

        @JvmStatic
        fun newInstance(param1: String, param2: String) =
            FormStepOneFragment().apply {
                arguments = Bundle().apply {
                }
            }
    }
}