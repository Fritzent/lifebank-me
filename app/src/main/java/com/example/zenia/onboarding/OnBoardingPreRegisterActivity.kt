package com.example.zenia.onboarding

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.example.zenia.authentication.login.LoginActivity
import com.example.zenia.authentication.register.RegisterActivity
import com.example.zenia.databinding.ActivityOnBoardingPreRegisterBinding

class OnBoardingPreRegisterActivity : AppCompatActivity() {

    private lateinit var binding : ActivityOnBoardingPreRegisterBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityOnBoardingPreRegisterBinding.inflate(layoutInflater)
        val view = binding.root
        setContentView(view)

        binding.iconBack.setOnClickListener{
            finish()
        }

        binding.btnDaftar.setOnClickListener {
            //TODO CHANGE LOGIN ACTIVITY INTO REGISTER ACTIVITY
            startActivity(Intent(applicationContext, RegisterActivity::class.java))
        }
    }
}