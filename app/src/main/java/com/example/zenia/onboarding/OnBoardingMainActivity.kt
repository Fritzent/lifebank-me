package com.example.zenia.onboarding

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.example.zenia.authentication.login.LoginActivity
import com.example.zenia.databinding.ActivityOnBoardingMainBinding

class OnBoardingMainActivity : AppCompatActivity() {

    private lateinit var binding : ActivityOnBoardingMainBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityOnBoardingMainBinding.inflate(layoutInflater)
        val view = binding.root
        setContentView(view)

        binding.btnDaftar.setOnClickListener {
            startActivity(Intent(applicationContext, OnBoardingPreRegisterActivity::class.java))
        }
        binding.btnLogin.setOnClickListener {
            startActivity(Intent(applicationContext, LoginActivity::class.java))

        }
    }
}