package com.example.zenia.onboarding

import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.TextView
import androidx.viewpager.widget.ViewPager
import com.example.zenia.R
import com.example.zenia.authentication.login.LoginActivity
import com.example.zenia.databinding.ActivityOnBoardingAppBinding
import com.google.android.material.tabs.TabLayout

class OnBoardingAppActivity : AppCompatActivity() {

    private lateinit var binding : ActivityOnBoardingAppBinding

    private var viewPagerAdapter: ViewPagerAdapterBoard? = null
    private var tabLayout: TabLayout? = null
    private var onBoardingViewPager: ViewPager? = null
    private var next: TextView? = null
    private var position = 0
    private var sharedPreferences: SharedPreferences? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityOnBoardingAppBinding.inflate(layoutInflater)
        val view = binding.root
        setContentView(view)

        if(restorePrefData()){
            val i = Intent(applicationContext, OnBoardingMainActivity::class.java)
            startActivity(i)
            finish()
        }

        tabLayout = binding.tbLayoutBoard
        next = binding.btnOnboard

        next!!.visibility = View.GONE

        //here the data on the onboarding

        val onBoardingData: MutableList<OnBoardingDataApp> = ArrayList()
        onBoardingData.add(
            OnBoardingDataApp("Easy Saving", "Perencanaan dimulai sejak muda untuk mewujudkan kebahagian di hari tua",
            R.drawable.walkone
        )
        )
        onBoardingData.add(
            OnBoardingDataApp("Easy Planing", "Perencanaan dimulai sejak muda untuk mewujudkan kebahagian di hari tua",
            R.drawable.walktwo
        )
        )
        onBoardingData.add(
            OnBoardingDataApp("Easy Payment", "Perencanaan dimulai sejak muda untuk mewujudkan kebahagian di hari tua",
            R.drawable.walkthree
        )
        )

        setOnBoardingViewPagerAdapter(onBoardingData)

        position = onBoardingViewPager!!.currentItem

        next?.setOnClickListener {
            setupSharedPref()
            // TODO CHANGE LOGIN ACTIVITY INTO ACTIVITY SCREEN
            val i = Intent(applicationContext, OnBoardingMainActivity::class.java)
            startActivity(i)

        }

        tabLayout!!.addOnTabSelectedListener(object : TabLayout.OnTabSelectedListener {
            override fun onTabReselected(tab: TabLayout.Tab?) {
            }

            override fun onTabUnselected(tab: TabLayout.Tab?) {
            }

            override fun onTabSelected(tab: TabLayout.Tab?) {

                position = tab!!.position
                if(tab.position == onBoardingData.size - 1){
                    next!!.visibility = View.VISIBLE
                }else{
                    next!!.visibility = View.GONE
                }
            }

        })
    }
    private fun setOnBoardingViewPagerAdapter(onBoardingData: List<OnBoardingDataApp>){
        onBoardingViewPager = findViewById(binding.vwOnboard.id)
        viewPagerAdapter = ViewPagerAdapterBoard(this,onBoardingData)
        onBoardingViewPager!!.adapter = viewPagerAdapter
        tabLayout?.setupWithViewPager(onBoardingViewPager)
    }

    private fun setupSharedPref() {
        sharedPreferences = applicationContext.getSharedPreferences("mode", Context.MODE_PRIVATE)
        val editor = sharedPreferences!!.edit()
        editor.putBoolean("FirstTimeRun", true)
        editor.apply()
    }
    private fun restorePrefData(): Boolean {
        sharedPreferences = applicationContext.getSharedPreferences("mode", Context.MODE_PRIVATE)
        return sharedPreferences!!.getBoolean("FirstTimeRun", false)
    }
}