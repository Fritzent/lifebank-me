package com.example.zenia.lainnya.view.ubahPassword

import android.graphics.Color
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import com.example.zenia.R
import com.example.zenia.databinding.ActivityUbahPasswordBinding
import com.example.zenia.lainnya.presenter.ubahPassword.UbahPasswordPresenter
import kotlinx.android.synthetic.main.toolbar.view.*

class UbahPasswordActivity : AppCompatActivity(), UbahPasswordPresenter.Listener {
    private lateinit var binding: ActivityUbahPasswordBinding
    private lateinit var presenter: UbahPasswordPresenter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityUbahPasswordBinding.inflate(layoutInflater)
        presenter = UbahPasswordPresenter(this)

        setContentView(binding.root)


        val appBar = binding.menuToolbar.appBarLayout.toolbar
        setSupportActionBar(appBar)
        supportActionBar?.title = "Ubah Password"
        supportActionBar?.setDisplayHomeAsUpEnabled(true)


        binding.infoPassword.visibility = View.GONE

        binding.etPasswordBaru.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {

            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {

            }

            override fun afterTextChanged(s: Editable?) {
                binding.etPasswordBaru.removeTextChangedListener(this)

                presenter.checkPass(s.toString())

                binding.etPasswordBaru.text?.length?.let { binding.etPasswordBaru.setSelection(it) }
                binding.etPasswordBaru.addTextChangedListener(this)

            }

        })
    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }

    override fun showAngkaPasswordTrue() {
        binding.ivAngkaPassword.setImageResource(R.drawable.ic_password_oke)
        binding.tvAngka.setTextColor(getColor(R.color.colorBlue))
    }

    override fun showAngkaPasswordFalse() {
        binding.ivAngkaPassword.setImageResource(R.drawable.ic_password_no)
        binding.tvAngka.setTextColor(getColor(R.color.colorRed))
    }

    override fun showHurufKecilTrue() {
        binding.ivHurufKecilPassword.setImageResource(R.drawable.ic_password_oke)
        binding.tvHurufKecil.setTextColor(getColor(R.color.colorBlue))
    }

    override fun showHurufKecilFalse() {
        binding.ivHurufKecilPassword.setImageResource(R.drawable.ic_password_no)
        binding.tvHurufKecil.setTextColor(getColor(R.color.colorRed))
    }

    override fun showHurufBesarTrue() {
        binding.ivHurufKapitalPassword.setImageResource(R.drawable.ic_password_oke)
        binding.tvHurufKapital.setTextColor(getColor(R.color.colorBlue))
    }

    override fun showHurufBesarFalse() {
        binding.ivHurufKapitalPassword.setImageResource(R.drawable.ic_password_no)
        binding.tvHurufKapital.setTextColor(getColor(R.color.colorRed))
    }

    override fun showMinCharTrue() {
        binding.ivMinCharPassword.setImageResource(R.drawable.ic_password_oke)
        binding.tvMinKaraket.setTextColor(getColor(R.color.colorBlue))
    }

    override fun showMinCharFalse() {
        binding.ivMinCharPassword.setImageResource(R.drawable.ic_password_no)
        binding.tvMinKaraket.setTextColor(getColor(R.color.colorRed))
    }

    override fun showPasswordChecker() {
       binding.infoPassword.visibility = View.VISIBLE
    }

    override fun dismissPasswordChecker() {
        binding.infoPassword.visibility = View.GONE
    }
}