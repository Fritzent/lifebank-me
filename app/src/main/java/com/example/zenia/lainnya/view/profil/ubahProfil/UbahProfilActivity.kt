package com.example.zenia.lainnya.view.profil.ubahProfil

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.example.zenia.databinding.ActivityUbahProfilBinding
import com.example.zenia.lainnya.presenter.profil.ubahProfil.UbahProfilPresenter
import kotlinx.android.synthetic.main.toolbar.view.*

class UbahProfilActivity : AppCompatActivity(), UbahProfilPresenter.Listener {
    private lateinit var binding: ActivityUbahProfilBinding
    private lateinit var presenter: UbahProfilPresenter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityUbahProfilBinding.inflate(layoutInflater)
        presenter = UbahProfilPresenter(this)

        setContentView(binding.root)

        val appBar = binding.menuToolbar.appBarLayout.toolbar
        setSupportActionBar(appBar)
        supportActionBar?.title = "Profil Saya"
        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        binding.btnUbah.setOnClickListener {
            presenter.goBack()
        }
    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }

    override fun finishActivity() {
        finish()
    }
}