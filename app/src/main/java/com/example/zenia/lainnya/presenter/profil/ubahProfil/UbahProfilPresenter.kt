package com.example.zenia.lainnya.presenter.profil.ubahProfil

class UbahProfilPresenter (val listener: Listener){
    interface Listener{
        fun finishActivity()
    }

    fun goBack(){
        listener.finishActivity()
    }
}