package com.example.zenia.lainnya.view.profil.lihatProfil

import android.content.Intent
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import androidx.appcompat.app.AppCompatActivity
import com.example.zenia.R
import com.example.zenia.databinding.ActivityProfilBinding
import com.example.zenia.lainnya.presenter.profil.lihatProfil.ProfilPresenter
import com.example.zenia.lainnya.view.profil.ubahProfil.UbahProfilActivity
import kotlinx.android.synthetic.main.toolbar.view.*


class ProfilActivity : AppCompatActivity(), ProfilPresenter.Listener {
    private lateinit var binding: ActivityProfilBinding
    private lateinit var presenter: ProfilPresenter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityProfilBinding.inflate(layoutInflater)


        setContentView(binding.root)

        presenter = ProfilPresenter(this)

        val appBar = binding.menuToolbar.appBarLayout.toolbar
        setSupportActionBar(appBar)
        supportActionBar?.title = "Profil Saya"
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        val inflater = menuInflater
        inflater.inflate(R.menu.edit_button_menu, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        //return super.onOptionsItemSelected(item)
        return when (item.itemId){
            R.id.edit_button ->{
                presenter.goUbahProfil()
                true
            }
            else ->return super.onOptionsItemSelected(item)
        }

    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }

    override fun goUbahProfilActivity() {
        startActivity(Intent(this, UbahProfilActivity::class.java))
    }


}