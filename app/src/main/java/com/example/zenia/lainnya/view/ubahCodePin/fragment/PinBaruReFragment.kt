package com.example.zenia.lainnya.view.ubahCodePin.fragment

import android.graphics.Color
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.example.zenia.R
import com.example.zenia.lainnya.view.ubahCodePin.UbahCodePinActivity
import kotlinx.android.synthetic.main.fragment_pin_baru_re.*
import kotlinx.android.synthetic.main.fragment_pin_baru_re.view.*

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 * Use the [PinBaruReFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class PinBaruReFragment : Fragment() {
    // TODO: Rename and change types of parameters
    private var param1: String? = null
    private var param2: String? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            param1 = it.getString(ARG_PARAM1)
            param2 = it.getString(ARG_PARAM2)
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_pin_baru_re, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val pin = arguments?.getString(ARG_PIN)

        Log.d("Pin", "$pin")

        tv_error.visibility = View.GONE

        disableButton()

        view.pin_input.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {

            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                if (s != null) {
                    if (s.length == 6) {
                        if (view.pin_input.text.toString() == pin) {
//                            Toast.makeText(view.context, "Ini Isi Pin ${view.pin_input.text.toString()}", Toast.LENGTH_LONG).show()
                            //Here Code to handle to other fragment
                            (activity as UbahCodePinActivity).updateProgressBar(100)


                            Log.d("Pin","Matcher")
//                            (activity as UbahCodePinActivity).goFragmentPinBaru()
                            enableButton()
                        } else {
                            tv_error.visibility = View.VISIBLE
                            view.pin_input.setLineColor(Color.parseColor("#EE0D0D"))
                            view.pin_input.cursorColor = Color.parseColor("#EE0D0D")
                            view.pin_input.requestFocus()
                            disableButton()
                        }
                    } else {
                        tv_error.visibility = View.GONE
                        view.pin_input.setLineColor(Color.parseColor("#3BA1FF"))
                        view.pin_input.cursorColor = Color.parseColor("#3BA1FF")
                        disableButton()
                    }
                }
            }

            override fun afterTextChanged(s: Editable?) {

            }

        })
    }

    fun disableButton() {
        btnUbahPin.isClickable = false
        btnUbahPin.setBackgroundResource(R.drawable.disabled_button)
    }

    fun enableButton() {
        btnUbahPin.isClickable = true
        btnUbahPin.setBackgroundResource(R.drawable.primary_button)
    }

    companion object {

        const val ARG_PIN = "pin"

        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         *
//         * @param param1 Parameter 1.
//         * @param param2 Parameter 2.
         * @return A new instance of fragment PinBaruReFragment.
         */
        // TODO: Rename and change types and number of parameters
        @JvmStatic
        fun newInstance(pin: String) =
            PinBaruReFragment().apply {
                arguments = Bundle().apply {
                    putString(ARG_PIN, pin)
                }
            }
//        fun newInstance(param1: String, param2: String) =
//            PinBaruReFragment().apply {
//                arguments = Bundle().apply {
//                    putString(ARG_PARAM1, param1)
//                    putString(ARG_PARAM2, param2)
//                }
//            }
    }
}