package com.example.zenia.lainnya.view.ubahCodePin.fragment

import android.graphics.Color
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ProgressBar
import com.example.zenia.R
import com.example.zenia.authentication.register.fragment.FormStepThreeFragment
import com.example.zenia.home.activity.HomeActivity
import com.example.zenia.lainnya.view.ubahCodePin.UbahCodePinActivity
import kotlinx.android.synthetic.main.fragment_form_step_two.view.*

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 * Use the [PinSaatIniFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class PinSaatIniFragment : Fragment() {
    // TODO: Rename and change types of parameters
    private var param1: String? = null
    private var param2: String? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            param1 = it.getString(ARG_PARAM1)
            param2 = it.getString(ARG_PARAM2)
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_pin_saat_ini, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val pin = "123456"

        //this is to handle the soft input keyboard
        if (view.pin_input.isFocusable) {
            view.pin_input.showSoftInputOnFocus
        }

        view.tv_error.visibility = View.GONE

        view.pin_input.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {

            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                if (s != null) {
                    if (s.length == 6) {
                        if (view.pin_input.text.toString() == pin) {
//                            Toast.makeText(view.context, "Ini Isi Pin ${view.pin_input.text.toString()}", Toast.LENGTH_LONG).show()
                            //Here Code to handle to other fragment
                            (activity as UbahCodePinActivity).updateProgressBar(50)

                            (activity as UbahCodePinActivity).goFragmentPinBaru()
                        } else {
                            view.tv_error.visibility = View.VISIBLE
                            view.pin_input.setLineColor(Color.parseColor("#EE0D0D"))
                            view.pin_input.cursorColor = Color.parseColor("#EE0D0D")
                            view.pin_input.requestFocus()
                        }
                    } else {
                        view.tv_error.visibility = View.GONE
                        view.pin_input.setLineColor(Color.parseColor("#3BA1FF"))
                        view.pin_input.cursorColor = Color.parseColor("#3BA1FF")
                    }
                }
            }

            override fun afterTextChanged(s: Editable?) {
//                if (view.pin_input.text.toString() == pin){
////                            Toast.makeText(view.context, "Ini Isi Pin ${view.pin_input.text.toString()}", Toast.LENGTH_LONG).show()
//                    //Here Code to handle to other fragment
//
//                    (activity as UbahCodePinActivity).updateProgressBar(32)
//
//                    (activity as UbahCodePinActivity).goFragmentPinBaru()
//
//
//
//                } else {
//
//                }
            }

        }
        )
    }

    companion object {
        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         *
         * @param param1 Parameter 1.
         * @param param2 Parameter 2.
         * @return A new instance of fragment PinSaatIniFragment.
         */
        // TODO: Rename and change types and number of parameters
        @JvmStatic
        fun newInstance(param1: String, param2: String) =
            PinSaatIniFragment().apply {
                arguments = Bundle().apply {
                    putString(ARG_PARAM1, param1)
                    putString(ARG_PARAM2, param2)
                }
            }
    }
}