package com.example.zenia.lainnya.presenter.ubahPassword

class UbahPasswordPresenter(val listener:Listener){
    interface Listener{
        fun showAngkaPasswordTrue()
        fun showAngkaPasswordFalse()
        fun showHurufKecilTrue()
        fun showHurufKecilFalse()
        fun showHurufBesarTrue()
        fun showHurufBesarFalse()
        fun showMinCharTrue()
        fun showMinCharFalse()

        fun showPasswordChecker()
        fun dismissPasswordChecker()
    }

    private var stateCheckHurufKecil = false
    private var stateCheckAngka = false
    private var stateCheckHurufKapital = false
    private var stateCheckMinChar = false

    fun checkPass(et: String) {
        stateCheckAngka = if (et.matches((".*\\d.*").toRegex())) {
            listener.showAngkaPasswordTrue()
            true
        } else {
            listener.showAngkaPasswordFalse()
            false
        }

        stateCheckHurufKecil = if (et.matches((".*[a-z].*").toRegex())) {
            listener.showHurufKecilTrue()
            true
        } else {
            listener.showHurufKecilFalse()
            false
        }

        stateCheckHurufKapital = if (et.matches((".*[A-Z].*").toRegex())) {
            listener.showHurufBesarTrue()
            true
        } else {
            listener.showHurufBesarFalse()
            false
        }

        stateCheckMinChar = if (et.length >= 8) {
            listener.showMinCharTrue()
            true
        } else {
            listener.showMinCharFalse()
            false
        }

        checkState()

    }

    private fun checkState(){
        if (stateCheckAngka && stateCheckHurufKapital && stateCheckHurufKecil && stateCheckMinChar){
            listener.dismissPasswordChecker()
        } else {
            listener.showPasswordChecker()
        }
    }
}