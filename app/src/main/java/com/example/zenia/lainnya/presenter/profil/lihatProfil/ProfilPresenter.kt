package com.example.zenia.lainnya.presenter.profil.lihatProfil

class ProfilPresenter (val listener: Listener){
    interface Listener{
        fun goUbahProfilActivity()
    }

    fun goUbahProfil(){
        listener.goUbahProfilActivity()
    }
}