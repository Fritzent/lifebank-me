package com.example.zenia.lainnya.view.ubahCodePin

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.fragment.app.FragmentTransaction
import com.example.zenia.R
import com.example.zenia.databinding.ActivityUbahCodePinBinding
import com.example.zenia.lainnya.presenter.ubahPin.UbahCodePinPresenter
import com.example.zenia.lainnya.view.ubahCodePin.fragment.PinBaruFragment
import com.example.zenia.lainnya.view.ubahCodePin.fragment.PinBaruReFragment
import com.example.zenia.lainnya.view.ubahCodePin.fragment.PinSaatIniFragment
import kotlinx.android.synthetic.main.toolbar.view.*

class UbahCodePinActivity : AppCompatActivity(), UbahCodePinPresenter.Listener {
    private lateinit var binding: ActivityUbahCodePinBinding
    private lateinit var presenter: UbahCodePinPresenter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityUbahCodePinBinding.inflate(layoutInflater)
        presenter = UbahCodePinPresenter(this)
        setContentView(binding.root)

        val appBar = binding.menuToolbar.appBarLayout.toolbar
        setSupportActionBar(appBar)
        supportActionBar?.title = "Ubah PIN"
        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        //CHANGE THE VALUE OF THE PROGRESS TO HANDLE THE STATUS PROGRESSBAR
        binding.progressBarUbahPin.progress = 25

        //default fragment
        supportFragmentManager
            .beginTransaction()
            .replace(R.id.containerFragmentPin, PinSaatIniFragment())
            .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
            .commit()
    }

    fun updateProgressBar(progress: Int){
        binding.progressBarUbahPin.progress = progress
    }

    fun goFragmentPinBaru(){
        supportFragmentManager
            .beginTransaction()
            .replace(R.id.containerFragmentPin, PinBaruFragment())
            .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
            .commit()
    }

    fun goFragmentPinBaruRe(pin: String){
        supportFragmentManager
            .beginTransaction()
            .replace(R.id.containerFragmentPin, PinBaruReFragment.newInstance(pin))
            .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
            .commit()
    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }
}