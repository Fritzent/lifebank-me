package com.example.zenia.transfer.sesama

import android.os.Bundle

class ResiSesamaPresenter (val listener: Listener) {

    private lateinit var deskripsi : String
    private lateinit var norekTujuan : String
    private lateinit var namaRek : String
    lateinit var nominal : String
    lateinit var nominalSaja : String
    private lateinit var namaPengirim : String
    private lateinit var noPengirim : String

    interface Listener{
        fun showHasil (
            nominal: String,
            deskripsi: String,
            norekTujuan: String,
            namaRek: String,
            namaPengirim : String,
            noPengirim : String
        )
    }

    fun setData (bundle: Bundle){
        nominal = bundle.getString("nominal").toString()
        deskripsi = bundle.getString("deskripsi").toString()
        norekTujuan = bundle.getString("no rek tujuan").toString()
        namaRek = bundle.getString("nama rek").toString()
        namaPengirim = bundle.getString("nama pengirim").toString()
        noPengirim = bundle.getString("nomor pengirim").toString()
    }

    fun showHasil(){
        listener.showHasil(
            nominal,
            deskripsi,
            norekTujuan,
            namaRek,
            namaPengirim,
            noPengirim
        )
    }

}