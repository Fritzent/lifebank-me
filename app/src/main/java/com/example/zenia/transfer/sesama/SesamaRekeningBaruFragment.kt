package com.example.zenia.transfer.sesama

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import com.example.zenia.R
import com.example.zenia.authentication.login.LoginActivity
import com.example.zenia.network.ApiClient
import com.example.zenia.pojo.GetNomorRekeningResponse
import com.example.zenia.pojo.PostTransferGetUserBody
import com.example.zenia.pojo.PostTransferGetUserResponse
import com.google.android.material.bottomsheet.BottomSheetDialog
import kotlinx.android.synthetic.main.fragment_sesama_rekening_baru.*
import kotlinx.android.synthetic.main.fragment_sesama_rekening_baru.view.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import kotlin.properties.Delegates

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 * Use the [SesamaRekeningBaruFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class SesamaRekeningBaruFragment : Fragment() {
    // TODO: Rename and change types of parameters
    private var param1: String? = null
    private var param2: String? = null

    private var nomorRekening: String? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            param1 = it.getString(ARG_PARAM1)
            param2 = it.getString(ARG_PARAM2)
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val view = inflater.inflate(R.layout.fragment_sesama_rekening_baru, container, false)
        // here code for other costume

        view.tvAlertNoRekening.visibility = View.INVISIBLE

        val sharedPreferences =
            activity?.getSharedPreferences(LoginActivity.SP_NAME, Context.MODE_PRIVATE)
        val token = sharedPreferences?.getString("token", "No Data")
        token?.let { Log.d("Token On Transfer", it) }


        view.etSesamaNoRekBaru.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {

            }

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
//                val nomor_rekening = view.etSesamaNoRekBaru.text.toString()
//                NOMOR_REKENING = nomor_rekening
            }

            override fun afterTextChanged(p0: Editable?) {
                nomorRekening = etSesamaNoRekBaru.text.toString()
            }
        })


        view.btnLanjutRekBaru.setOnClickListener {
            val rekeningNUmber = nomorRekening?.let { it1 -> PostTransferGetUserBody(it1) }

            if (rekeningNUmber != null) {
                ApiClient.apiService.postRekeningNumber(
                    rekeningNUmber,
                    authHeader = "Bearer $token"
                ).enqueue(
                    object : Callback<PostTransferGetUserResponse> {
                        override fun onResponse(
                            call: Call<PostTransferGetUserResponse>,
                            response: Response<PostTransferGetUserResponse>
                        ) {
                            if (response.isSuccessful && response.body()?.status == "Success") {
                                val data: PostTransferGetUserResponse.Data? = response.body()?.data

                                //bottom seet activity
                                val bottomSheetDialog =
                                    BottomSheetDialog(view.context, R.style.BottomSheetDialogTheme)

                                val bottomSheetView =
                                    LayoutInflater.from(view.context.applicationContext).inflate(
                                        R.layout.layout_bottom_sheet_button_sesama_rektujuan_lanjut,
                                        view.findViewById(R.id.button_sheet_setor)
                                    )

                                //TODO Contoh Kalau Di Bottom Sheet nya Ada button atau action
                                bottomSheetView.findViewById<ImageView>(R.id.buttonExitBotSheetSesama)
                                    .setOnClickListener {
                                        bottomSheetDialog.dismiss()
                                    }

                                bottomSheetView.findViewById<TextView>(R.id.tvIsiNamaRekBotSheetSesama).text =
                                    data?.name
                                bottomSheetView.findViewById<TextView>(R.id.tvIsiNoRekBotSheetSesama).text =
                                    data?.rekeningNumber

                                bottomSheetView.findViewById<Button>(R.id.buttonLanjut)
                                    .setOnClickListener {
                                        //TODO intent
                                        val intent =
                                            Intent(activity, TransferSesamaActivity::class.java)
                                        val bundle = Bundle()
                                        bundle.putString(
                                            "nomor rekening",
                                            data?.rekeningNumber
                                        )
                                        bundle.putString("nama", data?.name)
                                        data?.saved?.let { it1 -> bundle.putBoolean("saved", it1) }
                                        intent.putExtras(bundle)
                                        startActivityForResult(intent, 1)
                                        activity?.finish()
                                    }
                                bottomSheetDialog.setContentView(bottomSheetView)
                                bottomSheetDialog.show()
                            } else {
                                Toast.makeText(
                                    context,
                                    "Terjadi Kesalahan Pada Api Service",
                                    Toast.LENGTH_SHORT
                                ).show()
                            }
                        }

                        override fun onFailure(
                            call: Call<PostTransferGetUserResponse>,
                            t: Throwable
                        ) {
                            Toast.makeText(
                                context,
                                "Terjadi Kesalahan Pada Api Service : $t",
                                Toast.LENGTH_SHORT
                            ).show()

                            Log.d("Transfer Failure", t.toString())
                        }

                    })
            }

//            ApiClient.apiService.getNoRekening(authHeader = "Bearer $token").enqueue(object :
//                Callback<GetNomorRekeningResponse> {
//                override fun onFailure(call: Call<GetNomorRekeningResponse>, t: Throwable) {
//                    Toast.makeText (context, "Terjadi Kesalahan Pada Api Service", Toast.LENGTH_SHORT).show()
//                }
//
//                override fun onResponse(
//                    call: Call<GetNomorRekeningResponse>,
//                    response: Response <GetNomorRekeningResponse>
//                ) {
//                    if(response.isSuccessful && response.body()?.status == "success") {
//                        //TODO here code to call the bottom sheet activity
//                        val bottomSheetDialog = BottomSheetDialog(view.context, R.style.BottomSheetDialogTheme)
//
//                        val bottomSheetView = LayoutInflater.from(view.context.applicationContext).inflate(
//                            R.layout.layout_bottom_sheet_button_sesama_rektujuan_lanjut,
//                            view.findViewById(R.id.button_sheet_setor)
//                        )
//
//                        //TODO Contoh Kalau Di Bottom Sheet nya Ada button atau action
//                        bottomSheetView.findViewById<ImageView>(R.id.buttonExitBotSheetSesama).setOnClickListener {
//                            bottomSheetDialog.dismiss()
//                        }
//                        bottomSheetView.findViewById<TextView>(R.id.tvIsiNoRekBotSheetSesama).text = NOMOR_REKENING.toString()
//                        bottomSheetView.findViewById<Button>(R.id.buttonLanjut).setOnClickListener {
//                            //TODO intent
//                            val intent = Intent(activity, TransferSesamaActivity::class.java)
//                            val bundle = Bundle()
//                            bundle.putString("REKENING", NOMOR_REKENING.toString())
//                            intent.putExtras(bundle)
//                            startActivityForResult(intent, 1)
//                            activity?.finish()
//                        }
//                        bottomSheetDialog.setContentView(bottomSheetView)
//                        bottomSheetDialog.show()
//                    }else {
//                        Toast.makeText (context, "Terjadi Kesalahan Pada Api Service", Toast.LENGTH_SHORT).show()
//                    }
//                }
//            })
        }
        return view
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
//        super.onActivityResult(requestCode, resultCode, data)

    }

    companion object {
//        private var NOMOR_REKENING: String? = null

        // TODO: Rename and change types and number of parameters
        @JvmStatic
        fun newInstance(param1: String, param2: String) =
            SesamaRekeningBaruFragment().apply {
                arguments = Bundle().apply {
                    putString(ARG_PARAM1, param1)
                    putString(ARG_PARAM2, param2)
                }
            }
    }
}