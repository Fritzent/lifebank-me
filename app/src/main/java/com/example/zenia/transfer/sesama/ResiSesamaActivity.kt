package com.example.zenia.transfer.sesama

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.example.zenia.R
import kotlinx.android.synthetic.main.activity_konfirmasi_transfer_sesama.*
import kotlinx.android.synthetic.main.activity_resi_sesama.*

class ResiSesamaActivity : AppCompatActivity(),
    ResiSesamaPresenter.Listener {
    private lateinit var presenter: ResiSesamaPresenter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_resi_sesama)

        try {
            this.supportActionBar?.hide()
        } catch (e: NullPointerException) {}

        presenter = ResiSesamaPresenter(this)

        try {
            this.supportActionBar?.hide()
        } catch (e: NullPointerException) { }

        val bundle = intent.extras
        if (bundle != null) {
            presenter.setData(bundle)
        }

        presenter.showHasil()


        ivBackResiSesama.setOnClickListener {
            finish()
        }
    }

    override fun showHasil(
        nominal: String,
        deskripsi: String,
        norekTujuan: String,
        namaRek: String,
        namaPengirim : String,
        noPengirim : String
    ) {
        tvDeskripsiResi.text = deskripsi
        tvJumlahTransferResi.text = nominal
        tvRekTujuanResi.text = "$namaRek - $norekTujuan"
        tvRekPengirimResi.text = "$namaPengirim - $noPengirim"
    }
}