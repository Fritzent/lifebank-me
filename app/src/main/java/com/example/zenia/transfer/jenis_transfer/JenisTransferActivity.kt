package com.example.zenia.transfer.jenis_transfer

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.example.zenia.R
import com.example.zenia.transfer.sesama.TransferSesamaActivity
import kotlinx.android.synthetic.main.activity_jenis_transfer.*

class JenisTransferActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_jenis_transfer)

        try {
            this.supportActionBar?.hide()
        } catch (e: NullPointerException) {}


        buttonSesamaRekening.setOnClickListener {
            val intent = Intent (this, TransferSesamaActivity::class.java)
            startActivity(intent)
        }

        ivbackJenisTransfer.setOnClickListener {
            finish()
        }
    }
}