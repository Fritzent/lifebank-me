package com.example.zenia.transfer.sesama

import android.os.Bundle
import android.util.Log
import java.text.DecimalFormat
import java.text.NumberFormat
import java.util.*
import kotlin.properties.Delegates

class TransferSesamaPresenter (val listener : Listener) {
    private var namaPengirim : String = ""
    private var noPengirim : String = ""
    private var namaRek: String = ""
    private var nominal: Long = 0
    private var deskripsi: String = ""

    private var norek: String = ""

    private var saldo: Long = 10000000

    private var stateNorek: Boolean = true
    private var stateNominal :Boolean = false

    private var stateCheckBox : Boolean = false

    interface Listener{
        fun goToHasil(
            norek : String,
            nominal: Long,
            deskripsi: String,
            namaRek: String,
            namaPengirim : String,
            noPengirim : String
        )

        fun showHasil(
            norek: String,
            nama: String,
            saved: Boolean
        )

        fun showWarningNominal(msg:String)
        fun disableButton()
        fun enableButton()
        fun enableNickName()
        fun disableNickName()
    }

    fun setNomor (bundle: Bundle){
        Log.d("checkBundle" , "ini $bundle")
        norek = bundle.getString("nomor rekening").toString()
        namaRek = bundle.getString("nama").toString()
        stateCheckBox = bundle.getBoolean("saved")

        buttonCheck()

    }

    fun showHasil(){
        listener.showHasil(
            norek, namaRek, stateCheckBox
        )
    }

    fun goToHasilActivity() {
        listener.goToHasil(
            norek,
            nominal,
            deskripsi,
            namaRek,
            namaPengirim,
            noPengirim
        )
    }

    fun setNominal(input: String) : String {
        var textInput = input

        if (textInput.contains(".")) {
            textInput = textInput.replace("[.]".toRegex(), "")
        }

        if (textInput.contains("Rp ")) {
            textInput = textInput.replace("Rp ".toRegex(), "")
        }

        nominal = if (textInput == "") {
            0
        } else {
            textInput.toLong()
        }

        stateNominal = when {
            nominal <= 10000 -> {
                listener.showWarningNominal("Minimal nominal Rp 10.000")
                false
            }
            nominal > saldo -> {
                listener.showWarningNominal("Saldo tidak cukup")
                false
            }
            else -> {
//                listener.showWarningNominal("")
                true
            }
        }

        val formatter =
            NumberFormat.getCurrencyInstance(Locale("in", "ID")) as DecimalFormat

        var formattedString = formatter.format(nominal).toString()

        if (formattedString.contains("Rp")) {
            formattedString = formattedString.replace("Rp".toRegex(), "Rp ")
        }

        if (formattedString.contains(",00")) {
            formattedString = formattedString.replace(",00".toRegex(), "")
        }

        buttonState()

        return formattedString

    }

    fun showSaldoFormatter(): String {
        val formatter =
            NumberFormat.getCurrencyInstance(Locale("in", "ID")) as DecimalFormat

        var formattedString = formatter.format(saldo).toString()

        if (formattedString.contains("Rp")) {
            formattedString = formattedString.replace("Rp".toRegex(), "Rp ")
        }

        if (formattedString.contains(",00")) {
            formattedString = formattedString.replace(",00".toRegex(), "")
        }

        return formattedString
    }

    fun setNorek(textInput: String) {
        if (textInput == "") {
            norek = ""
            stateNorek = false

        } else {
            norek = textInput
            stateNorek = true
        }


        buttonState()
    }

    fun setCheckbox (input: Boolean) {
        stateCheckBox = if (input == false) {
            false
        } else {
            true
        }

        buttonCheck()
    }

    fun setDeskipsi(textInput: String) {
        deskripsi = if (textInput == "") {
            ""
        } else {
            textInput
        }
    }

    fun buttonState(){
        if (stateNominal && stateNorek){
            listener.enableButton()
        } else {
            listener.disableButton()
        }
    }

    fun buttonCheck(){
        if (stateCheckBox){
            listener.enableNickName()
        } else {
            listener.disableNickName()
        }
    }

}