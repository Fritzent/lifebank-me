package com.example.zenia.transfer.sesama

import android.content.Context
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import com.example.zenia.R
import com.example.zenia.authentication.login.LoginActivity
import kotlinx.android.synthetic.main.activity_nomor_rekening_tujuan.*

class NomorRekeningTujuanActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_nomor_rekening_tujuan)

        try {
            this.supportActionBar?.hide()
        } catch (e: NullPointerException) { }

//        val sharedPreferences = getSharedPreferences(LoginActivity.SP_NAME, Context.MODE_PRIVATE)
//        val token = sharedPreferences.getString("token", "No Data")
//        token?.let { Log.d("Token Home", it) }

        viewPagerNorekTujuan.adapter = AdapterSesama (supportFragmentManager)
        tabLayoutNorekTujuan.setupWithViewPager(viewPagerNorekTujuan)

        ivBackNorekTujuan.setOnClickListener {
            finish()
        }
    }
}