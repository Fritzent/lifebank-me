package com.example.zenia.transfer.sesama

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter

class AdapterSesama(fm: FragmentManager): FragmentPagerAdapter(fm){
    private val pages = listOf(
        SesamaRekeningBaruFragment(),SesamaRekeningTersimpanFragment()
    )

    override fun getItem(position: Int): Fragment {
        return pages[position]
    }

    override fun getCount(): Int {
        return pages.size
    }

    override fun getPageTitle(position: Int): CharSequence? {
        return when (position) {
            0 -> "Rekening Baru"
            else -> "Rekening Tersimpan"
        }
    }
}