package com.example.zenia.transfer.sesama

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.example.zenia.R
import kotlinx.android.synthetic.main.activity_konfirmasi_transfer_sesama.*
import kotlinx.android.synthetic.main.activity_transfer_sesama.*
import kotlinx.android.synthetic.main.activity_transfer_sesama.btnTransferSesama

class KonfirmasiTransferSesamaActivity : AppCompatActivity(),
    KonfirmasiTransferSesamaPresenter.Listener {
    private lateinit var presenter: KonfirmasiTransferSesamaPresenter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_konfirmasi_transfer_sesama)

        try {
            this.supportActionBar?.hide()
        } catch (e: NullPointerException) {}

        presenter = KonfirmasiTransferSesamaPresenter(this)

        val bundle = intent.extras
        if (bundle != null) {
            presenter.setData(bundle)
        }

        presenter.showHasil()

        btnBayar.setOnClickListener {
            presenter.goToResiActivity()}

        ivBackKonfirmTransfer.setOnClickListener {
            finish()
        }

    }

    override fun showHasil(
        nominal: String,
        deskripsi: String,
        norekTujuan: String,
        jumlahTotal: String,
        namaRek : String
    ) {
//        tvDeskripsiIsi.text = "$deskripsi dan $nominal"
        tvDeskripsiIsi.text = deskripsi
        tvNominalIsi.text = nominal
        tvNoRekTujuanRekIsi.text = norekTujuan
        tvJumlahTotalIsi.text = jumlahTotal
        tvNamaRekTujuanIsi.text = namaRek
    }

    override fun goToActivity(
        nominal: String,
        deskripsi: String,
        norekTujuan: String,
        namaRek: String,
        namaPengirim : String,
        noPengirim : String
    ) {
        val intent = Intent(this, ResiSesamaActivity::class.java)
        val bundle = Bundle()

        bundle.putString("no rek tujuan", norekTujuan)
        bundle.putString("nominal", nominal)
        bundle.putString("deskripsi", deskripsi)
        bundle.putString("nama rek", namaRek)
        bundle.putString("nama pengirim", namaPengirim)
        bundle.putString("nomor pengirim", noPengirim)


        intent.putExtras(bundle)
        startActivity(intent)
    }




}