package com.example.zenia.transfer.sesama

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.View
import com.example.zenia.R
import com.example.zenia.databinding.ActivityTransferSesamaBinding
import kotlinx.android.synthetic.main.activity_jenis_transfer.*
import kotlinx.android.synthetic.main.activity_konfirmasi_transfer_sesama.*
import kotlinx.android.synthetic.main.activity_transfer_sesama.*

class TransferSesamaActivity : AppCompatActivity(), TransferSesamaPresenter.Listener {
    lateinit var presenter: TransferSesamaPresenter
    lateinit var binding: ActivityTransferSesamaBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityTransferSesamaBinding.inflate(layoutInflater)
        setContentView(binding.root)

        presenter = TransferSesamaPresenter(this)


        val bundle = intent.extras
        if (bundle != null) {
            presenter.setNomor(bundle)
        }

        presenter.showHasil()
        presenter.buttonState()

//        val bundle = Bundle()
//        var norek = bundle.getString("REKENING")
//
//        presenter = norek?.let { TransferSesamaPresenter(this, it) }!!


        layoutNamaSingkat.visibility = View.GONE

        ivbackSesamaPage.setOnClickListener {
            finish()
        }


        binding.etNominal.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {

            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {

            }

            override fun afterTextChanged(s: Editable?) {
                etNominal.removeTextChangedListener(this)

                etNominal.setText(presenter.setNominal(s.toString()))

                etNominal.setSelection(etNominal.text.length)
                etNominal.addTextChangedListener(this)


            }

        })

        binding.tvJumlahSaldo.setText(presenter.showSaldoFormatter())

        binding.etDeskripsiOpsi.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {

            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {

            }

            override fun afterTextChanged(s: Editable?) {
                etDeskripsiOpsi.removeTextChangedListener(this)

                presenter.setDeskipsi(s.toString())

                etDeskripsiOpsi.setSelection(etDeskripsiOpsi.text.length)
                etDeskripsiOpsi.addTextChangedListener(this)
            }

        })


        binding.checkboxSesama.setOnClickListener {
            if (binding.checkboxSesama.isChecked) {
                layoutNamaSingkat.visibility = View.VISIBLE
            }
            else {
                layoutNamaSingkat.visibility = View.GONE
            }
        }


        binding.etNoRekTujuan.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {

            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {

            }

            override fun afterTextChanged(s: Editable?) {
                etNoRekTujuan.removeTextChangedListener(this)

                presenter.setNorek(s.toString())

                etNoRekTujuan.setSelection(etNoRekTujuan.text.length)
                etNoRekTujuan.addTextChangedListener(this)
            }

        })


        try {
            this.supportActionBar?.hide()
        } catch (e: NullPointerException) {}

        etNoRekTujuan.setOnClickListener {
            val intent = Intent (this, NomorRekeningTujuanActivity::class.java)
            startActivity(intent)
        }
//
//        btnTransferSesama.setOnClickListener {
//            startActivity(Intent (this, KonfirmasiTransferSesamaActivity::class.java))
//        }

    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }

    override fun goToHasil(
        norek: String,
//        namaRekening: String,
        nominal: Long,
        deskripsi: String,
        namaRek: String,
        namaPengirim : String,
        noPengirim : String

    ) {
        val intent = Intent(this, KonfirmasiTransferSesamaActivity::class.java)
        val bundle = Bundle()

        bundle.putString("no rek tujuan", norek)
        bundle.putLong("nominal", nominal)
        bundle.putString("deskripsi", deskripsi)
        bundle.putString("nama rek", namaRek)
        bundle.putString("nama pengirim", namaPengirim)
        bundle.putString("nomor pengirim", noPengirim)

        intent.putExtras(bundle)
        startActivity(intent)

    }

    override fun showHasil(norek: String, nama: String, saved: Boolean) {
        Log.d("check" , "ini $norek, $nama, is Saved : $saved")
        etNoRekTujuan.setText("$nama - $norek")

        if (saved){
            binding.layoutCheckbox.visibility = View.GONE
        } else {
            binding.layoutCheckbox.visibility = View.VISIBLE
        }
    }


    override fun showWarningNominal(msg: String) {
        binding.etNominal.error = msg
    }

    override fun disableButton() {
        binding.btnTransferSesama.isClickable = false
        binding.btnTransferSesama.setBackgroundResource(R.drawable.disabled_button)
    }

    override fun enableButton() {
        binding.btnTransferSesama.isClickable = true
        binding.btnTransferSesama.setBackgroundResource(R.drawable.primary_button)
        binding.btnTransferSesama.setOnClickListener {
            presenter.goToHasilActivity()
        }
    }

    override fun enableNickName() {
        checkboxSesama.setOnClickListener{
            binding.etNamaSingkat.visibility = View.VISIBLE
        }
    }

    override fun disableNickName() {
        checkboxSesama.setOnClickListener{
            binding.etNamaSingkat.visibility = View.GONE
        }
    }

}