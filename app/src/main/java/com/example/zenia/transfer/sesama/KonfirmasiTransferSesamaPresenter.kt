package com.example.zenia.transfer.sesama

import android.os.Bundle
import java.text.DecimalFormat
import java.text.NumberFormat
import java.util.*
import kotlin.properties.Delegates


class KonfirmasiTransferSesamaPresenter (val listener: Listener) {

    var nominal by Delegates.notNull<Long>()
    private lateinit var deskripsi : String
    private lateinit var norekTujuan : String
    private lateinit var namaRek : String
    private lateinit var nominalSaja: String
    private lateinit var namaPengirim : String
    private lateinit var noPengirim : String
    private lateinit var pin : String

    interface Listener{
        fun showHasil(
            nominal: String,
            deskripsi: String,
            norekTujuan: String,
            jumlahTotal: String,
            namaRek: String
//            namaPengirim : String,
//            noPengirim : String
        )

        fun goToActivity (
            nominal: String,
            deskripsi: String,
            norekTujuan: String,
            namaRek: String,
            namaPengirim : String,
            noPengirim : String
        )

    }

    fun goToResiActivity() {
        listener.goToActivity(
            formatUang(nominal),
            deskripsi,
            norekTujuan,
            namaRek,
            namaPengirim,
            noPengirim)

//            val objectPut = PostTransferBody (
//                namaRek,
//                namaPengirim,
//                norekTujuan,
//                noPengirim,
//                nominal,
//                deskripsi,
//                pin
//        )

//        ApiClient.apiService.postTransfer(objectPut, KonfirmasiTransferSesamaPresenter.USER_ID).enqueue(object :
//            Callback<PostTransferResponse> {
//            override fun onFailure(call: Call<PostTransferResponse>, t: Throwable) {
//                Toast.makeText(this, "Error Consume API", Toast.LENGTH_LONG).show()
//            }
//
//            override fun onResponse(
//                call: Call<PostTransferResponse>,
//                response: Response<PostTransferResponse>
//            ) {
//                Toast.makeText(this, "Sukses Consume API", Toast.LENGTH_LONG).show()
//            }
//        })

    }



    fun setData (bundle: Bundle){
        nominal = bundle.getLong("nominal")
        deskripsi = bundle.getString("deskripsi").toString()
        norekTujuan = bundle.getString("no rek tujuan").toString()
        namaRek = bundle.getString("nama rek").toString()
        namaPengirim = bundle.getString("nama pengirim").toString()
        noPengirim = bundle.getString("nomor pengirim").toString()
    }


    fun showHasil(){
        listener.showHasil(
            formatUang(nominal),
            deskripsi,
            norekTujuan,
            formatUang(totalJumlah()),
            namaRek
//            namaPengirim,
//            noPengirim
        )
    }

    fun totalJumlah(): Long {
        return nominal + 2500
    }

    private fun formatUang(input: Long): String {

        val formatter =
            NumberFormat.getCurrencyInstance(Locale("in", "ID")) as DecimalFormat

        var formattedString = formatter.format(input).toString()

        if (formattedString.contains("Rp")) {
            formattedString = formattedString.replace("Rp".toRegex(), "Rp ")
        }

        if (formattedString.contains(",00")) {
            formattedString = formattedString.replace(",00".toRegex(), "")
        }

        return formattedString
    }

}