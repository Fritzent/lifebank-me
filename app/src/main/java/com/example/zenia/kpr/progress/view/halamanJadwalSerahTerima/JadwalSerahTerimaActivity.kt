package com.example.zenia.kpr.progress.view.halamanJadwalSerahTerima

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.example.zenia.databinding.ActivityJadwalSerahTerimaBinding
import kotlinx.android.synthetic.main.toolbar.view.*

class JadwalSerahTerimaActivity : AppCompatActivity() {
    lateinit var binding:ActivityJadwalSerahTerimaBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityJadwalSerahTerimaBinding.inflate(layoutInflater)

        setContentView(binding.root)

        val appBar = binding.menuToolbar.appBarLayout.toolbar
        setSupportActionBar(appBar)
        supportActionBar?.title = "KPR"
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }
}