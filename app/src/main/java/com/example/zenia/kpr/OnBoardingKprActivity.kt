package com.example.zenia.kpr

import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.TextView
import androidx.viewpager.widget.ViewPager
import com.example.zenia.R
import com.example.zenia.databinding.ActivityOnBoardingKprBinding
import com.example.zenia.home.activity.HomeActivity
import com.google.android.material.tabs.TabLayout

class OnBoardingKprActivity : AppCompatActivity() {

    private lateinit var binding: ActivityOnBoardingKprBinding

    private var viewPagerAdapter: ViewPagerAdapter? = null
    private var tabLayout: TabLayout? = null
    private var onBoardingViewPager: ViewPager? = null
    private var next: TextView? = null
    private var position = 0
    private var sharedPreferences: SharedPreferences? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityOnBoardingKprBinding.inflate(layoutInflater)
        val view = binding.root
        setContentView(view)

        if(restorePrefData()){
            val i = Intent(applicationContext, FormKprActivity::class.java)
            startActivity(i)
        }

        tabLayout = binding.tbLayoutBoard
        next = binding.btnOnboard

        next!!.visibility = View.GONE

        //here the data on the onboarding

        binding.iconBackack.setOnClickListener {
            finish()
        }

        val onBoardingData: MutableList<OnBoardingData> = ArrayList()
        onBoardingData.add(OnBoardingData("Pengajuan Tanpa Ribet", "Nikmati kemudahan pengajuan KPR secara online",
            R.drawable.onboardsatu
        ))
        onBoardingData.add(OnBoardingData("Keuntungan Tanpa Bunga", "Nikmati KPR tanpa bunga dengan kesepakatan yang adil dan cicilan flat",
            R.drawable.onboarddua
        ))
        onBoardingData.add(OnBoardingData("DP Hanya 15%", "Hanya dengan uang muka 15% kamu bisa segera memiliki hunian",
            R.drawable.onboardtiga
        ))

        setOnBoardingViewPagerAdapter(onBoardingData)

        position = onBoardingViewPager!!.currentItem

        next?.setOnClickListener {
            setupSharedPref()
            val i = Intent(applicationContext, FormKprActivity::class.java)
            startActivity(i)

//            if(position < onBoardingData.size) {
//                position++
//                onBoardingViewPager!!.currentItem = position
//            }
//            if(position == onBoardingData.size){
//                setupSharedPref()
//                val i = Intent(applicationContext, HomeActivity::class.java)
//                startActivity(i)
//            }

        }

        tabLayout!!.addOnTabSelectedListener(object : TabLayout.OnTabSelectedListener {
            override fun onTabReselected(tab: TabLayout.Tab?) {
            }

            override fun onTabUnselected(tab: TabLayout.Tab?) {
            }

            override fun onTabSelected(tab: TabLayout.Tab?) {

                position = tab!!.position
                if(tab.position == onBoardingData.size - 1){
                    next!!.visibility = View.VISIBLE
                }else{
                    next!!.visibility = View.GONE
                }
            }

        })
    }

    private fun setOnBoardingViewPagerAdapter(onBoardingData: List<OnBoardingData>){
        onBoardingViewPager = findViewById(binding.vwOnboard.id)
        viewPagerAdapter = ViewPagerAdapter(this,onBoardingData)
        onBoardingViewPager!!.adapter = viewPagerAdapter
        tabLayout?.setupWithViewPager(onBoardingViewPager)
    }

    private fun setupSharedPref() {
        sharedPreferences = applicationContext.getSharedPreferences("preferences", Context.MODE_PRIVATE)
        val editor = sharedPreferences!!.edit()
        editor.putBoolean("FirstTimeRun", true)
        editor.apply()
    }
    private fun restorePrefData(): Boolean {
        sharedPreferences = applicationContext.getSharedPreferences("preferences", Context.MODE_PRIVATE)
        return sharedPreferences!!.getBoolean("FirstTimeRun", false)
    }
}