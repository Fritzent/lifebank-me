package com.example.zenia.kpr.progress.view

import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.zenia.databinding.ActivityKprProgressBinding
import com.example.zenia.kpr.progress.adapter.KprProgressAdapter
import com.example.zenia.kpr.progress.model.ProgressKPR
import com.example.zenia.kpr.progress.presenter.KprProgressPresenter
import com.example.zenia.kpr.progress.view.halamanCicilan.ProgressKPRCicilanActivity
import com.example.zenia.kpr.progress.view.halamanJadwalAkad.JadwalAkadActivity
import com.example.zenia.kpr.progress.view.halamanJadwalSerahTerima.JadwalSerahTerimaActivity
import com.example.zenia.kpr.progress.view.halamanJadwalWawancara.JadwalWawancaraActivity
import kotlinx.android.synthetic.main.toolbar.view.*


class KprProgressActivity : AppCompatActivity(), KprProgressPresenter.Listener {
    lateinit var binding: ActivityKprProgressBinding
    lateinit var presenter: KprProgressPresenter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityKprProgressBinding.inflate(layoutInflater)
        presenter = KprProgressPresenter(this)
        setContentView(binding.root)

        val appBar = binding.menuToolbar.appBarLayout.toolbar
        setSupportActionBar(appBar)
        supportActionBar?.title = "KPR"
        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        presenter.showKPRStatus()
    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }

    override fun showStatusKprList(listStatusKpr: ArrayList<ProgressKPR>) {
        binding.rvKprStatus.layoutManager =
            LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false)
        binding.rvKprStatus.adapter = KprProgressAdapter(listStatusKpr, presenter)
    }

    override fun goToHalamaWawancara() {
        startActivity(Intent(this, JadwalWawancaraActivity::class.java))
    }

    override fun goToHalamanAkad() {
        startActivity(Intent(this, JadwalAkadActivity::class.java))
    }

    override fun goToHalamanCicilan() {
        startActivity(Intent(this, ProgressKPRCicilanActivity::class.java))
    }

    override fun goToHalamanSerahTerima() {
        startActivity(Intent(this, JadwalSerahTerimaActivity::class.java))
    }
}