package com.example.zenia.kpr.progress.view.halamanCicilan

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.example.zenia.databinding.ActivityProgressKPRCicilanBinding
import com.example.zenia.kpr.progress.view.halamanCicilan.adapter.CicilanPagerAdapter
import kotlinx.android.synthetic.main.toolbar.view.*

class ProgressKPRCicilanActivity : AppCompatActivity() {
    lateinit var binding : ActivityProgressKPRCicilanBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityProgressKPRCicilanBinding.inflate(layoutInflater)

        setContentView(binding.root)

        val appBar = binding.menuToolbar.appBarLayout.toolbar
        setSupportActionBar(appBar)
        supportActionBar?.title = "KPR"
        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        binding.viewPagerCicilan.adapter = CicilanPagerAdapter(supportFragmentManager)

        binding.tabMain.setupWithViewPager(binding.viewPagerCicilan)
    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }
}