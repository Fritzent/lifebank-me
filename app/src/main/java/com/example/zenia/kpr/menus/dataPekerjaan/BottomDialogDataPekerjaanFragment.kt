package com.example.zenia.kpr.menus.dataPekerjaan

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.example.zenia.R
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import kotlinx.android.synthetic.main.fragment_bottom_dialog_data_pekerjaan.*

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 * Use the [BottomDialogDataPekerjaanFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class BottomDialogDataPekerjaanFragment : BottomSheetDialogFragment() {
    // TODO: Rename and change types of parameters
    private var param1: String? = null
    private var param2: String? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            param1 = it.getString(ARG_PARAM1)
            param2 = it.getString(ARG_PARAM2)
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_bottom_dialog_data_pekerjaan, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        pekerjaanPNS.setOnClickListener {
            (activity as KprDataPekerjaanActivity).presenter.setPekerjaan("PNS")
            dismiss()
        }

        pekerjaanKaryawanBUMN.setOnClickListener {
            (activity as KprDataPekerjaanActivity).presenter.setPekerjaan("Karyawan BUMN")
            dismiss()
        }

        pekerjaanKaryawanSwasta.setOnClickListener {
            (activity as KprDataPekerjaanActivity).presenter.setPekerjaan("Karyawan Swasta")
            dismiss()
        }

        pekerjaanProfessional.setOnClickListener {
            (activity as KprDataPekerjaanActivity).presenter.setPekerjaan("Professional")
            dismiss()
        }

        pekerjaanTNIPolri.setOnClickListener {
            (activity as KprDataPekerjaanActivity).presenter.setPekerjaan("TNI/Polri")
            dismiss()
        }

        pekerjaanWiraswasta.setOnClickListener {
            (activity as KprDataPekerjaanActivity).presenter.setPekerjaan("Wiraswasta")
            dismiss()
        }
    }

    companion object {
        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         *
         * @param param1 Parameter 1.
         * @param param2 Parameter 2.
         * @return A new instance of fragment BottomDialogDataPekerjaanFragment.
         */
        // TODO: Rename and change types and number of parameters
        @JvmStatic
        fun newInstance(param1: String, param2: String) =
            BottomDialogDataPekerjaanFragment().apply {
                arguments = Bundle().apply {
                    putString(ARG_PARAM1, param1)
                    putString(ARG_PARAM2, param2)
                }
            }
    }
}