package com.example.zenia.kpr.progress.view.halamanCicilan.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.zenia.R
import com.example.zenia.kpr.progress.adapter.KprProgressAdapter
import com.example.zenia.kpr.progress.model.RiwayatCicilan
import kotlinx.android.synthetic.main.item_mutasi_rekening.view.*
import kotlinx.android.synthetic.main.item_riwayat_cicilan.view.*

class RiwayatCicilanAdapter(val listRiwayatCicilan: ArrayList<RiwayatCicilan>) :
    RecyclerView.Adapter<RiwayatCicilanAdapter.ViewHolder>() {

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.item_riwayat_cicilan, parent, false)

        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.itemView.titleRiwayat.text = listRiwayatCicilan[position].title
        holder.itemView.tanggalPembayaranCicilan.text = listRiwayatCicilan[position].tanggal
        holder.itemView.jumlahPembayaran.text = listRiwayatCicilan[position].nominal
    }

    override fun getItemCount(): Int {
        return listRiwayatCicilan.size
    }


}