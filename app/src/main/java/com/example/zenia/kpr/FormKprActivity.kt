package com.example.zenia.kpr

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import com.example.zenia.databinding.ActivityFormKprBinding
import com.example.zenia.home.activity.HomeActivity
import com.example.zenia.kpr.fragment.KprFormInputFragment

class FormKprActivity : AppCompatActivity() {

    private lateinit var binding: ActivityFormKprBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityFormKprBinding.inflate(layoutInflater)
        val view = binding.root
        setContentView(view)

        binding.progressBar.progress = 50

        setupFrameLayout()

        binding.iconBackack.setOnClickListener {
            startActivity(Intent(applicationContext, HomeActivity::class.java))
            finish()
        }

    }

    override fun onBackPressed() {
        startActivity(Intent(applicationContext, HomeActivity::class.java))
        finish()
    }

    private fun setupFrameLayout() {
        supportFragmentManager.beginTransaction().add(binding.frameLayout.id,KprFormInputFragment(), "FormInput").commit()
    }
}