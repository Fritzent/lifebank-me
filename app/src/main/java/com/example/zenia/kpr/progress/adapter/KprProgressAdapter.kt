package com.example.zenia.kpr.progress.adapter

import android.graphics.Color
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.zenia.R
import com.example.zenia.kpr.progress.model.ProgressKPR
import com.example.zenia.kpr.progress.presenter.KprProgressPresenter
import kotlinx.android.synthetic.main.item_kpr_status.view.*

class KprProgressAdapter(
    val listStatusKPR: ArrayList<ProgressKPR>,
    val presenter: KprProgressPresenter
) : RecyclerView.Adapter<KprProgressAdapter.ViewHolder>() {
    class ViewHolder(itemview: View) : RecyclerView.ViewHolder(itemview)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.item_kpr_status, parent, false)

        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val title = holder.itemView.tvTitle
        val subTitle = holder.itemView.tvSubTitle
        val statusIcon = holder.itemView.ivStatusKpr
        val clickIcon = holder.itemView.arrowClick

        val kprState = listStatusKPR[position].state
        val kprStatus = listStatusKPR[position].status

        holder.itemView.setOnClickListener {
            when (position){
                0 -> null
                1 -> presenter.toJadwalWawancara()
                2 -> presenter.toJadwalAkad()
                3 -> presenter.toHalamanCicilan()
                4 -> presenter.toJadwalSerahTerima()
            }
        }

        if (kprState == "pengajuan"){
            title.text = "Pengajuan KPR"
            when (kprStatus) {
                0 -> {
                    subTitle.text = "Data pengajuan KPR akan dievaluasi"
                    state0(holder)
                    holder.itemView.isClickable = false
                }
                1 -> {
                    subTitle.text = "Data pengajuan KPR sedang dievaluasi"
                    state1(holder)
                }
                2 -> {
                    subTitle.text = "Data pengajuan KPR telah dievaluasi"
                    state2(holder)
                }
                else -> {
                    subTitle.text = "Data pengajuan KPR gagal dievaluasi"
                    state3(holder)
                }
            }
        } else if (kprState == "wawancara"){
            title.text = "Wawancara"
            when (kprStatus) {
                0 -> {
                    subTitle.text = "Mohon tunggu proses evaluasi data pengajuan KPR"
                    state0(holder)
                    holder.itemView.isClickable = false
                }
                1 -> {
                    subTitle.text = "Jadwal wawancara telah tersedia"
                    state1(holder)
                }
                2 -> {
                    subTitle.text = "Proses wawancara telah selesai"
                    state2(holder)
                }
                else -> {
                    subTitle.text = "Wawancara gagal, ajukan ulang KPR-mu dari awal"
                    state3(holder)
                }
            }
        } else if (kprState == "akad"){
            title.text = "Akad Kredit"
            when (kprStatus) {
                0 -> {
                    subTitle.text = "Jadwal belum tersedia"
                    state0(holder)
                    holder.itemView.isClickable = false
                }
                1 -> {
                    subTitle.text = "Jadwal telah tersedia"
                    state1(holder)
                }
                2 -> {
                    subTitle.text = "Akad kredit telah dilakukan"
                    state2(holder)
                }
                else -> {
                    subTitle.text = "Akad kredit gagal, ajukan ulang KPR-mu dari awal"
                    state3(holder)
                }
            }
        } else if (kprState == "cicil"){
            title.text = "Proses Cicilan KPR"
            when (kprStatus) {
                0 -> {
                    subTitle.text = "Proses cicilan KPR belum tersedia"
                    state0(holder)
                    holder.itemView.isClickable = false
                }
                1 -> {
                    subTitle.text = "Proses cicilan KPR sedang berjalan"
                    state1(holder)
                }
                2 -> {
                    subTitle.text = "Proses cicilan KPR telah selesai"
                    state2(holder)
                }
                else -> {
                    subTitle.text = "Proses cicilan KPR gagal"
                    state3(holder)
                }
            }
        } else if (kprState == "lunas"){
            title.text = "Serah Terima Dokumen Rumah"
            when (kprStatus) {
                0 -> {
                    subTitle.text = "Jadwal serah terima belum tersedia"
                    state0(holder)
                    holder.itemView.isClickable = false
                }
                1 -> {
                    subTitle.text = "Jadwal serah terima telah tersedia"
                    state1(holder)
                }
                2 -> {
                    subTitle.text = "Serah terima dokumen telah dilakukan"
                    state2(holder)
                }
                else -> {
                    subTitle.text = "Proses KPR gagal"
                    state3(holder)
                }
            }
        }
    }

    override fun getItemCount(): Int {
        return listStatusKPR.size
    }

    fun state0(holder: ViewHolder){
        holder.itemView.tvTitle.setTextColor(Color.parseColor("#888888"))
        holder.itemView.tvSubTitle.setTextColor(Color.parseColor("#888888"))
        holder.itemView.ivStatusKpr.setImageResource(R.drawable.ic_kpr_status)
        holder.itemView.arrowClick.visibility = View.INVISIBLE
        holder.itemView.setBackgroundResource(R.color.white)
    }

    fun state1(holder: ViewHolder){
        holder.itemView.tvTitle.setTextColor(Color.parseColor("#3BA1FF"))
        holder.itemView.tvSubTitle.setTextColor(Color.parseColor("#3BA1FF"))
        holder.itemView.ivStatusKpr.setImageResource(R.drawable.ic_kpr_status_on_proses)
        holder.itemView.arrowClick.visibility = View.VISIBLE
        holder.itemView.arrowClick.setImageResource(R.drawable.ic_kpr_status_click_on_proses)
        holder.itemView.setBackgroundResource(R.color.cSecondaryButton)
        holder.itemView.isClickable = true
    }
    fun state2(holder: ViewHolder){
        holder.itemView.tvTitle.setTextColor(Color.parseColor("#333333"))
        holder.itemView.tvSubTitle.setTextColor(Color.parseColor("#333333"))
        holder.itemView.ivStatusKpr.setImageResource(R.drawable.ic_kpr_status_complete)
        holder.itemView.arrowClick.visibility = View.INVISIBLE
        holder.itemView.setBackgroundResource(R.color.white)
        holder.itemView.isClickable = false
    }

    fun state3(holder: ViewHolder){
        holder.itemView.tvTitle.setTextColor(Color.parseColor("#EE0D0D"))
        holder.itemView.tvSubTitle.setTextColor(Color.parseColor("#EE0D0D"))
        holder.itemView.ivStatusKpr.setImageResource(R.drawable.ic_kpr_status_cancel)
        holder.itemView.arrowClick.visibility = View.VISIBLE
        holder.itemView.arrowClick.setImageResource(R.drawable.ic_kpr_status_click_on_error)
        holder.itemView.setBackgroundResource(R.color.colorRedTransparant)
        holder.itemView.isClickable = true
    }

}