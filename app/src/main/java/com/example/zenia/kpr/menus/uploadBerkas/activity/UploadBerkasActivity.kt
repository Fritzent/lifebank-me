package com.example.zenia.kpr.menus.uploadBerkas.activity

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import com.example.zenia.R
import com.example.zenia.kpr.menus.uploadBerkas.presenter.UploadBerkasPresenter
import kotlinx.android.synthetic.main.activity_upload_berkas.*

class UploadBerkasActivity : AppCompatActivity(), UploadBerkasPresenter.Listener {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_upload_berkas)

        upload_berkas_ktp.setOnClickListener {

        }
    }

    override fun showSlipGaji() {
        upload_berkas_Slip_Gaji.visibility = View.VISIBLE
    }

    override fun showSIP() {
        upload_berkas_SIP.visibility = View.VISIBLE
    }

    override fun showSIUP() {
        upload_berkas_SIUP.visibility = View.VISIBLE
    }

    override fun showAktaNikah() {
        upload_berkas_akta_nikah.visibility = View.VISIBLE
    }

    override fun showAktaCerai() {
        upload_berkas_akta_cerai.visibility = View.VISIBLE
    }

    override fun showAktaKematian() {
        upload_berkas_akta_kematian.visibility = View.VISIBLE
    }

    override fun dismissSlipGaji() {
        upload_berkas_Slip_Gaji.visibility = View.GONE
    }

    override fun dismissSIP() {
        upload_berkas_SIP.visibility = View.GONE
    }

    override fun dismissSIUP() {
        upload_berkas_SIUP.visibility = View.GONE
    }

    override fun dismissAktaNikah() {
        upload_berkas_akta_nikah.visibility = View.GONE
    }

    override fun dismissAktaCerai() {
        upload_berkas_akta_cerai.visibility = View.GONE
    }

    override fun dismissAktaKematian() {
        upload_berkas_akta_kematian.visibility = View.GONE
    }
}