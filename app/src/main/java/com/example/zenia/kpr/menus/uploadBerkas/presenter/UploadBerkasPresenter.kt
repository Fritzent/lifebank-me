package com.example.zenia.kpr.menus.uploadBerkas.presenter

class UploadBerkasPresenter(val listener:Listener) {
    interface Listener{
        fun showSlipGaji()
        fun showSIP()
        fun showSIUP()
        fun showAktaNikah()
        fun showAktaCerai()
        fun showAktaKematian()

        fun dismissSlipGaji()
        fun dismissSIP()
        fun dismissSIUP()
        fun dismissAktaNikah()
        fun dismissAktaCerai()
        fun dismissAktaKematian()
    }

    lateinit var status: String
    lateinit var pekerjaan: String

    fun setData(){
        status = "Belum Kawin"
        pekerjaan = "Pegawai"

        cekStatus()
        cekPekerjaan()
    }

    fun cekStatus(){
        when(status){
            "Belum Kawin"-> {
                listener.dismissAktaCerai()
                listener.dismissAktaKematian()
                listener.dismissAktaNikah()
            }
            "Kawin" -> {
                listener.showAktaNikah()
                listener.dismissAktaCerai()
                listener.dismissAktaKematian()
            }
            "Cerai Hidup" -> {
                listener.showAktaCerai()
                listener.dismissAktaKematian()
                listener.dismissAktaNikah()
            }
            "Cerai Mati" -> {
                listener.showAktaKematian()
                listener.dismissAktaCerai()
                listener.dismissAktaNikah()
            }
        }
    }

    fun cekPekerjaan(){
        when(pekerjaan){
            "Pegawai" -> {
                listener.showSlipGaji()
                listener.dismissSIP()
                listener.dismissSIUP()
            }
            "Profesional" -> {
                listener.showSlipGaji()
                listener.showSIP()
                listener.dismissSIUP()
            }
            "Profesional" -> {
                listener.dismissSlipGaji()
                listener.dismissSIP()
                listener.showSIUP()
            }
        }
    }
}