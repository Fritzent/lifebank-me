package com.example.zenia.kpr.menus.datapribadi.fragment

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ProgressBar
import com.example.zenia.R
import kotlinx.android.synthetic.main.fragment_form_dua_data_pribadi.view.*

class FormDuaDataPribadiFragment : Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val view = inflater.inflate(R.layout.fragment_form_dua_data_pribadi, container, false)

        view.btn_lanjut.setOnClickListener {
            val fm = fragmentManager

            val progressBarUpdate = activity?.findViewById<ProgressBar>(R.id.progress_bar)

            if(progressBarUpdate != null) {
                progressBarUpdate.progress = 100
            }

            fm!!.beginTransaction().replace(R.id.frame_layout,
                FormTigaDataPribadiFragment(), "FormTigaDataPribadi").commit()
        }
        return view
    }

    companion object {
        @JvmStatic
        fun newInstance(param1: String, param2: String) =
            FormDuaDataPribadiFragment()
                .apply {
                arguments = Bundle().apply {
                }
            }
    }
}