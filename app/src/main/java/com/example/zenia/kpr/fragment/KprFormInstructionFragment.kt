package com.example.zenia.kpr.fragment

import android.content.Intent
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.example.zenia.R
import com.example.zenia.home.activity.HomeActivity
import com.example.zenia.kpr.MainKprActivity
import kotlinx.android.synthetic.main.fragment_kpr_form_instruction.view.*

class KprFormInstructionFragment : Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val view = inflater.inflate(R.layout.fragment_kpr_form_instruction, container, false)

        view.btn_instuction.setOnClickListener {
            startActivity(Intent(view.context, MainKprActivity::class.java))
        }
        return view
    }

    companion object {
        @JvmStatic
        fun newInstance(param1: String, param2: String) =
            KprFormInstructionFragment().apply {
                arguments = Bundle().apply {
                }
            }
    }
}