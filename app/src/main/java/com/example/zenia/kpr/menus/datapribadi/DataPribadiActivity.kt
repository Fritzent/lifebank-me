package com.example.zenia.kpr.menus.datapribadi

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.example.zenia.databinding.ActivityDataPribadiBinding
import com.example.zenia.kpr.menus.datapribadi.fragment.FormSatuDataPribadiFragment

class DataPribadiActivity : AppCompatActivity() {

    private lateinit var binding : ActivityDataPribadiBinding
    companion object {
        var STATUS_DATA_PRIBADI = false
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityDataPribadiBinding.inflate(layoutInflater)
        val view = binding.root
        setContentView(view)

        binding.progressBar.progress = 33

        val bundle = Bundle()
        val statusDataPribadi = bundle.getBoolean("STATUS_DATA_PRIBADI")
        STATUS_DATA_PRIBADI = statusDataPribadi

        setupFrameLayout()

        binding.iconBack.setOnClickListener {
            finish()
        }

    }
    private fun setupFrameLayout() {
        supportFragmentManager.beginTransaction().add(binding.frameLayout.id,
            FormSatuDataPribadiFragment(), "FormSatuDataPribadi").commit()
    }
    fun getMyData(): Boolean? {
        return STATUS_DATA_PRIBADI
    }
}