package com.example.zenia.kpr.menus.datakontak

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.example.zenia.databinding.ActivityDataKontakBinding
import com.example.zenia.kpr.menus.datakontak.fragment.FormSatuDataKontakFragment

class DataKontakActivity : AppCompatActivity() {

    private lateinit var binding: ActivityDataKontakBinding
    companion object {
        var STATUS_DATA_KONTAK = false
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityDataKontakBinding.inflate(layoutInflater)
        val view = binding.root
        setContentView(view)

        val bundle = Bundle()
        val statusDataKontak = bundle.getBoolean("STATUS_DATA_KONTAK")
        STATUS_DATA_KONTAK = statusDataKontak

        setupFrameLayout()

        binding.iconBack.setOnClickListener {
            finish()
        }

    }
    private fun setupFrameLayout() {
        supportFragmentManager.beginTransaction().add(binding.frameLayout.id, FormSatuDataKontakFragment(), "FormSatuDataKontak").commit()
    }
    fun getMyData(): Boolean? {
        return STATUS_DATA_KONTAK
    }
}