package com.example.zenia.kpr.progress.view.halamanCicilan.adapter

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter
import com.example.zenia.kpr.progress.view.halamanCicilan.fragment.DetailCicilanFragment
import com.example.zenia.kpr.progress.view.halamanCicilan.fragment.RiwayatCicilanFragment

class CicilanPagerAdapter(fm : FragmentManager) : FragmentPagerAdapter(fm) {
    private val pages = listOf(DetailCicilanFragment(), RiwayatCicilanFragment())

    override fun getCount(): Int {
        return pages.size
    }

    override fun getItem(position: Int): Fragment {
        return pages[position]
    }

    override fun getPageTitle(position: Int): CharSequence? {
        var title= ""

        if (position == 0){
            title = "Detail"
        } else if (position == 1){
            title = "Riwayat"
        }

        return title
    }
}