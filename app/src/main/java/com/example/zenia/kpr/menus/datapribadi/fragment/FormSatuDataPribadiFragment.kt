package com.example.zenia.kpr.menus.datapribadi.fragment

import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ProgressBar
import androidx.core.content.ContextCompat
import com.example.zenia.R
import kotlinx.android.synthetic.main.fragment_form_satu_data_pribadi.view.*

class FormSatuDataPribadiFragment : Fragment() {
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val view = inflater.inflate(R.layout.fragment_form_satu_data_pribadi, container, false)

        view.input_tempat_lahir.requestFocus()

        view.input_tempat_lahir.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(p0: Editable?) {

            }

            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {

            }

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
                if(view.input_tempat_lahir.text.isNotEmpty()) {
                    view.btn_lanjut.setBackgroundResource(R.drawable.custome_button_solid_blue)
                    view.input_tempat_lahir.backgroundTintList =
                        ContextCompat.getColorStateList(view.context.applicationContext, R.color.colorBlue)
                } else if(view.input_tempat_lahir.text.isEmpty()) {
                    view.btn_lanjut.setBackgroundResource(R.drawable.custome_button_solid_gray)
                    view.input_tempat_lahir.backgroundTintList =
                        ContextCompat.getColorStateList(view.context.applicationContext, R.color.colorWhiteC4)
                }
            }
        })

        view.btn_lanjut.setOnClickListener {
            val fm = fragmentManager

            val progressBarUpdate = activity?.findViewById<ProgressBar>(R.id.progress_bar)
            if(progressBarUpdate != null) {
                progressBarUpdate.progress = 66
            }

            fm!!.beginTransaction().replace(R.id.frame_layout,
                FormDuaDataPribadiFragment(), "FormTwoDataPribadi").commit()
        }

        return view
    }

    companion object {
        @JvmStatic
        fun newInstance(param1: String, param2: String) =
            FormSatuDataPribadiFragment()
                .apply {
                arguments = Bundle().apply {

                }
            }
    }
}