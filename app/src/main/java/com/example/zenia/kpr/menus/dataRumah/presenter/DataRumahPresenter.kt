package com.example.zenia.kpr.menus.dataRumah.presenter

import android.util.Log
import java.text.DecimalFormat
import java.text.NumberFormat
import java.util.*
import kotlin.properties.Delegates

class DataRumahPresenter(val listener: Listener) {
    interface Listener {

        fun enableButton()
        fun disableButton()
    }

    private lateinit var alamatRumah: String
    private var hargaRumah by Delegates.notNull<Long>()
    private lateinit var kondisiRumah: String
    private lateinit var pihakPenjual: String
    private lateinit var namaPenjual: String
    private lateinit var noTeleponPenjual: String


    //state
    var stateAlamat = false
    var stateHargaRumah = false
    var stateNamaPenjual = false
    var stateNoTelpPenjual = false
    var stateKondisiRumah = false
    var statePenjual = false

    fun setAlamatRumah(input: String) {
        if (input == "") {
            stateAlamat = false
        } else {
            alamatRumah = input
            stateAlamat = true
        }

        Log.d("state", "state Alamat : $stateAlamat")

        buttonState()
    }

    fun setRumah(input: String) {
        kondisiRumah = input
        stateKondisiRumah = true
    }

    fun setPenjual(input: String) {
        pihakPenjual = input
        statePenjual = true
    }

    fun setNamaPenjual(input: String) {
        if (input == "") {
            stateNamaPenjual = false
        } else {
            namaPenjual = input
            stateNamaPenjual = true
        }
        Log.d("state", "nama penjual : $stateNamaPenjual")
        buttonState()
    }

    fun setNoTelpPenjual(input: String) {
        if (input == "") {
            stateNoTelpPenjual = false
        } else {
            noTeleponPenjual = input
            stateNoTelpPenjual = true
        }

        Log.d("state", "no penjual : $stateNoTelpPenjual")

        buttonState()
    }

    fun setHargaRumah(input: String): String {
        var textInput = input

        if (textInput.contains(".")) {
            textInput = textInput.replace("[.]".toRegex(), "")
        }

        if (textInput.contains("Rp ")) {
            textInput = textInput.replace("Rp ".toRegex(), "")
        }

        if (textInput == "") {
            hargaRumah = 0
            stateHargaRumah = false
        } else {
            hargaRumah = textInput.toLong()
            stateHargaRumah = true
        }

        Log.d("state", "harga rumah : $stateHargaRumah")

        buttonState()

        val formatter =
            NumberFormat.getCurrencyInstance(Locale("in", "ID")) as DecimalFormat

        var formattedString = formatter.format(hargaRumah).toString()

        if (formattedString.contains("Rp")) {
            formattedString = formattedString.replace("Rp".toRegex(), "Rp ")
        }

        if (formattedString.contains(",00")) {
            formattedString = formattedString.replace(",00".toRegex(), "")
        }

        return formattedString
    }

    fun buttonState() {
        if (stateAlamat && stateHargaRumah && stateNamaPenjual && stateNoTelpPenjual && stateKondisiRumah && stateKondisiRumah) {
            listener.enableButton()
        } else {
            listener.disableButton()
        }
    }

    fun logHasil() {
        Log.d(
            "Hasil",
            "$alamatRumah, $hargaRumah, $kondisiRumah, $pihakPenjual, $namaPenjual, $noTeleponPenjual"
        )
    }

}