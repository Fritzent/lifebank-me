package com.example.zenia.kpr

import android.content.Intent
import android.graphics.Color
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import com.example.zenia.R
import com.example.zenia.databinding.ActivityMainKprBinding
import com.example.zenia.kpr.menus.dataAlamat.KprDataAlamatActivity
import com.example.zenia.kpr.menus.dataPekerjaan.KprDataPekerjaanActivity
import com.example.zenia.kpr.menus.dataRumah.activity.DataRumahActivity
import com.example.zenia.kpr.menus.datakontak.DataKontakActivity
import com.example.zenia.kpr.menus.datapribadi.DataPribadiActivity
import com.example.zenia.kpr.menus.uploadBerkas.activity.UploadBerkasActivity

class MainKprActivity : AppCompatActivity() {

    private lateinit var binding: ActivityMainKprBinding

    companion object {
        private var DATA_PRIBADI = false
        private var DATA_KONTAK = false
        private var DATA_PEKERJAAN = false
        private var DATA_ALAMAT = false
        private var DATA_RUMAH = false
        private var DATA_BERKAS = false
    }

    override fun onResume() {
        super.onResume()
        if(DATA_PRIBADI) {
            binding.sectionImageDataPribadi.setImageResource(R.drawable.greencek)
        }
        if(DATA_KONTAK) {
            binding.sectionImageDataKontak.setImageResource(R.drawable.greencek)
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainKprBinding.inflate(layoutInflater)
        val view = binding.root
        setContentView(view)

        //TODO CEKING ITS THE VALUE OF DATA PRIBADI WILL CHANGE WHEN BACK FROM THE DATAPRIBADIACTIVITY
        binding.sectionDataPribadi.setOnClickListener {
            binding.tvDataPribadi.setTextColor(Color.parseColor("#3BA1FF"))
            binding.sectionDataPribadi.setBackgroundResource(R.drawable.custome_background_solid_blue_and_stroke_blue)
            val bundle = Bundle()
            bundle.putBoolean("STATUS_DATA_PRIBADI", DATA_PRIBADI)
            val intent = Intent(applicationContext, DataPribadiActivity::class.java)
            intent.putExtras(bundle)
            startActivity(intent)
        }

        binding.sectionDataKontak.setOnClickListener {
            //here code to handle data kontak
            binding.tvDataKontak.setTextColor(Color.parseColor("#3BA1FF"))
            binding.sectionDataKontak.setBackgroundResource(R.drawable.custome_background_solid_blue_and_stroke_blue)
            val bundle = Bundle()
            bundle.putBoolean("STATUS_DATA_KONTAK", DATA_KONTAK)
            val intent = Intent(applicationContext, DataKontakActivity::class.java)
            intent.putExtras(bundle)
            startActivity(intent)
        }

        binding.sectionDataPekerjaan.setOnClickListener {
            //here code to handle data kontak
            binding.tvDataPekerjaan.setTextColor(Color.parseColor("#3BA1FF"))
            binding.sectionDataPekerjaan.setBackgroundResource(R.drawable.custome_background_solid_blue_and_stroke_blue)
            val intent = Intent(applicationContext, KprDataPekerjaanActivity::class.java)
            startActivity(intent)
        }

        binding.sectionDataAlamat.setOnClickListener {
            //here code to handle data kontak
            binding.tvDataAlamat.setTextColor(Color.parseColor("#3BA1FF"))
            binding.sectionDataAlamat.setBackgroundResource(R.drawable.custome_background_solid_blue_and_stroke_blue)
            val intent = Intent(applicationContext, KprDataAlamatActivity::class.java)
            startActivity(intent)
        }

        binding.sectionDataRumah.setOnClickListener {
            //here code to handle data kontak
            binding.tvDataRumah.setTextColor(Color.parseColor("#3BA1FF"))
            binding.sectionDataRumah.setBackgroundResource(R.drawable.custome_background_solid_blue_and_stroke_blue)
            val intent = Intent(applicationContext, DataRumahActivity::class.java)
            startActivity(intent)
        }

        binding.sectionDataBerkas.setOnClickListener {
            //here code to handle data kontak
            binding.tvDataBerkas.setTextColor(Color.parseColor("#3BA1FF"))
            binding.sectionDataBerkas.setBackgroundResource(R.drawable.custome_background_solid_blue_and_stroke_blue)
            val intent = Intent(applicationContext, UploadBerkasActivity::class.java)
            startActivity(intent)
        }

        binding.iconBackack.setOnClickListener {
            finish()
        }

        //here logic to handle button in main kpr activity
        if(!DATA_PRIBADI && !DATA_KONTAK && !DATA_PEKERJAAN && !DATA_ALAMAT && !DATA_RUMAH && !DATA_BERKAS) {
            //here code to handle color of the btnAjukanKpr
            binding.btnAjukanKpr.setBackgroundResource(R.drawable.custome_button_solid_gray)
        } else {
            binding.btnAjukanKpr.setBackgroundResource(R.drawable.custome_button_solid_blue)
        }

        Log.d("CEKINGSTATUS", "ini data status pribadi : $DATA_PRIBADI")
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if(resultCode == 1) {
            val bundle = Bundle()
            val statusDataPribadi = bundle.getBoolean("NEW_STATUS_PRIBADI_UPDATE")
            Log.d("CEKINGSTATUS", "ini data status pribadi : $statusDataPribadi")
            DATA_PRIBADI = statusDataPribadi
            if(statusDataPribadi) {
                binding.sectionImageDataPribadi.setImageResource(R.drawable.greencek)
            }
        } else if(resultCode == 2) {
            val bundle = Bundle()
            val statusDataKontak = bundle.getBoolean("NEW_STATUS_KONTAK_UPDATE")
            Log.d("CEKINGSTATUS", "ini data status kontak : $statusDataKontak")
            DATA_KONTAK = statusDataKontak
            if(statusDataKontak) {
                binding.sectionImageDataPribadi.setImageResource(R.drawable.greencek)
            }
        }
    }

//    override fun onSaveInstanceState(savedInstanceState: Bundle) {
//        super.onSaveInstanceState(savedInstanceState)
//        if(DATA_PRIBADI) {
//            savedInstanceState.putBoolean("SAVED_DATA_PRIBADI_STATUS", DATA_PRIBADI)
//        } else if (DATA_KONTAK) {
//            savedInstanceState.putBoolean("SAVED_DATA_KONTAK_STATUS", DATA_KONTAK)
//        }
//    }
//    override fun onRestoreInstanceState(savedInstanceState: Bundle) {
//        super.onRestoreInstanceState(savedInstanceState)
//        val statusDataPribadi = savedInstanceState.getBoolean("SAVED_DATA_PRIBADI_STATUS")
//        val statusDataKontak = savedInstanceState.getBoolean("SAVED_DATA_KONTAK_STATUS")
//
////        DATA_PRIBADI = statusDataPribadi
////        DATA_KONTAK = statusDataKontak
//
//        //here to handle the ceklist in main kpr activity
//        if(statusDataPribadi) {
//            DATA_PRIBADI = statusDataPribadi
//        }
//        if(statusDataKontak) {
//            DATA_KONTAK = statusDataKontak
//        }
////        if(DATA_PRIBADI) {
////            binding.sectionImageDataPribadi.setImageResource(R.drawable.greencek)
////        }
////        if(DATA_KONTAK) {
////            binding.sectionImageDataKontak.setImageResource(R.drawable.greencek)
////        }
////        Log.d("CEKINGSTATUS", "UPDATE STATUS newStatusPribadi : $DATA_PRIBADI")
////        Log.d("CEKINGSTATUS", "UPDATE STATUS newStatusKontak : $DATA_KONTAK")
//    }
}