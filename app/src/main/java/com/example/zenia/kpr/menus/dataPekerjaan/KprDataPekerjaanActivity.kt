package com.example.zenia.kpr.menus.dataPekerjaan

import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import androidx.appcompat.app.AppCompatActivity
import com.example.zenia.R
import com.example.zenia.databinding.ActivityKprDataPekerjaanBinding

class KprDataPekerjaanActivity : AppCompatActivity(), KprDataPekerjaanPresenter.Listener {
    lateinit var binding: ActivityKprDataPekerjaanBinding
    lateinit var presenter: KprDataPekerjaanPresenter

    companion object {
        const val DRAWABLE_END = 2
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityKprDataPekerjaanBinding.inflate(layoutInflater)
        presenter = KprDataPekerjaanPresenter(this)

        setContentView(binding.root)

        presenter.buttonState()

        binding.ivBackDataPekerjaanKpr.setOnClickListener {
            finish()
        }

        binding.etJenisPekerjaan.setOnClickListener {
            BottomDialogDataPekerjaanFragment().show(supportFragmentManager, "Data Pekerjaan")
        }

        binding.etPenghasilanBulanan.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence?, p1: Int, p2: Int, p3: Int) {

            }

            override fun onTextChanged(s: CharSequence?, p1: Int, p2: Int, p3: Int) {

            }

            override fun afterTextChanged(s: Editable?) {
                binding.etPenghasilanBulanan.removeTextChangedListener(this)

                binding.etPenghasilanBulanan.setText(presenter.formatPenghasilan(s.toString()))

                binding.etPenghasilanBulanan.setSelection(binding.etPenghasilanBulanan.text.length)
                binding.etPenghasilanBulanan.addTextChangedListener(this)
            }

        })

        binding.etNpwp.addTextChangedListener(object : TextWatcher {
            var  count = 0

            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {

            }

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {

            }

            override fun afterTextChanged(s: Editable?) {
                binding.etNpwp.removeTextChangedListener(this)

                binding.etNpwp.setText(presenter.formatNPWP(s.toString()))

                binding.etNpwp.setSelection(binding.etNpwp.text.length)
                binding.etNpwp.addTextChangedListener(this)
            }

        })



//        binding.etJenisPekerjaan.setOnTouchListener(View.OnTouchListener { _, event ->
//            if(event.rawX >= binding.etJenisPekerjaan.right - binding.etJenisPekerjaan.compoundDrawables
//                        [DRAWABLE_END].bounds.width())
//            {
//                val bottomSheetDialog = BottomSheetDialog(this, R.style.BottomSheetDialogTheme)
//
//                val bottomSheetView = LayoutInflater.from(this).inflate(
//                    R.layout.layout_bottom_sheet_button_kpr_data_pekerjaan,
//                    findViewById(R.id.button_sheet_jenis_pekerjaan)
//                )
//
//                val pns = bottomSheetView.findViewById<TextView>(R.id.pns)
//                pns.setOnClickListener {
//                    binding.etJenisPekerjaan.setText(pns.text.toString())
//                    bottomSheetDialog.dismiss()
//                }
//
//                val tni = bottomSheetView.findViewById<TextView>(R.id.tni)
//                tni.setOnClickListener {
//                    binding.etJenisPekerjaan.setText(tni.text.toString())
//                    bottomSheetDialog.dismiss()
//                }
//
//                val profesional = bottomSheetView.findViewById<TextView>(R.id.profesional)
//                profesional.setOnClickListener {
//                    binding.etJenisPekerjaan.setText(profesional.text.toString())
//                    bottomSheetDialog.dismiss()
//                }
//
//                val bumn = bottomSheetView.findViewById<TextView>(R.id.bumn)
//                bumn.setOnClickListener {
//                    binding.etJenisPekerjaan.setText(bumn.text.toString())
//                    bottomSheetDialog.dismiss()
//                }
//
//                val swasta = bottomSheetView.findViewById<TextView>(R.id.swasta)
//                swasta.setOnClickListener {
//                    binding.etJenisPekerjaan.setText(swasta.text.toString())
//                    bottomSheetDialog.dismiss()
//                }
//
//                val wiraswasta = bottomSheetView.findViewById<TextView>(R.id.wiraswasta)
//                wiraswasta.setOnClickListener {
//                    binding.etJenisPekerjaan.setText(wiraswasta.text.toString())
//                    bottomSheetDialog.dismiss()
//                }
//
//
//
//                //TODO Contoh Kalau Di Bottom Sheet nya Ada button atau action
//                bottomSheetView.findViewById<ImageView>(R.id.ivCloseJenisPekerjaan).setOnClickListener {
//                    bottomSheetDialog.dismiss()
//                }
//                bottomSheetDialog.setContentView(bottomSheetView)
//                bottomSheetDialog.show()
//            }
//            false
//        })

    }

    override fun setPekerjaan(pekerjaan: String){
        binding.etJenisPekerjaan.setText(pekerjaan)
    }

    override fun enableButton() {
        binding.btnSimpanDataPekerjaan.isClickable = true
        binding.btnSimpanDataPekerjaan.setBackgroundResource(R.drawable.primary_button)
        binding.btnSimpanDataPekerjaan.setOnClickListener {
            presenter.logHasil()
        }
    }

    override fun disableButton() {
        binding.btnSimpanDataPekerjaan.isClickable = false
        binding.btnSimpanDataPekerjaan.setBackgroundResource(R.drawable.disabled_button)
    }
}