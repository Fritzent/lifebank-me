package com.example.zenia.kpr.menus.datakontak.fragment

import android.content.Intent
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.example.zenia.R
import com.example.zenia.kpr.MainKprActivity
import com.example.zenia.kpr.menus.datakontak.DataKontakActivity
import kotlinx.android.synthetic.main.fragment_form_satu_data_kontak.view.*

class FormSatuDataKontakFragment : Fragment() {
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val view = inflater.inflate(R.layout.fragment_form_satu_data_kontak, container, false)

        val activity: DataKontakActivity? = activity as DataKontakActivity?
        val myDataFromActivity: Boolean? = activity?.getMyData()

        view.btn_simpan.setOnClickListener {
            if(myDataFromActivity == false) {
                val bundle = Bundle()
                bundle.putBoolean("NEW_STATUS_KONTAK_UPDATE", true)
                val intent = Intent(view.context, MainKprActivity::class.java)
                intent.putExtras(bundle)
                startActivityForResult(intent, 2)
                activity.finish()
//                startActivity(intent)
            } else {
                val intent = Intent(view.context, MainKprActivity::class.java)
                startActivityForResult(intent, 2)
                activity?.finish()
//                startActivity(Intent(view.context, MainKprActivity::class.java))
            }
        }
        return view
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
//        super.onActivityResult(requestCode, resultCode, data)

    }

    companion object {
        @JvmStatic
        fun newInstance() =
            FormSatuDataKontakFragment().apply {
                arguments = Bundle().apply {

                }
            }
    }
}