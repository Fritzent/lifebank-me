package com.example.zenia.kpr.menus.dataRumah.activity

import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import com.example.zenia.R
import com.example.zenia.databinding.ActivityDataRumahBinding
import com.example.zenia.kpr.menus.dataRumah.presenter.DataRumahPresenter
import kotlinx.android.synthetic.main.activity_data_rumah.*

class DataRumahActivity : AppCompatActivity(), DataRumahPresenter.Listener {
    private lateinit var presenter: DataRumahPresenter
    lateinit var binding: ActivityDataRumahBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityDataRumahBinding.inflate(layoutInflater)
        presenter = DataRumahPresenter(this)

        setContentView(binding.root)

        binding.iconBack.setOnClickListener {
            finish()
        }

        binding.fotoRumahKprChoose.visibility = View.GONE

        binding.etAlamatRumahKPR.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {

            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {

            }

            override fun afterTextChanged(s: Editable?) {
                binding.etAlamatRumahKPR.removeTextChangedListener(this)

                presenter.setAlamatRumah(s.toString())

                binding.etAlamatRumahKPR.setSelection(binding.etAlamatRumahKPR.text.length)
                binding.etAlamatRumahKPR.addTextChangedListener(this)
            }
        })

        binding.etNamaPenjual.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {

            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {

            }

            override fun afterTextChanged(s: Editable?) {
                binding.etNamaPenjual.removeTextChangedListener(this)

                presenter.setNamaPenjual(s.toString())

                binding.etNamaPenjual.setSelection(binding.etNamaPenjual.text.length)
                binding.etNamaPenjual.addTextChangedListener(this)
            }
        })

        binding.etNoTelpPenjual.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {

            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {

            }

            override fun afterTextChanged(s: Editable?) {
                binding.etNoTelpPenjual.removeTextChangedListener(this)

                presenter.setNoTelpPenjual(s.toString())

                binding.etNoTelpPenjual.setSelection(binding.etNoTelpPenjual.text.length)
                binding.etNoTelpPenjual.addTextChangedListener(this)
            }
        })

        binding.etPerkiraanHarga.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {

            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {

            }

            override fun afterTextChanged(s: Editable?) {
                binding.etPerkiraanHarga.removeTextChangedListener(this)

                binding.etPerkiraanHarga.setText(presenter.setHargaRumah(s.toString()))

                binding.etPerkiraanHarga.setSelection(binding.etPerkiraanHarga.text.length)
                binding.etPerkiraanHarga.addTextChangedListener(this)
            }

        })

        presenter.buttonState()

        binding.rbBaru.setOnClickListener {
            presenter.setRumah("Baru")
        }

        binding.rbLama.setOnClickListener {
            presenter.setRumah("Lama")
        }

        binding.rbAgent.setOnClickListener {
            presenter.setPenjual("Agent")
        }
        binding.rbPerorangan.setOnClickListener {
            presenter.setPenjual("Perorangan")
        }
    }

    override fun disableButton() {
        binding.btnSimpan.isClickable = false
        binding.btnSimpan.setBackgroundResource(R.drawable.disabled_button)
        Log.d("Button", "disabled")
    }

    override fun enableButton() {
        binding.btnSimpan.isClickable = true
        binding.btnSimpan.setBackgroundResource(R.drawable.primary_button)
        binding.btnSimpan.setOnClickListener {
            presenter.logHasil()
        }
        Log.d("Button", "enabled")
    }

}