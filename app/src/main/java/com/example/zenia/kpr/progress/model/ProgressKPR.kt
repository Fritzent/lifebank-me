package com.example.zenia.kpr.progress.model

data class ProgressKPR (
    val state: String,
    val status: Int
)