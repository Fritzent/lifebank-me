package com.example.zenia.kpr.progress.view.halamanJadwalWawancara

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.example.zenia.databinding.ActivityJadwalWawancaraBinding
import kotlinx.android.synthetic.main.toolbar.view.*

class JadwalWawancaraActivity : AppCompatActivity() {
    lateinit var binding: ActivityJadwalWawancaraBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityJadwalWawancaraBinding.inflate(layoutInflater)

        setContentView(binding.root)

        val appBar = binding.menuToolbar.appBarLayout.toolbar
        setSupportActionBar(appBar)
        supportActionBar?.title = "KPR"
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }
}