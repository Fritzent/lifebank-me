package com.example.zenia.kpr.menus.datapribadi.fragment

import android.content.Intent
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import com.example.zenia.R
import com.example.zenia.kpr.MainKprActivity
import com.example.zenia.kpr.menus.datapribadi.DataPribadiActivity
import kotlinx.android.synthetic.main.fragment_form_tiga_data_pribadi.view.*

class FormTigaDataPribadiFragment : Fragment() {
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val view = inflater.inflate(R.layout.fragment_form_tiga_data_pribadi, container, false)

        val activity: DataPribadiActivity? = activity as DataPribadiActivity?
        val myDataFromActivity: Boolean? = activity?.getMyData()

        view.input_nomor_kk.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(p0: Editable?) {

            }

            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {

            }

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
                if(view.input_nomor_kk.text.isNotEmpty()) {
                    view.btn_simpan.setBackgroundResource(R.drawable.custome_button_solid_blue)
                    view.input_nomor_kk.backgroundTintList =
                        ContextCompat.getColorStateList(view.context.applicationContext, R.color.colorBlue)
                } else if(view.input_nomor_kk.text.isEmpty()) {
                    view.btn_simpan.setBackgroundResource(R.drawable.custome_button_solid_gray)
                    view.input_nomor_kk.backgroundTintList =
                        ContextCompat.getColorStateList(view.context.applicationContext, R.color.colorWhiteC4)
                }
            }
        })

        view.btn_simpan.setOnClickListener {
            if(myDataFromActivity == false) {
                //here code to back to MainKprActivity
                val bundle = Bundle()
                bundle.putBoolean("NEW_STATUS_PRIBADI_UPDATE", true)
                val intent = Intent(view.context, MainKprActivity::class.java)
                intent.putExtras(bundle)
//                startActivity(intent)
                startActivityForResult(intent, 1)
                activity.finish()
            } else {
                val intent = Intent(view.context, MainKprActivity::class.java)
                startActivityForResult(intent, 1)
                activity?.finish()
//                startActivity(Intent(view.context, MainKprActivity::class.java))
            }
        }

        return view
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
//        super.onActivityResult(requestCode, resultCode, data)
    }

    companion object {
        @JvmStatic
        fun newInstance(param1: String, param2: String) =
            FormTigaDataPribadiFragment()
                .apply {
                arguments = Bundle().apply {
                }
            }
    }
}