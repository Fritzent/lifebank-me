package com.example.zenia.kpr.fragment

import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ProgressBar
import androidx.core.content.ContextCompat
import com.example.zenia.R
import kotlinx.android.synthetic.main.fragment_kpr_form_input.view.*

class KprFormInputFragment : Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment

        val view = inflater.inflate(R.layout.fragment_kpr_form_input, container, false)

        var cekFirstClick = 0

        view.text_error.visibility = View.GONE
        //here code to handel the kpr form input
        view.input_cicilan_bulanan.requestFocus()
        view.tooltip_hint.visibility = View.GONE
        view.input_cicilan_bulanan.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(p0: Editable?) {
            }

            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {

            }

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int){
                if(view.input_cicilan_bulanan.text.isNotEmpty()) {
                    view.input_cicilan_bulanan.backgroundTintList =
                        ContextCompat.getColorStateList(view.context.applicationContext, R.color.colorBlue)
                    view.btn_kpr.setBackgroundResource(R.drawable.custome_button_solid_blue)
                } else {
                    view.input_cicilan_bulanan.backgroundTintList =
                        ContextCompat.getColorStateList(view.context.applicationContext, R.color.colorWhiteC4)
                    view.btn_kpr.setBackgroundResource(R.drawable.custome_button_solid_gray)
                }
            }
        })

        view.input_cicilan_bulanan.setOnClickListener {
            //here to handel first click in edit text
            if(cekFirstClick == 0) {
                view.tooltip_hint.visibility = View.VISIBLE
                cekFirstClick = 1
            } else  {
                view.tooltip_hint.visibility = View.GONE
                cekFirstClick = 0
            }
        }

        view.btn_kpr.setOnClickListener {
            // This code to check inputan cicilan bulanan must be more than 1 billion
            if(view.input_cicilan_bulanan.text.isNotEmpty()) {
                if(view.input_cicilan_bulanan.text.toString().toLong() < 1000000) {
                    view.text_error.visibility = View.VISIBLE
                    view.input_cicilan_bulanan.requestFocus()
                } else {
                    view.text_error.visibility = View.GONE
                    val fm = fragmentManager

                    val progressBarUpdate = activity?.findViewById<ProgressBar>(R.id.progress_bar)
                    if(progressBarUpdate != null) {
                        progressBarUpdate.progress = 100
                    }

                    fm!!.beginTransaction().replace(R.id.frame_layout, KprFormInstructionFragment(), "FormInstruction").commit()
                }
            } else {
                view.text_error.visibility = View.VISIBLE
                view.input_cicilan_bulanan.requestFocus()
            }

        }
        return view
    }

    companion object {
        @JvmStatic
        fun newInstance(param1: String, param2: String) =
            KprFormInputFragment().apply {
                arguments = Bundle().apply {
                }
            }
    }
}