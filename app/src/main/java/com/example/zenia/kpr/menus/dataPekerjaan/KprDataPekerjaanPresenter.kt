package com.example.zenia.kpr.menus.dataPekerjaan

import android.util.Log
import java.lang.StringBuilder
import java.text.DecimalFormat
import java.text.NumberFormat
import java.util.*

class KprDataPekerjaanPresenter(val listener: Listener) {
    interface Listener {
        fun setPekerjaan(pekerjaan: String)

        fun enableButton()
        fun disableButton()
    }

    private var kerja: String = ""
    private var penghasilan: Long = 0
    private var npwp: String = ""

    private var statePekerjaan = false
    private var statePenghasilan = false
    private var stateNPWP = false

    fun setPekerjaan(pekerjaan: String) {

        if (pekerjaan == ""){
            statePekerjaan = false
        } else {
            statePekerjaan = true
        }

        Log.d("Log","$statePekerjaan")

        kerja = pekerjaan

        listener.setPekerjaan(pekerjaan)

        buttonState()
    }

    fun formatPenghasilan(input: String): String {
        var textInput = input

        if (textInput.contains(".")) {
            textInput = textInput.replace("[.]".toRegex(), "")
        }

        if (textInput.contains("Rp ")) {
            textInput = textInput.replace("Rp ".toRegex(), "")
        }

        penghasilan = if (textInput == "") {
            0
        } else {
            textInput.toLong()
        }

        statePenghasilan = when {
            penghasilan <= 0 -> {
                false
            }
            else -> {
                true
            }
        }

        val formatter =
            NumberFormat.getCurrencyInstance(Locale("in", "ID")) as DecimalFormat

        var formattedString = formatter.format(penghasilan).toString()

        if (formattedString.contains("Rp")) {
            formattedString = formattedString.replace("Rp".toRegex(), "Rp ")
        }

        if (formattedString.contains(",00")) {
            formattedString = formattedString.replace(",00".toRegex(), "")
        }

        Log.d("Log","$statePenghasilan")

        buttonState()

        return formattedString
    }

    fun formatNPWP(input: String): String{
        var textInput = input

        if (textInput.contains(" ")) {
            textInput = textInput.replace(" ", "")
        }

        if (textInput == ""){
            npwp = ""
            stateNPWP = false
        } else {
            npwp = textInput
            stateNPWP = true
        }

        val s = StringBuilder(textInput)

        run {
            var i = 4
            while (i < s.length) {
                s.insert(i, " ")
                i += 5
            }
        }

        Log.d("Log","$stateNPWP")
        buttonState()

        return s.toString()
    }

    fun buttonState(){
        Log.d("State", "$statePekerjaan, $stateNPWP, $statePenghasilan")
        if (statePekerjaan && statePenghasilan && stateNPWP){
            listener.enableButton()
        } else {
            listener.disableButton()
        }
    }

    fun logHasil(){
        Log.d("Hasil Data Pekerjaan", "Pekerjaan : $kerja, penghasilan : $penghasilan, NPWP : $npwp")
    }
}