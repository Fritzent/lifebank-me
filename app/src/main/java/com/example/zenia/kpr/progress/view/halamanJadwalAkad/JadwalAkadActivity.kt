package com.example.zenia.kpr.progress.view.halamanJadwalAkad

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.example.zenia.databinding.ActivityJadwalAkadBinding
import kotlinx.android.synthetic.main.toolbar.view.*

class JadwalAkadActivity : AppCompatActivity() {
    lateinit var binding: ActivityJadwalAkadBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityJadwalAkadBinding.inflate(layoutInflater)

        setContentView(binding.root)

        val appBar = binding.menuToolbar.appBarLayout.toolbar
        setSupportActionBar(appBar)
        supportActionBar?.title = "KPR"
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }
}