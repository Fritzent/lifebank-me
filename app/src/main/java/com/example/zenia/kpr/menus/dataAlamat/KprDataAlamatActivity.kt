package com.example.zenia.kpr.menus.dataAlamat

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import com.example.zenia.R
import com.example.zenia.databinding.ActivityKprDataAlamatBinding

class KprDataAlamatActivity : AppCompatActivity(), KprDataAlamatPresenter.Listener {
    lateinit var binding:ActivityKprDataAlamatBinding
    lateinit var presenter:KprDataAlamatPresenter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = ActivityKprDataAlamatBinding.inflate(layoutInflater)
        presenter = KprDataAlamatPresenter(this)

        setContentView(binding.root)

        presenter.checkState()

        binding.ivBackDataAlamatKpr.setOnClickListener {
            finish()
        }

        binding.etAlamat.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {

            }

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {

            }

            override fun afterTextChanged(p0: Editable?) {
                binding.etAlamat.removeTextChangedListener(this)

                presenter.setAlamat(p0.toString())

                binding.etAlamat.setSelection(binding.etAlamat.text.length)
                binding.etAlamat.addTextChangedListener(this)
            }
        })

        binding.etProvinsi.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {

            }

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {

            }

            override fun afterTextChanged(p0: Editable?) {
                binding.etProvinsi.removeTextChangedListener(this)

                presenter.setProvinsi(p0.toString())

                binding.etProvinsi.setSelection(binding.etProvinsi.text.length)
                binding.etProvinsi.addTextChangedListener(this)
            }
        })

        binding.etKota.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {

            }

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {

            }

            override fun afterTextChanged(p0: Editable?) {
                binding.etKota.removeTextChangedListener(this)

                presenter.setKota(p0.toString())

                binding.etKota.setSelection(binding.etKota.text.length)
                binding.etKota.addTextChangedListener(this)
            }
        })

        binding.etKecamatan.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {

            }

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {

            }

            override fun afterTextChanged(p0: Editable?) {
                binding.etKecamatan.removeTextChangedListener(this)

                presenter.setKecamatan(p0.toString())

                binding.etKecamatan.setSelection(binding.etKecamatan.text.length)
                binding.etKecamatan.addTextChangedListener(this)
            }
        })

        binding.etKelurahan.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {

            }

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {

            }

            override fun afterTextChanged(p0: Editable?) {
                binding.etKelurahan.removeTextChangedListener(this)

                presenter.setKelurahan(p0.toString())

                binding.etKelurahan.setSelection(binding.etKelurahan.text.length)
                binding.etKelurahan.addTextChangedListener(this)
            }
        })

        binding.etKodePos.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {

            }

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {}

            override fun afterTextChanged(p0: Editable?) {
                binding.etKodePos.removeTextChangedListener(this)

                presenter.setKodePos(p0.toString())

                binding.etKodePos.setSelection(binding.etKodePos.text.length)
                binding.etKodePos.addTextChangedListener(this)
            }
        })
    }

    override fun enableButton() {
        binding.btnDataAlamat.isClickable = true
        binding.btnDataAlamat.setBackgroundResource(R.drawable.primary_button)
        binding.btnDataAlamat.setOnClickListener {
            presenter.logHasil()
        }
    }

    override fun disableButton() {
        binding.btnDataAlamat.isClickable = false
        binding.btnDataAlamat.setBackgroundResource(R.drawable.disabled_button)
    }
}