package com.example.zenia.kpr.progress.presenter

import com.example.zenia.kpr.progress.model.ProgressKPR

class KprProgressPresenter(val listener: Listener) {
    interface Listener{
        fun showStatusKprList(listStatusKpr: ArrayList<ProgressKPR>)

        fun goToHalamaWawancara()
        fun goToHalamanAkad()
        fun goToHalamanCicilan()
        fun goToHalamanSerahTerima()
    }

    fun showKPRStatus(){
        val kprStatusList = arrayListOf(
            ProgressKPR("pengajuan", 2),
            ProgressKPR("wawancara", 1),
            ProgressKPR("akad", 1),
            ProgressKPR("cicil", 1),
            ProgressKPR("lunas", 1)
        )

        listener.showStatusKprList(kprStatusList)
    }

    fun toJadwalWawancara(){
        listener.goToHalamaWawancara()
    }

    fun toJadwalAkad(){
        listener.goToHalamanAkad()
    }

    fun toHalamanCicilan(){
        listener.goToHalamanCicilan()
    }

    fun toJadwalSerahTerima(){
        listener.goToHalamanSerahTerima()
    }
}