package com.example.zenia.kpr.menus.dataAlamat

import android.util.Log

class KprDataAlamatPresenter(val listener: Listener) {
    interface Listener {
        fun enableButton()
        fun disableButton()
    }

    lateinit var alamatKPR: String
    lateinit var provinsiKPR: String
    lateinit var kotaKPR: String
    lateinit var kecamatanKPR: String
    lateinit var kelurahanKPR: String
    lateinit var kodePosKPR: String

    var stateAlamat = false
    var stateProvinsi = false
    var stateKota = false
    var stateKecamatan = false
    var stateKelurahan = false
    var stateKodePos = false

    fun setAlamat(input: String) {
        stateAlamat = input != ""

        alamatKPR = input

        checkState()
    }

    fun setProvinsi(input: String) {
        stateProvinsi = input != ""

        provinsiKPR = input

        checkState()
    }

    fun setKota(input: String) {
        stateKota = input != ""

        kotaKPR = input

        checkState()
    }

    fun setKecamatan(input: String) {
        stateKecamatan = input != ""

        kecamatanKPR = input

        checkState()
    }

    fun setKelurahan(input: String) {
        stateKelurahan = input != ""

        kelurahanKPR = input

        checkState()
    }

    fun setKodePos(input: String) {
        stateKodePos = input != ""

        kodePosKPR = input

        checkState()
    }

    fun checkState() {
        if (stateAlamat && stateProvinsi && stateKota && stateKecamatan && stateKelurahan && stateKodePos) {
            listener.enableButton()
        } else {
            listener.disableButton()
        }
    }

    fun logHasil() {
        Log.d(
            "Hasil",
            "Alamat : $alamatKPR, Provinsi: $provinsiKPR, Kota: $kotaKPR, Kecamatan: $kecamatanKPR, Kelurahan: $kelurahanKPR, Kode Pos : $kodePosKPR"
        )
    }
}