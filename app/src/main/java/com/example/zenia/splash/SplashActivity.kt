package com.example.zenia.splash

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import android.view.animation.AnimationUtils
import com.example.zenia.R
import com.example.zenia.databinding.ActivitySplashBinding
import com.example.zenia.onboarding.OnBoardingAppActivity

class SplashActivity : AppCompatActivity() {
    companion object {
        const val TIME_DELAY: Long = 6000
    }

    private lateinit var binding:  ActivitySplashBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivitySplashBinding.inflate(layoutInflater)
        val view = binding.root
        setContentView(view)

        setupAnimation()


        Handler().postDelayed({
            //TODO CHANGE LOGIN ACTIVITY INTO ONBOARDING
            startActivity(Intent(this, OnBoardingAppActivity::class.java))
            finish()
        }, TIME_DELAY)
    }

    private fun setupAnimation(){
        val animFadeIn = AnimationUtils.loadAnimation(applicationContext,
            R.anim.fade_in_anim
        )
        binding.logoZenia.startAnimation(animFadeIn)
    }
}